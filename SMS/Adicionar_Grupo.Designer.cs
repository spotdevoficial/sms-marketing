﻿namespace SMS
{
    partial class Adicionar_Grupo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Adicionar_Grupo));
            this.buttonCancelar = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonSalvar = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonRemoverAll = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonAdicionarAll = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonRemover1 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonAdicionar1 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.listSemGrupo = new ComponentFactory.Krypton.Toolkit.KryptonListBox();
            this.kryptonLabel3 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.textNomeGrupo = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.labelTotalS = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.labelTotalG = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.listComGrupo = new ComponentFactory.Krypton.Toolkit.KryptonListBox();
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel2 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.labelCarregamento = new System.Windows.Forms.ToolStripStatusLabel();
            this.progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonCancelar
            // 
            this.buttonCancelar.Location = new System.Drawing.Point(302, 292);
            this.buttonCancelar.Name = "buttonCancelar";
            this.buttonCancelar.Size = new System.Drawing.Size(78, 58);
            this.buttonCancelar.TabIndex = 55;
            this.buttonCancelar.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonCancelar.Values.Image")));
            this.buttonCancelar.Values.Text = "";
            this.buttonCancelar.Click += new System.EventHandler(this.buttonCancelar_Click);
            // 
            // buttonSalvar
            // 
            this.buttonSalvar.Location = new System.Drawing.Point(302, 228);
            this.buttonSalvar.Name = "buttonSalvar";
            this.buttonSalvar.Size = new System.Drawing.Size(78, 58);
            this.buttonSalvar.TabIndex = 54;
            this.buttonSalvar.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonSalvar.Values.Image")));
            this.buttonSalvar.Values.Text = "";
            this.buttonSalvar.Click += new System.EventHandler(this.buttonSalvar_Click);
            // 
            // buttonRemoverAll
            // 
            this.buttonRemoverAll.Location = new System.Drawing.Point(283, 168);
            this.buttonRemoverAll.Name = "buttonRemoverAll";
            this.buttonRemoverAll.Size = new System.Drawing.Size(111, 25);
            this.buttonRemoverAll.TabIndex = 50;
            this.buttonRemoverAll.Values.Text = "Remover <<";
            this.buttonRemoverAll.Click += new System.EventHandler(this.buttonRemoverAll_Click);
            // 
            // buttonAdicionarAll
            // 
            this.buttonAdicionarAll.Location = new System.Drawing.Point(283, 137);
            this.buttonAdicionarAll.Name = "buttonAdicionarAll";
            this.buttonAdicionarAll.Size = new System.Drawing.Size(111, 25);
            this.buttonAdicionarAll.TabIndex = 49;
            this.buttonAdicionarAll.Values.Text = "Adicionar >>";
            this.buttonAdicionarAll.Click += new System.EventHandler(this.buttonAdicionarAll_Click);
            // 
            // buttonRemover1
            // 
            this.buttonRemover1.Location = new System.Drawing.Point(283, 106);
            this.buttonRemover1.Name = "buttonRemover1";
            this.buttonRemover1.Size = new System.Drawing.Size(111, 25);
            this.buttonRemover1.TabIndex = 48;
            this.buttonRemover1.Values.Text = "Remover <";
            this.buttonRemover1.Click += new System.EventHandler(this.buttonRemover1_Click);
            // 
            // buttonAdicionar1
            // 
            this.buttonAdicionar1.Location = new System.Drawing.Point(283, 75);
            this.buttonAdicionar1.Name = "buttonAdicionar1";
            this.buttonAdicionar1.Size = new System.Drawing.Size(111, 25);
            this.buttonAdicionar1.TabIndex = 47;
            this.buttonAdicionar1.Values.Text = "Adicionar >";
            this.buttonAdicionar1.Click += new System.EventHandler(this.buttonAdicionar1_Click);
            // 
            // listSemGrupo
            // 
            this.listSemGrupo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listSemGrupo.HorizontalScrollbar = true;
            this.listSemGrupo.Location = new System.Drawing.Point(9, 75);
            this.listSemGrupo.Name = "listSemGrupo";
            this.listSemGrupo.Size = new System.Drawing.Size(268, 270);
            this.listSemGrupo.TabIndex = 46;
            // 
            // kryptonLabel3
            // 
            this.kryptonLabel3.Location = new System.Drawing.Point(400, 57);
            this.kryptonLabel3.Name = "kryptonLabel3";
            this.kryptonLabel3.Size = new System.Drawing.Size(118, 20);
            this.kryptonLabel3.TabIndex = 45;
            this.kryptonLabel3.Values.Text = "Membros do Grupo";
            // 
            // textNomeGrupo
            // 
            this.textNomeGrupo.Location = new System.Drawing.Point(105, 11);
            this.textNomeGrupo.Name = "textNomeGrupo";
            this.textNomeGrupo.Size = new System.Drawing.Size(566, 20);
            this.textNomeGrupo.TabIndex = 43;
            // 
            // labelTotalS
            // 
            this.labelTotalS.Location = new System.Drawing.Point(246, 57);
            this.labelTotalS.Name = "labelTotalS";
            this.labelTotalS.Size = new System.Drawing.Size(31, 20);
            this.labelTotalS.TabIndex = 52;
            this.labelTotalS.Values.Text = "( 0 )";
            // 
            // labelTotalG
            // 
            this.labelTotalG.Location = new System.Drawing.Point(640, 57);
            this.labelTotalG.Name = "labelTotalG";
            this.labelTotalG.Size = new System.Drawing.Size(31, 20);
            this.labelTotalG.TabIndex = 53;
            this.labelTotalG.Values.Text = "( 0 )";
            // 
            // listComGrupo
            // 
            this.listComGrupo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listComGrupo.HorizontalScrollbar = true;
            this.listComGrupo.Location = new System.Drawing.Point(403, 75);
            this.listComGrupo.Name = "listComGrupo";
            this.listComGrupo.Size = new System.Drawing.Size(268, 270);
            this.listComGrupo.TabIndex = 56;
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.Location = new System.Drawing.Point(9, 11);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(102, 20);
            this.kryptonLabel1.TabIndex = 42;
            this.kryptonLabel1.Values.Text = "Nome do Grupo:";
            // 
            // kryptonLabel2
            // 
            this.kryptonLabel2.Location = new System.Drawing.Point(9, 57);
            this.kryptonLabel2.Name = "kryptonLabel2";
            this.kryptonLabel2.Size = new System.Drawing.Size(126, 20);
            this.kryptonLabel2.TabIndex = 44;
            this.kryptonLabel2.Values.Text = "Membros sem Grupo";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.labelCarregamento,
            this.progressBar});
            this.statusStrip1.Location = new System.Drawing.Point(0, 355);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(683, 22);
            this.statusStrip1.TabIndex = 57;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // labelCarregamento
            // 
            this.labelCarregamento.Name = "labelCarregamento";
            this.labelCarregamento.Size = new System.Drawing.Size(126, 17);
            this.labelCarregamento.Text = "Adicionando ao grupo";
            // 
            // progressBar
            // 
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // Adicionar_Grupo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 377);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.listComGrupo);
            this.Controls.Add(this.buttonCancelar);
            this.Controls.Add(this.buttonSalvar);
            this.Controls.Add(this.buttonRemoverAll);
            this.Controls.Add(this.buttonAdicionarAll);
            this.Controls.Add(this.buttonRemover1);
            this.Controls.Add(this.buttonAdicionar1);
            this.Controls.Add(this.listSemGrupo);
            this.Controls.Add(this.kryptonLabel3);
            this.Controls.Add(this.kryptonLabel2);
            this.Controls.Add(this.textNomeGrupo);
            this.Controls.Add(this.kryptonLabel1);
            this.Controls.Add(this.labelTotalS);
            this.Controls.Add(this.labelTotalG);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Adicionar_Grupo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Adicionar Grupo";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Adicionar_Grupo_FormClosing);
            this.Load += new System.EventHandler(this.Adicionar_Grupo_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonCancelar;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonSalvar;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonRemoverAll;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonAdicionarAll;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonRemover1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonAdicionar1;
        private ComponentFactory.Krypton.Toolkit.KryptonListBox listSemGrupo;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel3;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textNomeGrupo;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel labelTotalS;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel labelTotalG;
        private ComponentFactory.Krypton.Toolkit.KryptonListBox listComGrupo;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel1;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel labelCarregamento;
        private System.Windows.Forms.ToolStripProgressBar progressBar;
    }
}