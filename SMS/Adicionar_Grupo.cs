﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace SMS
{
    public partial class Adicionar_Grupo : Form
    {
        int indexLista = -1;
        Thread sendingThread = null;

        public Adicionar_Grupo()
        {
            InitializeComponent();
        }

        private void Adicionar_Grupo_Load(object sender, EventArgs e)
        {
            SQLiteDataReader dr = BD.retornaQuery("SELECT Nome, Telefone, ID_Grupo, ID_Contato FROM Contatos WHERE ID_Grupo < 0");
            while (dr.Read())
            {
                if (dr[2].ToString() == "-1")
                    listSemGrupo.Items.Add((dr[0].ToString() + " <" + dr[1].ToString() + ">"));
                else
                    listComGrupo.Items.Add((dr[0].ToString() + " <" + dr[1].ToString() + ">"));
            }
            dr.Close();
        }

        private void atualizaTotal()
        {
            //ATUALIZA OS CONTADORES ATUAIS 
            labelTotalS.Text = "(" + listSemGrupo.Items.Count + ")";
            labelTotalG.Text = "(" + listComGrupo.Items.Count + ")";
        }

        private void buttonAdicionar1_Click(object sender, EventArgs e)
        {
            if (listSemGrupo.SelectedIndex >= 0)
            {
                Object selecionado = listSemGrupo.SelectedItem;
                listComGrupo.Items.Add(selecionado);
                listSemGrupo.Items.Remove(selecionado);

                string telefoneArrastado = selecionado.ToString().Substring(selecionado.ToString().IndexOf(" <"), +
                        (selecionado.ToString().Length - selecionado.ToString().IndexOf(" <"))).Replace("<", "").Replace(">", "").Replace(" ", "");

                int count = listSemGrupo.Items.Count;
                List<object> aRemover = new List<object>();
         
                for (int i = 0; i < count; i ++)
                {
                    Object listBoxItem = listSemGrupo.Items[i];
                    string telefone = listBoxItem.ToString().Substring(listBoxItem.ToString().IndexOf(" <"), +
                            (listBoxItem.ToString().Length - listBoxItem.ToString().IndexOf(" <"))).Replace("<", "").Replace(">", "").Replace(" ", "");
                    if (telefone.Equals(telefoneArrastado))
                    {
                        listComGrupo.Items.Add(listBoxItem);
                        aRemover.Add(listBoxItem);
                    }

                }

                foreach(object o in aRemover)
                {
                    listSemGrupo.Items.Remove(o);
                }

                if(aRemover.Count > 1)
                    MessageBox.Show("Movimentado " + aRemover.Count + " outros contatos por conter o mesmo número de celular.", "Informação",MessageBoxButtons.OK,MessageBoxIcon.Information);
                else if (aRemover.Count == 1)
                    MessageBox.Show("Movimentado " + aRemover.Count + " contato a mais por conter o mesmo número de celular.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }

            atualizaTotal();
        }

        private void buttonAdicionarAll_Click(object sender, EventArgs e)
        {
            //inseri os valores na lista 2
            foreach (Object listBoxItem in listSemGrupo.Items)
                listComGrupo.Items.Add(listBoxItem);

            //remover os valores da lista 1
            listSemGrupo.Items.Clear();

            atualizaTotal();
        }

        private void buttonRemover1_Click(object sender, EventArgs e)
        {
            if (listComGrupo.SelectedIndex >= 0)
            {
                Object selecionado = listComGrupo.SelectedItem;
                listSemGrupo.Items.Add(selecionado);
                listComGrupo.Items.Remove(selecionado);

                string telefoneArrastado = selecionado.ToString().Substring(selecionado.ToString().IndexOf(" <"), +
                        (selecionado.ToString().Length - selecionado.ToString().IndexOf(" <"))).Replace("<", "").Replace(">", "").Replace(" ", "");

                int count = listComGrupo.Items.Count;
                List<object> aRemover = new List<object>();

                for (int i = 0; i < count; i++)
                {
                    Object listBoxItem = listComGrupo.Items[i];
                    string telefone = listBoxItem.ToString().Substring(listBoxItem.ToString().IndexOf(" <"), +
                            (listBoxItem.ToString().Length - listBoxItem.ToString().IndexOf(" <"))).Replace("<", "").Replace(">", "").Replace(" ", "");
                    if (telefone.Equals(telefoneArrastado))
                    {
                        listSemGrupo.Items.Add(listBoxItem);
                        aRemover.Add(listBoxItem);
                    }

                }

                foreach (object o in aRemover)
                {
                    listComGrupo.Items.Remove(o);
                }

                if (aRemover.Count > 1)
                    MessageBox.Show("Movimentado " + aRemover.Count + " outros contatos por conter o mesmo número de celular.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else if(aRemover.Count == 1)
                    MessageBox.Show("Movimentado " + aRemover.Count + " contato a mais por conter o mesmo número de celular.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }

        }

        private void buttonRemoverAll_Click(object sender, EventArgs e)
        {
            //inseri os valores na lista 2
            foreach (Object listBoxItem in listComGrupo.Items)
                listSemGrupo.Items.Add(listBoxItem);


            //remover os valores da lista 1
            listComGrupo.Items.Clear();

            atualizaTotal();
        }

        private void buttonSalvar_Click(object sender, EventArgs e)
        {
            
            buttonSalvar.Enabled = false;
            buttonAdicionar1.Enabled = false;
            buttonAdicionarAll.Enabled = false;
            buttonRemover1.Enabled = false;
            buttonRemoverAll.Enabled = false;
            if (textNomeGrupo.Text == "")
            {
                MessageBox.Show("Necessário preencher o nome do Grupo", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                buttonSalvar.Enabled = true;
                buttonAdicionar1.Enabled = true;
                buttonAdicionarAll.Enabled = true;
                buttonRemover1.Enabled = true;
                buttonRemoverAll.Enabled = true;
            }
            else
            {
                SQLiteConnection connection = BD.connection();
                BD.executaQuery("INSERT INTO GRUPOS(Nome) VALUES('" + textNomeGrupo.Text + "')");
                SQLiteDataReader sldr = BD.retornaQuery("SELECT ID_Grupo FROM Grupos WHERE Nome LIKE '" + textNomeGrupo.Text + "'");
                string id_grupo = sldr["ID_Grupo"].ToString();

                // Index do contato que esta sendo adicionado
                int index = 0;

                // Quantidade de contatos total
                int size = listComGrupo.Items.Count;

                /**
                 *
                 * Thread para aumentar a combobox e atualizar a label     
                 *
                 */
                Console.WriteLine("Seram realizadas " + listComGrupo.Items.Count + " querys");
                labelCarregamento.Text = "Abrindo canal,aguarde...";
                new Thread(() =>
                {
                    try {
                        while (sendingThread.IsAlive)
                        {
                            double porcentagem = ((double)index / (double)size) * 100;
                            this.Invoke((MethodInvoker)delegate
                            {
                                Console.WriteLine(index + "/" + size);
                                if (porcentagem != 0)
                                    labelCarregamento.Text = "Salvando a lista... " + (int)porcentagem + "%";
                                progressBar.Value = ((int)porcentagem);
                            });

                            Thread.Sleep(1000);
                        }
                    }catch(Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }).Start();

                /**
                 *
                 * Thread para dar update no grupo dos contatos    
                 *
                 */
                sendingThread = new Thread(() =>
                {

                    string nome = null;
                    string telefone = null;
                    string[] itens = new string[listComGrupo.Items.Count];
                    listComGrupo.Items.CopyTo(itens, 0);
                    string textQuery = string.Empty;
                    long milliseconds = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                    int indexResetable = 0;

                    Console.WriteLine(itens.Length);

                    BD.executaQuery("begin");
                    foreach (Object listBoxItem in itens)
                    {
                        indexResetable++;
                        nome = listBoxItem.ToString().Substring(0, listBoxItem.ToString().IndexOf(" <"));
                        telefone = listBoxItem.ToString().Substring(listBoxItem.ToString().IndexOf(" <"), +
                            (listBoxItem.ToString().Length - listBoxItem.ToString().IndexOf(" <"))).Replace("<", "").Replace(">", "").Replace(" ", "");
                        textQuery += "UPDATE Contatos SET ID_GRUPO =  " + id_grupo +
                                        " WHERE Nome LIKE '" + nome + "' AND Telefone LIKE '" + telefone + "';";
                        if (itens.Length >= 250)
                        {
                            if (indexResetable == 250)
                            {

                                BD.executaQuery(textQuery);

                                textQuery = string.Empty;
                                index += indexResetable;
                                indexResetable = 0;
                            }
                        }
                        else
                        {
                            BD.executaQuery(textQuery);
                            textQuery = string.Empty;
                        }
                            
                    }
                    BD.executaQuery("end");
                    long milliseconds2 = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                    Console.WriteLine((milliseconds2 - milliseconds) / 10 + " segundos para query ");
                   

                    buttonSalvar.BeginInvoke((Action)delegate ()
                    {
                        buttonSalvar.Enabled = true;
                        buttonAdicionar1.Enabled = true;
                        buttonAdicionarAll.Enabled = true;
                        buttonRemover1.Enabled = true;
                        buttonRemoverAll.Enabled = true;
                        this.Close();
                    });
                    

                });
                sendingThread.Start();
            }    
        }

        private void Adicionar_Grupo_FormClosing(object sender, FormClosingEventArgs e)
        {
            Grupos obj = (Grupos)Application.OpenForms["Grupos"];
            obj.Grupos_Load(sender, e);
            if (sendingThread != null && sendingThread.IsAlive)
                sendingThread.Abort();
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            if (sendingThread != null && sendingThread.IsAlive)
                sendingThread.Abort();
            this.Close();
        }
    }
}
