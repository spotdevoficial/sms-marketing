﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using System.Drawing;
using System.IO.Ports;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Threading;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Text.RegularExpressions;
using System.Runtime.Remoting.Channels;
using GsmComm.PduConverter;
using GsmComm.PduConverter.SmartMessaging;
using GsmComm.GsmCommunication;
using GsmComm.Interfaces;
using GsmComm.Server;
using System.Globalization;
using System.Windows.Forms;
using System.Management;
using System.Text.RegularExpressions;
using ComponentFactory.Krypton.Toolkit;


namespace SMS
{
    public partial class AparelhosForm : Form
    {
        string[] ports = null;
        bool carregado = false;

        public AparelhosForm()
        {
            InitializeComponent();
            gridAparelhos.Sort(new AlphanumComparatorFast());
        }

        private void Aparelhos_Load(object sender, EventArgs e)
        {
            adicionaPortas();
            teste();
        }

        private void adicionaPortas()
        {
            ports = SerialPort.GetPortNames();
            List<Aparelho> portasSalvas = BD.listAparelhosSalvos();
            foreach (string port in ports)
            {

                int index = portasSalvas.FindIndex(f => f.com == (port));

                this.gridAparelhos.Rows.Add(
                    index >= 0 ? 1 : 0,
                    Properties.Resources.ModemOk,
                    port,
                    null,
                    null,
                    0
                );
            }
            
            bwsAparelho.RunWorkerAsync();

        }

        private void buttonSalvar_Click(object sender, EventArgs e)
        {

            if(carregado == false)
            {
                MessageBox.Show("Aguarde! Os aparelhos ainda estão sendo carregados.", "Aguarde!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            BD.executaQuery("DELETE FROM Dispositivos");
            foreach (DataGridViewRow dr in gridAparelhos.Rows)
            {
                if (bool.Parse(dr.Cells[0].EditedFormattedValue.ToString()))
                {
                    string enviados = null;
                    if (dr.Cells[6].Value == null)
                        enviados = "0";
                    else
                        enviados = dr.Cells[6].Value.ToString(); ;
                    BD.executaQuery("INSERT INTO Dispositivos(Porta, limite ) VALUES ('" + dr.Cells[2].Value.ToString() + "', '" + enviados + "')");
                }
            }
        }


        private void gridContatos_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (gridAparelhos.CurrentRow == null)
            {
                return;
            }

            if (string.IsNullOrEmpty((string)gridAparelhos.CurrentRow.Cells[3].Value))
            {
                kryptonTextBox1.Text = String.Empty;
                kryptonTextBox2.Text = String.Empty;
                kryptonTextBox3.Text = String.Empty;
                kryptonTextBox4.Text = String.Empty;
                kryptonTextBox5.Text = String.Empty;
                return;
            }

            GsmCommMain comm;
            string cod1 = gridAparelhos.CurrentRow.Cells["Nome"].Value.ToString();
            SerialPort teste = new SerialPort();
            teste.PortName = cod1;

            kryptonTextBox1.Text = String.Empty;
            kryptonTextBox2.Text = String.Empty;
            kryptonTextBox3.Text = String.Empty;
            kryptonTextBox4.Text = String.Empty;
            kryptonTextBox5.Text = String.Empty;

            try
            {
                comm = new GsmCommMain(cod1, 9600, 200);
                comm.Open();
                IdentificationInfo info = comm.IdentifyDevice();
                try
                {
                    SignalQualityInfo info2 = comm.GetSignalQuality();
                    kryptonTextBox5.Text = info2.SignalStrength.ToString() + "%";
                }
                catch (Exception ex)
                {

                }

                kryptonTextBox1.Text = info.SerialNumber;
                kryptonTextBox2.Text = info.Model;
                kryptonTextBox3.Text = info.Manufacturer;
                kryptonTextBox4.Text = info.Revision;

                comm.Close();
            }
            catch
            {
            }

        }

        private void teste()
        {

            buttonExcluir.Enabled = false;
        }

        private void customSortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            int a = int.Parse(e.CellValue1.ToString().Replace("COM","")), b = int.Parse(e.CellValue2.ToString().Replace("COM", ""));

            e.SortResult = a.CompareTo(b);

            e.Handled = true;
        }

        private void bwsAparelho_DoWork(object sender, DoWorkEventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            int count = gridAparelhos.Rows.Count;
            int index = 0;
            foreach (DataGridViewRow row in gridAparelhos.Rows)
            {
                int tentativa = 0;
                new Thread(()=>
                {
                    checkAparelhoValues(row, ref index, ref count, ref tentativa);
                    
                }).Start();
            }

            
        }

        public void checkAparelhoValues(DataGridViewRow row, ref int index, ref int count, ref int tentativa)
        {
            string porta = row.Cells[2].Value.ToString();
            GsmCommMain comm = new GsmCommMain(porta, 9600, 300);
            try
            {

                comm.Open();
                IdentificationInfo info = comm.IdentifyDevice();

                //Cursor.Current = Cursors.Default;
                //if (MessageBox.Show(this, "No phone connected.", "Connection setup",
                //    MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation) == DialogResult.Cancel)
                //{
                Aparelho ap = BD.getAparelho(porta);
                row.Cells[3].Value = info.Model;
                row.Cells[4].Value = (comm.IsOpen()) && (string)row.Cells[4].Value != "Ocupado" ? "Pronto" : "Ocupado";
                row.Cells[5].Value = ap == null ? 0 : ap.limit;
                row.Cells[6].Value = ap == null ? 0 : ap.enviados;

                //MessageBox.Show(this, porta, "Connection setup", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                Console.WriteLine("teste6 "+ex.InnerException+" - "+ex);

                if (ex is InvalidOperationException || ex.InnerException is IOException)
                {
                    index--;
                    tentativa++;
                    Console.WriteLine("Tentativa: " + tentativa);

                    if(tentativa == 5)
                    {
                        tentativa = 0;
                        row.Cells[4].Value = "Sem sinal";
                        index++;
                        row.Cells[4].Value = null;
                        row.Cells[1].Value = Properties.Resources.PortaCOM;
                        return;
                    }

                    checkAparelhoValues(row, ref index,ref count, ref tentativa);
                    if(ex.InnerException is IOException)
                    {
                        Console.WriteLine(row.Cells[2].Value + " " + ex.Message + " " + ex.InnerException);
                        row.Cells[4].Value = "Ocupado";
                    }
                }else
                {
                    Console.WriteLine(row.Cells[2].Value+ " " +ex.Message+" "+ex.InnerException);
                    row.Cells[4].Value = null;
                    row.Cells[1].Value = Properties.Resources.PortaCOM;
                }

            }
            finally
            {

                if (comm.IsOpen())
                    comm.Close();
                index++;
                if (count == index)
                    carregado = true;

            }
        }

        private void buttonExcluir_Click(object sender, EventArgs e)
        {
            if (carregado == false)
            {
                MessageBox.Show("Aguarde! Os aparelhos ainda estão sendo carregados.", "Aguarde!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            gridAparelhos.Rows.Clear();
            adicionaPortas();
            teste();
        }

        private void bwsAparelho_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            buttonExcluir.Enabled = true;
        }


        public string ExecCommand(SerialPort port, string command, int responseTimeout, string errorMessage)
        {



            try
            {
                port.DiscardOutBuffer();
                port.DiscardInBuffer();
                //receiveNow.Reset();
                port.Write(command + "\r");

                string input = ReadResponse(port, responseTimeout);
                if ((input.Length == 0) || ((!input.EndsWith("\r\n> ")) && (!input.EndsWith("\r\nOK\r\n"))))
                    throw new ApplicationException("No success message was received.");
                return input;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AutoResetEvent receiveNow;
        private string enviados;

        public string ReadResponse(SerialPort port, int timeout)
        {
            string buffer = string.Empty;
            try
            {
                do
                {
                    if (receiveNow.WaitOne(timeout, false))
                    {
                        string t = port.ReadExisting();
                        buffer += t;
                    }
                    else
                    {
                        if (buffer.Length > 0)
                            throw new ApplicationException("Response received is incomplete.");
                        else
                            throw new ApplicationException("No data received from phone.");
                    }
                }
                while (!buffer.EndsWith("\r\nOK\r\n") && !buffer.EndsWith("\r\n> ") && !buffer.EndsWith("\r\nERROR\r\n"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return buffer;
        }

        private void gridContatos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (carregado == false)
            {
                return;
            }
            gridAparelhos.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        int first = 0;
        private void dataGridView1_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex == 0 && carregado == false && e.RowIndex != -1 &&
                string.IsNullOrEmpty((string)gridAparelhos[3, e.RowIndex].Value))
            {
                e.Cancel = true;

                if(first > 0)
                    MessageBox.Show("Aguarde! Os aparelhos ainda estão sendo carregados.", "Aguarde!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                first++;
            }

        }

        private void gridContatos_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            

            if (gridAparelhos.Columns[e.ColumnIndex] is DataGridViewTextBoxColumn && e.RowIndex != -1)
            {
                   
                string check = gridAparelhos.Rows[e.RowIndex].Cells[0].EditedFormattedValue.ToString();
                if (bool.Parse(check))
                {
                    
                    BD.executaQuery("update dispositivos set limite = " +
                        gridAparelhos.Rows[e.RowIndex].Cells[5].Value.ToString()
                        + " where Porta = '" +
                        gridAparelhos.Rows[e.RowIndex].Cells[2].Value.ToString()
                        + "';");
                }
                else
                {
                    BD.executaQuery("DELETE from dispositivos where Porta = '" + gridAparelhos.Rows[e.RowIndex].Cells[2].Value.ToString() + "';");
                }

            }

            if (gridAparelhos.Columns[e.ColumnIndex] is KryptonDataGridViewCheckBoxColumn && e.RowIndex != -1)
            {
                string check = gridAparelhos.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString();
                if (bool.Parse(check))
                {
                    gridAparelhos.Rows[e.RowIndex].Cells[1].Value = Properties.Resources.ModemOk;
                    BD.executaQuery("INSERT INTO Dispositivos(Porta, limite ) VALUES ('" +
                        gridAparelhos.Rows[e.RowIndex].Cells[2].Value.ToString() + "', '" +
                        gridAparelhos.Rows[e.RowIndex].Cells[5].Value.ToString()
                    + "')");
                }
                else
                {
                    gridAparelhos.Rows[e.RowIndex].Cells[1].Value = Properties.Resources.PortaCOM;
                    BD.executaQuery("DELETE from dispositivos where Porta = '" + gridAparelhos.Rows[e.RowIndex].Cells[2].Value.ToString() + "';");
                }

            }
        }

        private void gridAparelhos_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= new KeyPressEventHandler(limiteColum_KeyPressed);
            if (gridAparelhos.CurrentCell.ColumnIndex == 5) // 5 = Coluna do Limite
            {
                TextBox tb = e.Control as TextBox;
                if (tb != null)
                {
                    tb.KeyPress += new KeyPressEventHandler(limiteColum_KeyPressed);
                }
            }
        }

        private void limiteColum_KeyPressed(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

    }



}
