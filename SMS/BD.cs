﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Windows.Forms;

namespace SMS
{
    class BD
    {
        public static string dbConnection = "Data Source=sms.db;Version=3;New=False;Compress=True;";
        public static SQLiteConnection sqlconnection = null;

        //MÉTODO QUE RETORNA A CONEXÃO COM A BASE DE DADOS
        public static SQLiteConnection connection()
        {
            try
            {
                //Instância o sqlconnection com a string de conexão.
                if (sqlconnection == null || sqlconnection.State == ConnectionState.Closed)
                {
                    sqlconnection = new SQLiteConnection(dbConnection);
                    sqlconnection.Open();
                    Console.WriteLine(sqlconnection.State);
                }
                

                //Retorna o sqlconnection.
                return sqlconnection;

            }
            catch (SQLiteException ex)
            {
                throw ex;
            }

        }

        //MÉTODO QUE RETORNA UM DATAREADER COM O RESULTADO DA QUERY
        public static SQLiteDataReader retornaQuery(string query)
        {
            try
            {
                //Instância o SQLiteCommand com a query sql que será executada e a conexão.
                SQLiteCommand comando = new SQLiteCommand(query, connection());
                comando.CommandTimeout = 60;

                //Executa a query sql.
                var retornaQuery = comando.ExecuteReader();

                //Retorna o dataReader com o resultado
                return retornaQuery;

            }
            catch (SQLiteException ex)
            {
                throw ex;
            }

        }


        //MÉTODO QUE RETORNA O RESULTADO DA CONSULTA EM UM DATASET
        public static DataSet retornaQueryDataSet(string query)
        {
            try
            {
                //Instância o SQLiteCommand com a query sql que será executada e a conexão.
                SQLiteCommand comando = new SQLiteCommand(query, connection());

                //Instância o sqldataAdapter.
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(comando);

                //Instância o dataSet de retorno.
                DataSet dataSet = new DataSet();

                //Atualiza o dataSet
                adapter.Fill(dataSet);

                //Retorna o dataSet com o resultado da query sql.
                return dataSet;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static void SqlCommandPrepareEx(List<Contato> list)
        {
            SqlCommandPrepareEx(list, -1);
        }

        public static void SqlCommandPrepareEx(List<Contato> list, int id_grupo)
        {

            try
            {
                //Instância o SQLiteCommand com a query sql que será executada e a conexão.
                SQLiteConnection cnn = connection();
                SQLiteCommand comando = new SQLiteCommand(null, cnn);
                SQLiteTransaction trans = cnn.BeginTransaction();
                comando.Transaction = trans;

                comando.CommandText = "INSERT INTO Contatos(Nome, Telefone, ID_Grupo, Variavel1, Variavel2) " +
                "VALUES (@nome, @telefone, @grupo, @v1, @v2)";
    
                foreach (Contato contato in list)
                {
                    comando.Parameters.Add(comando.Parameters.AddWithValue("@nome", contato.nome));
                    comando.Parameters.Add(comando.Parameters.AddWithValue("@telefone", contato.telefone));
                    
                    if(id_grupo > -1)
                        comando.Parameters.Add(comando.Parameters.AddWithValue("@grupo", id_grupo));
                    else
                        comando.Parameters.Add(comando.Parameters.AddWithValue("@grupo", contato.grupo));

                    comando.Parameters.Add(comando.Parameters.AddWithValue("@v1", contato.variavel1));
                    comando.Parameters.Add(comando.Parameters.AddWithValue("@v2", contato.variavel2));

                    comando.Prepare();
                    comando.ExecuteNonQuery();
                }

                trans.Commit();


            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                throw ex;
            }

        }

        //MÉTODO QUE EXECUTA A QUERY SQL
        public static void executaQuery(string query)
        {
            try
            {
                //Instância o SQLiteCommand com a query sql que será executada e a conexão.
                SQLiteCommand comando = new SQLiteCommand(query, connection());
                comando.CommandType = CommandType.Text;
                comando.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message +"\n["+query+"]");

            }
        }

        

        public static Aparelho getAparelho(string com){
            SQLiteDataReader dr = BD.retornaQuery("SELECT Porta, limite, enviados FROM Dispositivos where Porta = '" + com + "';");
            Aparelho ap = null;

            while (dr.Read())
            {
                ap = new Aparelho();
                ap.com = dr["Porta"].ToString();
                ap.limit = int.Parse(dr[1].ToString());
                ap.enviados = int.Parse(dr[2].ToString());

            }
            dr.Close();
            return ap;
        }
        public static List<Aparelho> listAparelhosSalvos()
        {
            List<Aparelho> lista = new List<Aparelho>();
            SQLiteDataReader dr = BD.retornaQuery("SELECT Porta, limite , enviados FROM Dispositivos");


            while (dr.Read())
            {

                Aparelho ap = new Aparelho();
                ap.com = dr["Porta"].ToString();
                ap.limit = int.Parse(dr[1].ToString());

                ap.enviados = int.Parse(dr[2].ToString());

                lista.Add(ap);
            }
            dr.Close();
            return lista;
        }



         public static List<int> getCountSMS()
        {
            List<int> lista = new List<int>();
            SQLiteDataReader dr = BD.retornaQuery("SELECT  ( SELECT COUNT(*) FROM   Caixa_Saida ) AS count1, ( SELECT COUNT(*) FROM   Caixa_Recebidos ) AS count2, ( SELECT COUNT(*) FROM   Mensagens) AS count3");
            if (dr.Read())
            {
                lista.Add(dr.GetInt32(0)); // Caixa de Saida
                lista.Add(dr.GetInt32(1)); // Caixa de Entrada
                lista.Add(dr.GetInt32(2)); // Mensagens

            }
            dr.Close();
            return lista; 
        }


        //MÉTODO QUE EXECUTA A QUERY SQL E RETORNA O IDENTITY
        public static int executaQueryIdentity(string query)
        {
            try
            {

                //Instância o SQLiteCommand com a query sql que será executada e a conexão.
                SQLiteCommand comando = new SQLiteCommand(query, connection());
                comando.CommandType = CommandType.Text;

                //Executa a query sql.

                //comando.ExecuteNonQuery();
                return Convert.ToInt32(comando.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
