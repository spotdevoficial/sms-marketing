﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace SMS
{
    public partial class BlackList : Form
    {
        public BlackList()
        {
            InitializeComponent();
        }

        private void BlackList_Load(object sender, EventArgs e)
        {
            atualizaLista();
        }

        private void atualizaLista()
        {
            listNumero.Items.Clear();
            SQLiteDataReader dr = BD.retornaQuery("SELECT ID_Numero, Numero FROM BlackList");
            while (dr.Read())
                listNumero.Items.Add(dr[1].ToString());
            dr.Close();
        }

        private void kryptonButton1_Click(object sender, EventArgs e)
        {
            BD.executaQuery("INSERT INTO BlackList (Numero) "+
                "SELECT '" + textNumero.Text + "' WHERE '" + textNumero.Text + "' NOT IN (SELECT Numero FROM BlackList)");
            atualizaLista();
            textNumero.Text = String.Empty;
        }

        private void kryptonButton2_Click(object sender, EventArgs e)
        {
            try
            {
                BD.executaQuery("DELETE FROM BlackList WHERE Numero LIKE '" + listNumero.SelectedItem + "' ");
                atualizaLista();
            }
            catch { }
        }

        private void kryptonButton3_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog file = new OpenFileDialog();
                file.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                if (file.ShowDialog() == DialogResult.OK)
                {
                    StreamReader stream = new StreamReader(@file.FileName);

                    string linha = null;
                    while ((linha = stream.ReadLine()) != null)
                    {
                        string[] coluna = linha.Split(';');
                        //MessageBox.Show(coluna[0]+" - " + coluna[1] +" - " + coluna[2] + " - " + coluna[3] + "<br />");
                        BD.executaQuery("INSERT INTO BlackList (Numero) " +
                            "SELECT '" + coluna[0] + "' WHERE '" + coluna[0] + "' NOT IN (SELECT Numero FROM BlackList)");                       
                    }
                    stream.Close();
                    MessageBox.Show("Numeros adicionados a BlackList com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //Importar_CSV.leArquivo(file.FileName);
                    //BD.executaQuery("UPDATE Configuracoes_Sistema SET Imagem = '" + file.FileName + "'");
                }
                atualizaLista();
            }
            catch (Exception erro)
            {
                MessageBox.Show(erro.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void kryptonButton4_Click(object sender, EventArgs e)
        {
            try
            {
                // Displays a SaveFileDialog so the user can save the Image
                // assigned to Button2.
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Text files (*.txt)|*.txt";
                saveFileDialog1.Title = "Exportar lista de números na BlackList";
                saveFileDialog1.ShowDialog();

                if (saveFileDialog1.FileName != "")
                {
                    StreamWriter stream = new StreamWriter(saveFileDialog1.FileName, true, Encoding.ASCII);
                    foreach (Object listBoxItem in listNumero.Items)
                    {
                        stream.WriteLine(listBoxItem.ToString());
                    }
                    stream.Close();
                }
                
            }
            catch { }
        }

        
    }
}
