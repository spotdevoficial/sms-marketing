﻿namespace SMS
{
    partial class Caixa_Entrada
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Caixa_Entrada));
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.kryptonPanel1 = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.gridRecebidos = new ComponentFactory.Krypton.Toolkit.KryptonDataGridView();
            this.kryptonPanel2 = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.labelCaracteres = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.textMensagem = new ComponentFactory.Krypton.Toolkit.KryptonRichTextBox();
            this.Telefone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID_Mensagem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mensagem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Data_Recebimento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonExcluir = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel1)).BeginInit();
            this.kryptonPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridRecebidos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel2)).BeginInit();
            this.kryptonPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Custom2;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(605, 31);
            this.kryptonHeader1.TabIndex = 3;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Caixa de Entrada";
            this.kryptonHeader1.Values.Image = ((System.Drawing.Image)(resources.GetObject("kryptonHeader1.Values.Image")));
            // 
            // kryptonPanel1
            // 
            this.kryptonPanel1.Controls.Add(this.gridRecebidos);
            this.kryptonPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonPanel1.Location = new System.Drawing.Point(0, 31);
            this.kryptonPanel1.Name = "kryptonPanel1";
            this.kryptonPanel1.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.GridHeaderColumnList;
            this.kryptonPanel1.Size = new System.Drawing.Size(605, 173);
            this.kryptonPanel1.TabIndex = 5;
            // 
            // gridRecebidos
            // 
            this.gridRecebidos.AllowUserToAddRows = false;
            this.gridRecebidos.AllowUserToDeleteRows = false;
            this.gridRecebidos.AllowUserToResizeRows = false;
            this.gridRecebidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridRecebidos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Telefone,
            this.ID_Mensagem,
            this.Mensagem,
            this.Data_Recebimento});
            this.gridRecebidos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridRecebidos.GridStyles.Style = ComponentFactory.Krypton.Toolkit.DataGridViewStyle.Mixed;
            this.gridRecebidos.GridStyles.StyleBackground = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.GridHeaderColumnCustom1;
            this.gridRecebidos.Location = new System.Drawing.Point(0, 0);
            this.gridRecebidos.MultiSelect = false;
            this.gridRecebidos.Name = "gridRecebidos";
            this.gridRecebidos.RowHeadersVisible = false;
            this.gridRecebidos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridRecebidos.Size = new System.Drawing.Size(605, 173);
            this.gridRecebidos.TabIndex = 0;
            this.gridRecebidos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridRecebidos_CellClick);
            // 
            // kryptonPanel2
            // 
            this.kryptonPanel2.Controls.Add(this.buttonExcluir);
            this.kryptonPanel2.Controls.Add(this.labelCaracteres);
            this.kryptonPanel2.Controls.Add(this.kryptonLabel1);
            this.kryptonPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonPanel2.Location = new System.Drawing.Point(0, 204);
            this.kryptonPanel2.Name = "kryptonPanel2";
            this.kryptonPanel2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Blue;
            this.kryptonPanel2.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.HeaderCustom2;
            this.kryptonPanel2.Size = new System.Drawing.Size(605, 34);
            this.kryptonPanel2.TabIndex = 6;
            // 
            // labelCaracteres
            // 
            this.labelCaracteres.Location = new System.Drawing.Point(67, 7);
            this.labelCaracteres.Name = "labelCaracteres";
            this.labelCaracteres.Size = new System.Drawing.Size(17, 20);
            this.labelCaracteres.TabIndex = 1;
            this.labelCaracteres.Values.Text = "0";
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.Location = new System.Drawing.Point(3, 6);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(70, 20);
            this.kryptonLabel1.TabIndex = 0;
            this.kryptonLabel1.Values.Text = "Caracteres:";
            // 
            // textMensagem
            // 
            this.textMensagem.Location = new System.Drawing.Point(0, 236);
            this.textMensagem.Name = "textMensagem";
            this.textMensagem.Size = new System.Drawing.Size(605, 147);
            this.textMensagem.TabIndex = 7;
            this.textMensagem.Text = "";
            this.textMensagem.TextChanged += new System.EventHandler(this.textMensagem_TextChanged);
            // 
            // Telefone
            // 
            this.Telefone.DataPropertyName = "Telefone";
            this.Telefone.HeaderText = "Telefone";
            this.Telefone.Name = "Telefone";
            this.Telefone.ReadOnly = true;
            this.Telefone.Width = 200;
            // 
            // ID_Mensagem
            // 
            this.ID_Mensagem.DataPropertyName = "ID_Mensagem";
            this.ID_Mensagem.HeaderText = "ID_Mensagem";
            this.ID_Mensagem.Name = "ID_Mensagem";
            this.ID_Mensagem.ReadOnly = true;
            this.ID_Mensagem.Visible = false;
            // 
            // Mensagem
            // 
            this.Mensagem.DataPropertyName = "Mensagem";
            this.Mensagem.HeaderText = "Mensagem";
            this.Mensagem.Name = "Mensagem";
            this.Mensagem.ReadOnly = true;
            this.Mensagem.Width = 235;
            // 
            // Data_Recebimento
            // 
            this.Data_Recebimento.DataPropertyName = "Data";
            this.Data_Recebimento.HeaderText = "Data_Recebimento";
            this.Data_Recebimento.Name = "Data_Recebimento";
            this.Data_Recebimento.ReadOnly = true;
            this.Data_Recebimento.Width = 150;
            // 
            // buttonExcluir
            // 
            this.buttonExcluir.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.Gallery;
            this.buttonExcluir.Location = new System.Drawing.Point(463, 3);
            this.buttonExcluir.Name = "buttonExcluir";
            this.buttonExcluir.Size = new System.Drawing.Size(139, 25);
            this.buttonExcluir.TabIndex = 14;
            this.buttonExcluir.Values.Text = "Excluir mensagem";
            this.buttonExcluir.Click += new System.EventHandler(this.buttonExcluir_Click);
            // 
            // Caixa_Entrada
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(605, 383);
            this.Controls.Add(this.textMensagem);
            this.Controls.Add(this.kryptonPanel2);
            this.Controls.Add(this.kryptonPanel1);
            this.Controls.Add(this.kryptonHeader1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Caixa_Entrada";
            this.Text = "Caixa_Entrada";
            this.Load += new System.EventHandler(this.Caixa_Entrada_Load);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel1)).EndInit();
            this.kryptonPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridRecebidos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel2)).EndInit();
            this.kryptonPanel2.ResumeLayout(false);
            this.kryptonPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel kryptonPanel1;
        private ComponentFactory.Krypton.Toolkit.KryptonDataGridView gridRecebidos;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel kryptonPanel2;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel1;
        private ComponentFactory.Krypton.Toolkit.KryptonRichTextBox textMensagem;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel labelCaracteres;
        private System.Windows.Forms.DataGridViewTextBoxColumn Telefone;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_Mensagem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mensagem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data_Recebimento;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonExcluir;
    }
}