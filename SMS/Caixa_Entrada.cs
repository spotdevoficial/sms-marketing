﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SMS
{
    public partial class Caixa_Entrada : Form
    {
        BindingSource bs = new BindingSource();
        int cod1 = 0;

        public Caixa_Entrada()
        {
            InitializeComponent();
        }

        private void Caixa_Entrada_Load(object sender, EventArgs e)
        {
            atulizaLista();
        }

        private void atulizaLista()
        {
            //define um objeto DataSet
            DataSet ds = BD.retornaQueryDataSet("SELECT ID_Mensagem, Telefone, Data, Mensagem FROM Caixa_Recebidos ORDER BY Data DESC LIMIT 100 ");
            //da.Fill(ds);
            //atribui o dataset ao DataSource do BindingSource
            bs.DataSource = ds;
            //atribui o BindingSource ao BindingNavigator
            bs.DataMember = ds.Tables[0].TableName;
            //Atribui o BindingSource ao DataGridView
            //gridPropriedades.DataSource = bs;
            gridRecebidos.DataSource = bs;
        }

        private void gridRecebidos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            cod1 = Convert.ToInt32(gridRecebidos.CurrentRow.Cells["ID_Mensagem"].Value.ToString());
            SQLiteDataReader dr = BD.retornaQuery("SELECT ID_Mensagem, Mensagem FROM Caixa_Recebidos WHERE ID_Mensagem = '" + cod1 + "' ");
            while (dr.Read())
            {
                textMensagem.Text = dr[1].ToString();
            } dr.Close();
                
        }

        private void textMensagem_TextChanged(object sender, EventArgs e)
        {
            labelCaracteres.Text = textMensagem.TextLength.ToString();
            if (Convert.ToInt32(textMensagem.TextLength.ToString()) == 160)
            {
                labelCaracteres.Text = labelCaracteres.Text + " (MÁXIMO)";
                labelCaracteres.ForeColor = Color.Red;
            }
            else
            {
                labelCaracteres.ForeColor = Color.Black;
                labelCaracteres.Text = textMensagem.TextLength.ToString();
            }  
        }

        private void buttonExcluir_Click(object sender, EventArgs e)
        {
            BD.executaQuery("DELETE FROM Caixa_Recebidos WHERE ID_Mensagem = '" + cod1 + "' ");
            atulizaLista();
            textMensagem.Text = "";
        }
    }
}
