﻿namespace SMS
{
    partial class Caixa_Enviados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Caixa_Enviados));
            this.gridEnviados = new ComponentFactory.Krypton.Toolkit.KryptonDataGridView();
            this.Telefone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID_Mensagem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mensagem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Data_Enviada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kryptonPanel1 = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.kryptonButton1 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonExportar = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gridEnviados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel1)).BeginInit();
            this.kryptonPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridEnviados
            // 
            this.gridEnviados.AllowUserToAddRows = false;
            this.gridEnviados.AllowUserToDeleteRows = false;
            this.gridEnviados.AllowUserToResizeRows = false;
            this.gridEnviados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridEnviados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Telefone,
            this.ID_Mensagem,
            this.Mensagem,
            this.Data_Enviada,
            this.Status});
            this.gridEnviados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEnviados.GridStyles.Style = ComponentFactory.Krypton.Toolkit.DataGridViewStyle.Mixed;
            this.gridEnviados.GridStyles.StyleBackground = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.GridHeaderColumnCustom1;
            this.gridEnviados.Location = new System.Drawing.Point(0, 0);
            this.gridEnviados.MultiSelect = false;
            this.gridEnviados.Name = "gridEnviados";
            this.gridEnviados.RowHeadersVisible = false;
            this.gridEnviados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridEnviados.Size = new System.Drawing.Size(605, 347);
            this.gridEnviados.TabIndex = 0;
            // 
            // Telefone
            // 
            this.Telefone.DataPropertyName = "Destinatario";
            this.Telefone.HeaderText = "Telefone";
            this.Telefone.Name = "Telefone";
            this.Telefone.ReadOnly = true;
            this.Telefone.Width = 135;
            // 
            // ID_Mensagem
            // 
            this.ID_Mensagem.DataPropertyName = "ID_Mensagem";
            this.ID_Mensagem.HeaderText = "ID_Mensagem";
            this.ID_Mensagem.Name = "ID_Mensagem";
            this.ID_Mensagem.ReadOnly = true;
            this.ID_Mensagem.Visible = false;
            // 
            // Mensagem
            // 
            this.Mensagem.DataPropertyName = "Mensagem";
            this.Mensagem.HeaderText = "Mensagem";
            this.Mensagem.Name = "Mensagem";
            this.Mensagem.ReadOnly = true;
            this.Mensagem.Width = 150;
            // 
            // Data_Enviada
            // 
            this.Data_Enviada.DataPropertyName = "Data";
            this.Data_Enviada.HeaderText = "Data de Envio";
            this.Data_Enviada.Name = "Data_Enviada";
            this.Data_Enviada.ReadOnly = true;
            this.Data_Enviada.Width = 150;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.Width = 150;
            // 
            // kryptonPanel1
            // 
            this.kryptonPanel1.Controls.Add(this.gridEnviados);
            this.kryptonPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kryptonPanel1.Location = new System.Drawing.Point(0, 36);
            this.kryptonPanel1.Name = "kryptonPanel1";
            this.kryptonPanel1.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.GridHeaderColumnList;
            this.kryptonPanel1.Size = new System.Drawing.Size(605, 347);
            this.kryptonPanel1.TabIndex = 14;
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Custom2;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(605, 36);
            this.kryptonHeader1.TabIndex = 13;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Mensagens Enviadas";
            this.kryptonHeader1.Values.Image = ((System.Drawing.Image)(resources.GetObject("kryptonHeader1.Values.Image")));
            // 
            // kryptonButton1
            // 
            this.kryptonButton1.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.InputControl;
            this.kryptonButton1.Location = new System.Drawing.Point(462, 5);
            this.kryptonButton1.Name = "kryptonButton1";
            this.kryptonButton1.Size = new System.Drawing.Size(132, 25);
            this.kryptonButton1.TabIndex = 16;
            this.kryptonButton1.Values.Text = "Limpar itens enviados";
            this.kryptonButton1.Click += new System.EventHandler(this.kryptonButton1_Click);
            // 
            // buttonExportar
            // 
            this.buttonExportar.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.InputControl;
            this.buttonExportar.Location = new System.Drawing.Point(327, 5);
            this.buttonExportar.Name = "buttonExportar";
            this.buttonExportar.Size = new System.Drawing.Size(129, 25);
            this.buttonExportar.TabIndex = 153;
            this.buttonExportar.Values.Text = "Exportar Relatório";
            this.buttonExportar.Click += new System.EventHandler(this.buttonExportar_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Caixa_Enviados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(605, 383);
            this.Controls.Add(this.buttonExportar);
            this.Controls.Add(this.kryptonButton1);
            this.Controls.Add(this.kryptonPanel1);
            this.Controls.Add(this.kryptonHeader1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Caixa_Enviados";
            this.Text = "Caixa_Enviados";
            this.Load += new System.EventHandler(this.Caixa_Enviados_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridEnviados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel1)).EndInit();
            this.kryptonPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonDataGridView gridEnviados;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel kryptonPanel1;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Telefone;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_Mensagem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mensagem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data_Enviada;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonExportar;
        private System.Windows.Forms.Timer timer1;
    }
}