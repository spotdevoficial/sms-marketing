﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace SMS
{
    public partial class Caixa_Enviados : Form
    {
        BindingSource bs = new BindingSource();

        public Caixa_Enviados()
        {
            InitializeComponent();
        }

        private void Caixa_Enviados_Load(object sender, EventArgs e)
        {
            atulizaLista();
        }

        private void atulizaLista()
        {
            //define um objeto DataSet
            DataSet ds = BD.retornaQueryDataSet("SELECT ID_Mensagem, Mensagem, Destinatario, Data, Status FROM Mensagens ORDER BY Data DESC");
            //da.Fill(ds);
            //atribui o dataset ao DataSource do BindingSource
            bs.DataSource = ds;
            //atribui o BindingSource ao BindingNavigator
            bs.DataMember = ds.Tables[0].TableName;
            //Atribui o BindingSource ao DataGridView
            //gridPropriedades.DataSource = bs;
            gridEnviados.DataSource = bs;

            kryptonHeader1.Text = "Mensagens Enviadas (" + gridEnviados.RowCount + ")";
        }

        private void buttonAtualizar_Click(object sender, EventArgs e)
        {
            atulizaLista();
        }

        private void kryptonButton1_Click(object sender, EventArgs e)
        {
            BD.executaQuery("DELETE FROM Mensagens");
            atulizaLista();
        }

        private void buttonExportar_Click(object sender, EventArgs e)
        {
            string textEmpresa = null;
            string textCNPJ = null;
            string textEndereco = null;
            string textTelefone = null;

            try
            {
                SQLiteDataReader dr = BD.retornaQuery("SELECT Nome_Empresa, CNPJ, Endereco, Telefone FROM Configuracoes_Sistema");
                while (dr.Read())
                {
                    textEmpresa = dr[0].ToString();
                    textCNPJ = dr[1].ToString();
                    textEndereco = dr[2].ToString();
                    textTelefone = dr[3].ToString();
                } dr.Close();

                //Creating iTextSharp Table from the DataTable data
                PdfPTable pdfTable = new PdfPTable(gridEnviados.ColumnCount);
                pdfTable.DefaultCell.Padding = 3;
                pdfTable.WidthPercentage = 100;
                pdfTable.HorizontalAlignment = Element.ALIGN_LEFT;
                pdfTable.DefaultCell.BorderWidth = 1;


                //adiciona cabeçalho
                Paragraph Empresa = new Paragraph();

                //alinha do titulo e subtitulo
                Empresa.Alignment = 1;

                //adiciona o texto ao titulo e subtitulo
                Chunk tituloEmpresa = new Chunk(textEmpresa, FontFactory.GetFont("dax-black"));
                Phrase phrase1 = new Phrase(tituloEmpresa);
                Chunk subCNPJ = new Chunk("\n" + "CNPJ: " + textCNPJ);
                Phrase phrase2 = new Phrase(subCNPJ);
                Chunk subEndereco = new Chunk("\n" + "Endereço: " + textEndereco);
                Phrase phrase3 = new Phrase(subEndereco);
                Chunk subTelefone = new Chunk("\n" + "Telefone: " + textTelefone);
                Phrase phrase4 = new Phrase(subTelefone);
                Chunk subEspaco = new Chunk("\n" + " ");
                Phrase phrase5 = new Phrase(subEspaco);

                Empresa.Add(phrase1);
                Empresa.Add(phrase2);
                Empresa.Add(phrase3);
                Empresa.Add(phrase4);
                Empresa.Add(phrase5);

                //Adding Header row
                foreach (DataGridViewColumn column in gridEnviados.Columns)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText));
                    cell.BackgroundColor = new iTextSharp.text.Color(240, 240, 240);
                    pdfTable.AddCell(cell);
                }

                //Adding DataRow
                foreach (DataGridViewRow row in gridEnviados.Rows)
                {
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        pdfTable.AddCell(cell.Value.ToString());
                    }
                }

                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "PDF|*.pdf";
                saveFileDialog1.Title = "Exportar lista de mensagens enviadas";
                saveFileDialog1.ShowDialog();
                if (saveFileDialog1.FileName != "")
                {
                    System.IO.FileStream fs = (System.IO.FileStream)saveFileDialog1.OpenFile();
                    using (fs)
                    {
                        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                        PdfWriter.GetInstance(pdfDoc, fs);
                        pdfDoc.Open();
                        pdfDoc.Add(Empresa);
                        pdfDoc.Add(pdfTable);
                        pdfDoc.Close();
                        fs.Close();
                    }
                }
            }
            catch (Exception erro)
            {
                MessageBox.Show(erro.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                int scrollPosition = gridEnviados.FirstDisplayedScrollingRowIndex;
                atulizaLista();

                if (gridEnviados.RowCount > 0)
                    gridEnviados.FirstDisplayedScrollingRowIndex = scrollPosition;
            }
            catch
            {

            }
        }

        
    }
}
