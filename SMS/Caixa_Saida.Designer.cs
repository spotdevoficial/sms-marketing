﻿namespace SMS
{
    partial class Caixa_Saida
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Caixa_Saida));
            this.textMensagem = new ComponentFactory.Krypton.Toolkit.KryptonRichTextBox();
            this.kryptonPanel1 = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.gridCaixaSaida = new ComponentFactory.Krypton.Toolkit.KryptonDataGridView();
            this.Telefone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID_Mensagem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mensagem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Data_Criada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.limparCaixaSaidaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kryptonPanel2 = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.buttonExcluir = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonSalvar = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.labelCaracteres = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.timer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel1)).BeginInit();
            this.kryptonPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCaixaSaida)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel2)).BeginInit();
            this.kryptonPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // textMensagem
            // 
            this.textMensagem.Location = new System.Drawing.Point(0, 236);
            this.textMensagem.MaxLength = 160;
            this.textMensagem.Name = "textMensagem";
            this.textMensagem.Size = new System.Drawing.Size(605, 147);
            this.textMensagem.TabIndex = 11;
            this.textMensagem.Text = "";
            this.textMensagem.TextChanged += new System.EventHandler(this.textMensagem_TextChanged);
            // 
            // kryptonPanel1
            // 
            this.kryptonPanel1.AutoSize = true;
            this.kryptonPanel1.Controls.Add(this.gridCaixaSaida);
            this.kryptonPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonPanel1.Location = new System.Drawing.Point(0, 31);
            this.kryptonPanel1.Name = "kryptonPanel1";
            this.kryptonPanel1.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.GridHeaderColumnList;
            this.kryptonPanel1.Size = new System.Drawing.Size(605, 176);
            this.kryptonPanel1.TabIndex = 9;
            // 
            // gridCaixaSaida
            // 
            this.gridCaixaSaida.AllowUserToAddRows = false;
            this.gridCaixaSaida.AllowUserToDeleteRows = false;
            this.gridCaixaSaida.AllowUserToResizeRows = false;
            this.gridCaixaSaida.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridCaixaSaida.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Telefone,
            this.ID_Mensagem,
            this.Mensagem,
            this.Data_Criada});
            this.gridCaixaSaida.ContextMenuStrip = this.contextMenuStrip1;
            this.gridCaixaSaida.GridStyles.Style = ComponentFactory.Krypton.Toolkit.DataGridViewStyle.Mixed;
            this.gridCaixaSaida.GridStyles.StyleBackground = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.GridHeaderColumnCustom1;
            this.gridCaixaSaida.Location = new System.Drawing.Point(0, 0);
            this.gridCaixaSaida.MultiSelect = false;
            this.gridCaixaSaida.Name = "gridCaixaSaida";
            this.gridCaixaSaida.RowHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.gridCaixaSaida.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.gridCaixaSaida.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridCaixaSaida.Size = new System.Drawing.Size(605, 173);
            this.gridCaixaSaida.TabIndex = 0;
            this.gridCaixaSaida.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridCaixaSaida_CellClick);
            // 
            // Telefone
            // 
            this.Telefone.DataPropertyName = "Telefone";
            this.Telefone.HeaderText = "Telefone";
            this.Telefone.Name = "Telefone";
            this.Telefone.ReadOnly = true;
            this.Telefone.Width = 135;
            // 
            // ID_Mensagem
            // 
            this.ID_Mensagem.DataPropertyName = "ID_Mensagem";
            this.ID_Mensagem.HeaderText = "ID_Mensagem";
            this.ID_Mensagem.Name = "ID_Mensagem";
            this.ID_Mensagem.ReadOnly = true;
            this.ID_Mensagem.Visible = false;
            // 
            // Mensagem
            // 
            this.Mensagem.DataPropertyName = "Mensagem";
            this.Mensagem.HeaderText = "Mensagem";
            this.Mensagem.Name = "Mensagem";
            this.Mensagem.ReadOnly = true;
            this.Mensagem.Width = 250;
            // 
            // Data_Criada
            // 
            this.Data_Criada.DataPropertyName = "Data";
            this.Data_Criada.HeaderText = "Data de Criação";
            this.Data_Criada.Name = "Data_Criada";
            this.Data_Criada.ReadOnly = true;
            this.Data_Criada.Width = 200;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.limparCaixaSaidaToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(188, 26);
            // 
            // limparCaixaSaidaToolStripMenuItem
            // 
            this.limparCaixaSaidaToolStripMenuItem.Name = "limparCaixaSaidaToolStripMenuItem";
            this.limparCaixaSaidaToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.limparCaixaSaidaToolStripMenuItem.Text = "Limpar caixa de Saída";
            this.limparCaixaSaidaToolStripMenuItem.Click += new System.EventHandler(this.limparCaixaSaidaToolStripMenuItem_Click);
            // 
            // kryptonHeader1
            // 
            Desktop.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            Desktop.kryptonHeader1.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Custom2;
            Desktop.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            Desktop.kryptonHeader1.Name = "kryptonHeader1";
            Desktop.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Blue;
            Desktop.kryptonHeader1.Size = new System.Drawing.Size(605, 31);
            Desktop.kryptonHeader1.TabIndex = 8;
            Desktop.kryptonHeader1.Values.Description = "";
            Desktop.kryptonHeader1.Values.Image = ((System.Drawing.Image)(resources.GetObject("kryptonHeader1.Values.Image")));
            // 
            // kryptonPanel2
            // 
            this.kryptonPanel2.Controls.Add(this.buttonExcluir);
            this.kryptonPanel2.Controls.Add(this.buttonSalvar);
            this.kryptonPanel2.Controls.Add(this.labelCaracteres);
            this.kryptonPanel2.Controls.Add(this.kryptonLabel1);
            this.kryptonPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonPanel2.Location = new System.Drawing.Point(0, 207);
            this.kryptonPanel2.Name = "kryptonPanel2";
            this.kryptonPanel2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Blue;
            this.kryptonPanel2.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.HeaderCustom2;
            this.kryptonPanel2.Size = new System.Drawing.Size(605, 34);
            this.kryptonPanel2.TabIndex = 12;
            // 
            // buttonExcluir
            // 
            this.buttonExcluir.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.Gallery;
            this.buttonExcluir.Location = new System.Drawing.Point(309, 4);
            this.buttonExcluir.Name = "buttonExcluir";
            this.buttonExcluir.Size = new System.Drawing.Size(139, 25);
            this.buttonExcluir.TabIndex = 13;
            this.buttonExcluir.Values.Text = "Excluir mensagem";
            this.buttonExcluir.Click += new System.EventHandler(this.buttonExcluir_Click);
            // 
            // buttonSalvar
            // 
            this.buttonSalvar.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.Gallery;
            this.buttonSalvar.Location = new System.Drawing.Point(454, 4);
            this.buttonSalvar.Name = "buttonSalvar";
            this.buttonSalvar.Size = new System.Drawing.Size(139, 25);
            this.buttonSalvar.TabIndex = 12;
            this.buttonSalvar.Values.Text = "Modificar mensagem";
            this.buttonSalvar.Click += new System.EventHandler(this.buttonSalvar_Click);
            // 
            // labelCaracteres
            // 
            this.labelCaracteres.Location = new System.Drawing.Point(67, 7);
            this.labelCaracteres.Name = "labelCaracteres";
            this.labelCaracteres.Size = new System.Drawing.Size(17, 20);
            this.labelCaracteres.TabIndex = 1;
            this.labelCaracteres.Values.Text = "0";
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.Location = new System.Drawing.Point(3, 6);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(70, 20);
            this.kryptonLabel1.TabIndex = 0;
            this.kryptonLabel1.Values.Text = "Caracteres:";
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 800;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // Caixa_Saida
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(605, 383);
            this.Controls.Add(this.kryptonPanel2);
            this.Controls.Add(this.textMensagem);
            this.Controls.Add(this.kryptonPanel1);
            this.Controls.Add(Desktop.kryptonHeader1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Caixa_Saida";
            this.Text = "Caixa_Saida";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Caixa_Saida_FormClosing);
            this.Load += new System.EventHandler(this.Caixa_Saida_Load);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel1)).EndInit();
            this.kryptonPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCaixaSaida)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel2)).EndInit();
            this.kryptonPanel2.ResumeLayout(false);
            this.kryptonPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonRichTextBox textMensagem;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel kryptonPanel1;
        private ComponentFactory.Krypton.Toolkit.KryptonDataGridView gridCaixaSaida;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel kryptonPanel2;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel labelCaracteres;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Telefone;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_Mensagem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mensagem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data_Criada;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonSalvar;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonExcluir;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem limparCaixaSaidaToolStripMenuItem;
        public System.Windows.Forms.Timer timer;
    }
}