﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SMS
{
    public partial class Caixa_Saida : Form
    {
        BindingSource bs = new BindingSource();
        int cod1 = 0;

        public Caixa_Saida()
        {
            InitializeComponent();
        }

        private void Caixa_Saida_Load(object sender, EventArgs e)
        {
            atulizaLista();
            textMensagem.Enabled = false;
            buttonSalvar.Enabled = false;
        }

        private void atulizaLista()
        {
            //define um objeto DataSet
            DataSet ds = BD.retornaQueryDataSet("SELECT Telefone, ID_Mensagem, Mensagem, Data FROM Caixa_Saida ORDER BY ID_Mensagem");
            //da.Fill(ds);
            //atribui o dataset ao DataSource do BindingSource
            bs.DataSource = ds;
            //atribui o BindingSource ao BindingNavigator
            bs.DataMember = ds.Tables[0].TableName;
            //Atribui o BindingSource ao DataGridView
            //gridPropriedades.DataSource = bs;
            gridCaixaSaida.DataSource = bs;
        }

        private void gridCaixaSaida_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            buttonSalvar.Enabled = true;
            if (gridCaixaSaida.CurrentRow == null)
                return;
            cod1 = Convert.ToInt32(gridCaixaSaida.CurrentRow.Cells["ID_Mensagem"].Value.ToString());
            SQLiteDataReader dr = BD.retornaQuery("SELECT ID_Mensagem, Mensagem FROM Caixa_Saida WHERE ID_Mensagem = '"+cod1+"' ");
            while (dr.Read())
                textMensagem.Text = dr[1].ToString();
            dr.Close();
        }

        private void textMensagem_TextChanged(object sender, EventArgs e)
        {
            labelCaracteres.Text = textMensagem.TextLength.ToString();
            if (Convert.ToInt32(textMensagem.TextLength.ToString()) == 160)
            {
                labelCaracteres.Text = labelCaracteres.Text + " (MÁXIMO)";
                labelCaracteres.ForeColor = Color.Red;
            }       
            else
            {
                labelCaracteres.ForeColor = Color.Black;
                labelCaracteres.Text = textMensagem.TextLength.ToString();
            }      
        }

        private void buttonSalvar_Click(object sender, EventArgs e)
        {
            if (buttonSalvar.Text == "Modificar mensagem")
            {
                textMensagem.Enabled = true;
                buttonSalvar.Text = "Salvar";
            }
            else
            {
                textMensagem.Enabled = false;
                buttonSalvar.Text = "Modificar mensagem";
                BD.executaQuery("UPDATE Caixa_Saida SET Mensagem = '" + textMensagem.Text + "' WHERE ID_Mensagem = '" + cod1 + "' ");
            }
        }

        private void buttonExcluir_Click(object sender, EventArgs e)
        {
            BD.executaQuery("DELETE FROM Caixa_Saida WHERE ID_Mensagem = '" + cod1 + "' ");
            atulizaLista();
            textMensagem.Text = "";
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            int scrollPosition = gridCaixaSaida.FirstDisplayedScrollingRowIndex;
            atulizaLista();

            if(gridCaixaSaida.RowCount > 0)
            gridCaixaSaida.FirstDisplayedScrollingRowIndex = scrollPosition;
        }

        private void limparCaixaSaidaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            BD.executaQuery("DELETE FROM Caixa_Saida");
            atulizaLista();
            this.Cursor = Cursors.Default;
        }

        private void formCLosing(object sender, FormClosingEventArgs e)
        {
            //this.r
        }

        public void Caixa_Saida_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Controls.Remove(Desktop.kryptonHeader1);
        }
    }
}
