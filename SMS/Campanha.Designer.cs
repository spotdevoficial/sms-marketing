﻿namespace SMS
{
    partial class Campanha
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Campanha));
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.kryptonPanel1 = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.comboGrupo = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.kryptonLabel2 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.textNumeros = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonPanel2 = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.kryptonLinkLabel3 = new ComponentFactory.Krypton.Toolkit.KryptonLinkLabel();
            this.kryptonLinkLabel2 = new ComponentFactory.Krypton.Toolkit.KryptonLinkLabel();
            this.kryptonLinkLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLinkLabel();
            this.labelCaracter = new System.Windows.Forms.Label();
            this.kryptonLabel4 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            textMensagem = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.buttonLimpar = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.ttpGeral = new System.Windows.Forms.ToolTip(this.components);
            this.kryptonButton1 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.chkButtonCaracteres = new ComponentFactory.Krypton.Toolkit.KryptonCheckButton();
            this.bwsCaixaSaida = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel1)).BeginInit();
            this.kryptonPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboGrupo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel2)).BeginInit();
            this.kryptonPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Custom2;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(605, 31);
            this.kryptonHeader1.TabIndex = 1;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Campanha";
            this.kryptonHeader1.Values.Image = ((System.Drawing.Image)(resources.GetObject("kryptonHeader1.Values.Image")));
            // 
            // kryptonPanel1
            // 
            this.kryptonPanel1.Controls.Add(this.comboGrupo);
            this.kryptonPanel1.Controls.Add(this.kryptonLabel2);
            this.kryptonPanel1.Controls.Add(this.textNumeros);
            this.kryptonPanel1.Controls.Add(this.kryptonLabel1);
            this.kryptonPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonPanel1.Location = new System.Drawing.Point(0, 31);
            this.kryptonPanel1.Name = "kryptonPanel1";
            this.kryptonPanel1.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.GridHeaderColumnList;
            this.kryptonPanel1.Size = new System.Drawing.Size(605, 68);
            this.kryptonPanel1.TabIndex = 2;
            // 
            // comboGrupo
            // 
            this.comboGrupo.DropDownWidth = 481;
            this.comboGrupo.Location = new System.Drawing.Point(73, 35);
            this.comboGrupo.Name = "comboGrupo";
            this.comboGrupo.Size = new System.Drawing.Size(471, 21);
            this.comboGrupo.TabIndex = 5;
            this.comboGrupo.SelectedIndexChanged += new System.EventHandler(this.comboGrupo_SelectedIndexChanged);
            // 
            // kryptonLabel2
            // 
            this.kryptonLabel2.Location = new System.Drawing.Point(28, 35);
            this.kryptonLabel2.Name = "kryptonLabel2";
            this.kryptonLabel2.Size = new System.Drawing.Size(47, 20);
            this.kryptonLabel2.TabIndex = 2;
            this.kryptonLabel2.Values.Text = "Grupo:";
            // 
            // textNumeros
            // 
            this.textNumeros.Location = new System.Drawing.Point(73, 9);
            this.textNumeros.Name = "textNumeros";
            this.textNumeros.Size = new System.Drawing.Size(471, 17);
            this.textNumeros.TabIndex = 1;
            this.textNumeros.Visible = false;
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.Location = new System.Drawing.Point(13, 9);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(63, 20);
            this.kryptonLabel1.TabIndex = 0;
            this.kryptonLabel1.Values.Text = "Números:";
            this.kryptonLabel1.Visible = false;
            // 
            // kryptonPanel2
            // 
            this.kryptonPanel2.Controls.Add(this.kryptonLinkLabel3);
            this.kryptonPanel2.Controls.Add(this.kryptonLinkLabel2);
            this.kryptonPanel2.Controls.Add(this.kryptonLinkLabel1);
            this.kryptonPanel2.Controls.Add(this.labelCaracter);
            this.kryptonPanel2.Controls.Add(this.kryptonLabel4);
            this.kryptonPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonPanel2.Location = new System.Drawing.Point(0, 99);
            this.kryptonPanel2.Name = "kryptonPanel2";
            this.kryptonPanel2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Blue;
            this.kryptonPanel2.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.HeaderCustom2;
            this.kryptonPanel2.Size = new System.Drawing.Size(605, 28);
            this.kryptonPanel2.TabIndex = 3;
            // 
            // kryptonLinkLabel3
            // 
            this.kryptonLinkLabel3.Location = new System.Drawing.Point(302, 3);
            this.kryptonLinkLabel3.Name = "kryptonLinkLabel3";
            this.kryptonLinkLabel3.Size = new System.Drawing.Size(90, 20);
            this.kryptonLinkLabel3.TabIndex = 4;
            this.kryptonLinkLabel3.Values.Text = "Variável Nome";
            this.kryptonLinkLabel3.LinkClicked += new System.EventHandler(this.kryptonLinkLabel3_LinkClicked);
            // 
            // kryptonLinkLabel2
            // 
            this.kryptonLinkLabel2.Location = new System.Drawing.Point(474, 3);
            this.kryptonLinkLabel2.Name = "kryptonLinkLabel2";
            this.kryptonLinkLabel2.Size = new System.Drawing.Size(70, 20);
            this.kryptonLinkLabel2.TabIndex = 3;
            this.kryptonLinkLabel2.Values.Text = "Variável 02";
            this.kryptonLinkLabel2.LinkClicked += new System.EventHandler(this.kryptonLinkLabel2_LinkClicked);
            // 
            // kryptonLinkLabel1
            // 
            this.kryptonLinkLabel1.Location = new System.Drawing.Point(398, 3);
            this.kryptonLinkLabel1.Name = "kryptonLinkLabel1";
            this.kryptonLinkLabel1.Size = new System.Drawing.Size(70, 20);
            this.kryptonLinkLabel1.TabIndex = 2;
            this.kryptonLinkLabel1.Values.Text = "Variável 01";
            this.kryptonLinkLabel1.LinkClicked += new System.EventHandler(this.kryptonLinkLabel1_LinkClicked);
            // 
            // labelCaracter
            // 
            this.labelCaracter.AutoSize = true;
            this.labelCaracter.BackColor = System.Drawing.Color.Transparent;
            this.labelCaracter.Location = new System.Drawing.Point(84, 8);
            this.labelCaracter.Name = "labelCaracter";
            this.labelCaracter.Size = new System.Drawing.Size(13, 13);
            this.labelCaracter.TabIndex = 1;
            this.labelCaracter.Text = "0";
            this.labelCaracter.Click += new System.EventHandler(this.labelCaracter_Click);
            // 
            // kryptonLabel4
            // 
            this.kryptonLabel4.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.BoldControl;
            this.kryptonLabel4.Location = new System.Drawing.Point(12, 4);
            this.kryptonLabel4.Name = "kryptonLabel4";
            this.kryptonLabel4.Size = new System.Drawing.Size(74, 20);
            this.kryptonLabel4.TabIndex = 0;
            this.kryptonLabel4.Values.Text = "Caracteres:";
            // 
            // textMensagem
            // 
            textMensagem.Location = new System.Drawing.Point(28, 133);
            textMensagem.MaxLength = 155;
            textMensagem.Multiline = true;
            textMensagem.Name = "textMensagem";
            textMensagem.Size = new System.Drawing.Size(516, 208);
            textMensagem.TabIndex = 10;
            textMensagem.TextChanged += new System.EventHandler(textMensagem_TextChanged);
            // 
            // buttonLimpar
            // 
            this.buttonLimpar.Location = new System.Drawing.Point(550, 133);
            this.buttonLimpar.Name = "buttonLimpar";
            this.buttonLimpar.Size = new System.Drawing.Size(45, 34);
            this.buttonLimpar.TabIndex = 12;
            this.buttonLimpar.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonLimpar.Values.Image")));
            this.buttonLimpar.Values.Text = "";
            this.buttonLimpar.Click += new System.EventHandler(this.buttonLimpar_Click_1);
            // 
            // kryptonButton1
            // 
            this.kryptonButton1.Location = new System.Drawing.Point(357, 347);
            this.kryptonButton1.Name = "kryptonButton1";
            this.kryptonButton1.Size = new System.Drawing.Size(187, 25);
            this.kryptonButton1.TabIndex = 13;
            this.kryptonButton1.Values.Text = "Enviar para Caixa de Saída";
            this.kryptonButton1.Click += new System.EventHandler(this.kryptonButton1_Click);
            // 
            // chkButtonCaracteres
            // 
            this.chkButtonCaracteres.Location = new System.Drawing.Point(28, 347);
            this.chkButtonCaracteres.Name = "chkButtonCaracteres";
            this.chkButtonCaracteres.Size = new System.Drawing.Size(174, 25);
            this.chkButtonCaracteres.TabIndex = 14;
            this.chkButtonCaracteres.Values.Text = "Bloqueado +155 Caracteres";
            this.chkButtonCaracteres.Click += new System.EventHandler(this.chkButtonCaracteres_Click);
            // 
            // bwsCaixaSaida
            // 
            this.bwsCaixaSaida.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwsCaixaSaida_DoWork);
            // 
            // Campanha
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(605, 383);
            this.Controls.Add(this.chkButtonCaracteres);
            this.Controls.Add(this.kryptonButton1);
            this.Controls.Add(this.buttonLimpar);
            this.Controls.Add(textMensagem);
            this.Controls.Add(this.kryptonPanel2);
            this.Controls.Add(this.kryptonPanel1);
            this.Controls.Add(this.kryptonHeader1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Campanha";
            this.Text = "Campanha";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Campanha_FormClosing);
            this.Load += new System.EventHandler(this.Campanha_Load);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel1)).EndInit();
            this.kryptonPanel1.ResumeLayout(false);
            this.kryptonPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboGrupo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel2)).EndInit();
            this.kryptonPanel2.ResumeLayout(false);
            this.kryptonPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel kryptonPanel1;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox comboGrupo;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel2;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel kryptonPanel2;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel4;
        private static ComponentFactory.Krypton.Toolkit.KryptonTextBox textMensagem;
        private System.Windows.Forms.Label labelCaracter;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonLimpar;
        private System.Windows.Forms.ToolTip ttpGeral;
        private ComponentFactory.Krypton.Toolkit.KryptonLinkLabel kryptonLinkLabel2;
        private ComponentFactory.Krypton.Toolkit.KryptonLinkLabel kryptonLinkLabel1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton1;
        private ComponentFactory.Krypton.Toolkit.KryptonLinkLabel kryptonLinkLabel3;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckButton chkButtonCaracteres;
        private System.ComponentModel.BackgroundWorker bwsCaixaSaida;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textNumeros;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel1;
    }
}