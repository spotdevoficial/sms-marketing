﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace SMS
{
    public partial class Campanha : Form
    {
        int textoAberto = 0;
        int idGrupo = 0;
        int qtdCaracates = 0;
        public static string CAMPANHA_MESSAGE = "";

        public Campanha(int grupo)
        {
            InitializeComponent();
            idGrupo = grupo;
            textMensagem.Text = CAMPANHA_MESSAGE;
        }

        private void Campanha_Load(object sender, EventArgs e)
        {
            //DESABILITA A CAIXA DE TEXTO
            //textMensagem.Enabled = false;
            
            try
            {
                preencheGrupo();
                chkButtonCaracteres.Checked = true;

                //PREENCHE OS CAMPOS DE NÚMERO E GRUPO COM O VALOR QUE ESTÁ NO BANCO
                SQLiteDataReader dr = BD.retornaQuery("SELECT ID_Grupo, Numeros FROM Campanha");
                while (dr.Read())
                {
                    comboGrupo.SelectedValue = idGrupo;
                    //comboGrupo.SelectedValue = Convert.ToInt32(dr[0].ToString());
                    textNumeros.Text = dr[1].ToString();
                }
                dr.Close();
            }
            catch { }
            
                
            
            //TOOLTIP - BALÃO DE MENSAGEM COM INFORMAÇÕES DO BOTÃO
            //ttpGeral.SetToolTip(buttonSalvar, "Salvar texto!");
            ttpGeral.SetToolTip(buttonLimpar, "Limpar texto!");
            //ttpGeral.SetToolTip(buttonTexto1, "Texto 01 da Campanha");
            /*ttpGeral.SetToolTip(buttonTexto2, "Texto 02 da Campanha");
            ttpGeral.SetToolTip(buttonTexto3, "Texto 03 da Campanha");
            ttpGeral.SetToolTip(buttonTexto4, "Texto 04 da Campanha");
            ttpGeral.SetToolTip(buttonTexto5, "Texto 05 da Campanha");   */         
        }

        private void preencheGrupo()
        {
            ArrayList grupo = new ArrayList();
            SQLiteDataReader dr = BD.retornaQuery("SELECT ID_Grupo, Nome FROM Grupos");
            while (dr.Read())
            {
                grupo.Add(new Codigo_Descricao(dr[1].ToString(), Convert.ToInt32(dr[0].ToString())));
            }
            dr.Close();
            // atribuimos o ArrayList como DataSource do ComboBox
            //linguagensCombo.DataSource = linguagens;
            comboGrupo.DataSource = grupo;
            // o texto do item será o nome da linguagem
            comboGrupo.DisplayMember = "Descricao";
            // o valor do item será o código da linguagem
            comboGrupo.ValueMember = "Codigo";
        }

        private void textMensagem_TextChanged(object sender, EventArgs e)
        {
            //SE O TEXTO ATINGIR 160 CARACTERES, O LABEL REFERENTE A QUANTIDADE
            //É EXIBIDO EM VERMELHO E COM A PALAVRA MÁXIMO NA FRENTE
            //INDICANDO QUE A QUANTIDADE MÁXIMA FOI ATINGIDA

            CAMPANHA_MESSAGE = textMensagem.Text;

            if (qtdCaracates == 0)
            {
                labelCaracter.Text = textMensagem.TextLength.ToString();
                if (Convert.ToInt32(textMensagem.TextLength.ToString()) == 155)
                {
                    labelCaracter.Text = labelCaracter.Text + " (MÁXIMO)";
                    labelCaracter.ForeColor = Color.Red;
                }
                else if (Convert.ToInt32(textMensagem.TextLength.ToString()) >= 156)
                {
                    labelCaracter.Text = labelCaracter.Text + " (LIMETE ESTOURADO)";
                    labelCaracter.ForeColor = Color.Red;
                }
                else
                {
                    labelCaracter.ForeColor = Color.Black;
                    labelCaracter.Text = textMensagem.TextLength.ToString();
                }  
            }
            else
            {
                
                labelCaracter.Text = textMensagem.TextLength.ToString();
                //if (Convert.ToInt32(textMensagem.TextLength.ToString()) == 160)
                //{
                    labelCaracter.Text = labelCaracter.Text + " ("+(textMensagem.TextLength/155).ToString()+" SMS)";
                    labelCaracter.ForeColor = Color.Black;
                //}
                //else
                //{
                //    labelCaracter.ForeColor = Color.Black;
                //    labelCaracter.Text = textMensagem.TextLength.ToString();
                //}  
            }
                          
        }

        private void buttonTexto1_Click(object sender, EventArgs e)
        {

            //HABILITA O CAMPO TEXTO 
            if (textMensagem.Enabled == false)
                textMensagem.Enabled = true;

            //SETA A VARIAVEL PARA 1 CORRESPONDENTE A TELA ABERTA
            textoAberto = 1;

            //PREENCHE O CAMPO COM O VALOR QUE ESTÁ NO BANCO REFERENTE AO TEXTO 1
            SQLiteDataReader dr = BD.retornaQuery("SELECT Texto01 FROM Campanha");
            while (dr.Read())
            {
                textMensagem.Text = dr[0].ToString();
            } dr.Close();
                
        }

        private void buttonTexto2_Click(object sender, EventArgs e)
        {
            //HABILITA O CAMPO TEXTO 
            if (textMensagem.Enabled == false)
                textMensagem.Enabled = true;

            //SETA A VARIAVEL PARA 2 CORRESPONDENTE A TELA ABERTA
            textoAberto = 2;

            //PREENCHE O CAMPO COM O VALOR QUE ESTÁ NO BANCO REFERENTE AO TEXTO 2
            SQLiteDataReader dr = BD.retornaQuery("SELECT Texto02 FROM Campanha");
            while (dr.Read())
            {
                textMensagem.Text = dr[0].ToString();
            }
            dr.Close();
        }

        private void buttonTexto3_Click(object sender, EventArgs e)
        {
            //HABILITA O CAMPO TEXTO 
            if (textMensagem.Enabled == false)
                textMensagem.Enabled = true;

            //SETA A VARIAVEL PARA 3 CORRESPONDENTE A TELA ABERTA
            textoAberto = 3;

            //PREENCHE O CAMPO COM O VALOR QUE ESTÁ NO BANCO REFERENTE AO TEXTO 3
            SQLiteDataReader dr = BD.retornaQuery("SELECT Texto03 FROM Campanha");
            while (dr.Read())
                textMensagem.Text = dr[0].ToString();
            dr.Close();
        }

        private void buttonTexto4_Click(object sender, EventArgs e)
        {
            //HABILITA O CAMPO TEXTO 
            if (textMensagem.Enabled == false)
                textMensagem.Enabled = true;

            //SETA A VARIAVEL PARA 4 CORRESPONDENTE A TELA ABERTA
            textoAberto = 4;

            //PREENCHE O CAMPO COM O VALOR QUE ESTÁ NO BANCO REFERENTE AO TEXTO 4
            SQLiteDataReader dr = BD.retornaQuery("SELECT Texto04 FROM Campanha");
            while (dr.Read())
                textMensagem.Text = dr[0].ToString();
            dr.Close();
        }

        private void buttonTexto5_Click(object sender, EventArgs e)
        {
            //HABILITA O CAMPO TEXTO 
            if (textMensagem.Enabled == false)
                textMensagem.Enabled = true;

            //SETA A VARIAVEL PARA 5 CORRESPONDENTE A TELA ABERTA
            textoAberto = 5;

            //PREENCHE O CAMPO COM O VALOR QUE ESTÁ NO BANCO REFERENTE AO TEXTO 5
            SQLiteDataReader dr = BD.retornaQuery("SELECT Texto05 FROM Campanha");
            while (dr.Read())
                textMensagem.Text = dr[0].ToString();
            dr.Close();
        }

        private void buttonSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboGrupo.SelectedIndex != null)
                    try { idGrupo = Convert.ToInt32(comboGrupo.SelectedValue.ToString()); }
                    catch { }
                    

                if (textoAberto > 0)
                {
                    BD.executaQuery("UPDATE Campanha SET Texto0" + textoAberto + " = '" + textMensagem.Text + "', " +
                        "Numeros = '" + textNumeros.Text + "',  " +
                        "ID_Grupo = '" + idGrupo + "' ");
                    BD.executaQuery("UPDATE Caixa_Saida SET Mensagem = (SELECT CASE WHEN ID_Texto =1 THEN Texto01 "+
                                    "WHEN ID_Texto =2 THEN Texto02 WHEN ID_Texto =3 THEN Texto03 " +
                                    "WHEN ID_Texto =4 THEN Texto04 WHEN ID_Texto =5 THEN Texto05 END FROM Campanha)");
                }
                else
                {
                    BD.executaQuery("UPDATE Campanha SET ID_Grupo = '" + comboGrupo.SelectedValue + "'");
                }
            }
            catch (Exception erro)
            {
                MessageBox.Show(erro.Message);
            }
            

        }

        private void buttonLimpar_Click(object sender, EventArgs e)
        {
            textMensagem.Text = "";
        }

        private void Campanha_FormClosing(object sender, FormClosingEventArgs e)
        {
            //DialogResult resultado = MessageBox.Show("Deseja salvar a campanha?", "", MessageBoxButtons.YesNo);
            //if (resultado == DialogResult.Yes)
            //    buttonSalvar_Click(sender, e);
        }

        private void kryptonLinkLabel1_LinkClicked(object sender, EventArgs e)
        {
            textMensagem.Text = textMensagem.Text + " <Variável 01> ";
        }

        private void kryptonLinkLabel2_LinkClicked(object sender, EventArgs e)
        {
            textMensagem.Text = textMensagem.Text + " <Variável 02> ";
        }


        private void kryptonLinkLabel3_LinkClicked(object sender, EventArgs e)
        {
            textMensagem.Text = textMensagem.Text + " <Variável Nome> ";
        }

        private void kryptonButton1_Click(object sender, EventArgs e)
        {
            if (comboGrupo.SelectedIndex != null)
                try { idGrupo = Convert.ToInt32(comboGrupo.SelectedValue.ToString()); }
                catch { }
            bwsCaixaSaida.RunWorkerAsync();
        }

        private void chkButtonCaracteres_Click(object sender, EventArgs e)
        {
            if (chkButtonCaracteres.Checked == true)
            {
                qtdCaracates = 0;
                textMensagem.MaxLength = 155;                
                textMensagem_TextChanged(sender, e);
            }
            else
            {
                textMensagem.MaxLength = 999999;
                qtdCaracates = 1;
                textMensagem_TextChanged(sender, e);
            }
        }

        private void bwsCaixaSaida_DoWork(object sender, DoWorkEventArgs e)
        {            
            try
            {     
                Random rnd = new Random();
                string texto01 = String.Empty;
                string texto02 = String.Empty;
                string texto03 = String.Empty;
                string texto04 = String.Empty;
                string texto05 = String.Empty;
                string textoEnviar = String.Empty;
                ListBox nome = new ListBox();
                ListBox numeros = new ListBox();
                ListBox variavel1 = new ListBox();
                ListBox variavel2 = new ListBox();

                //SQLiteDataReader dr = BD.retornaQuery("SELECT Texto01, Texto02, Texto03, Texto04, Texto05 FROM Campanha");
                //while (dr.Read())
                //{
                //    texto01 = dr[0].ToString();
                //    texto02 = dr[1].ToString();
                //    texto03 = dr[2].ToString();
                //    texto04 = dr[3].ToString();
                //    texto05 = dr[4].ToString();
                //}

                string message = textMensagem.Text;
                if (message.Contains("<Variável Nome>") || message.Contains("<Variável 01>") || message.Contains("<Variável 02>"))
                {
                    BD.retornaQuery("begin");
                    SQLiteDataReader dr1 = BD.retornaQuery("SELECT Telefone, Nome, Variavel1, Variavel2 FROM Contatos WHERE ID_Grupo = '" + idGrupo + "' ");
                    while (dr1.Read())
                    {
                        numeros.Items.Add(dr1[0].ToString());
                        nome.Items.Add(dr1[1].ToString());
                        variavel1.Items.Add(dr1[2].ToString());
                        variavel2.Items.Add(dr1[3].ToString());
                    }

                    for (int i = 0; i < numeros.Items.Count; i++)
                    {
                        BD.executaQuery("INSERT INTO Caixa_Saida(Telefone, Mensagem, Data, ID_Texto) VALUES('" + numeros.Items[i].ToString() + "', " +
                         "'" + RemoveAccents(message.Replace("<Variável Nome>", nome.Items[i].ToString()).Replace("<Variável 01>", variavel1.Items[i].ToString()).Replace("<Variável 02>", variavel2.Items[i].ToString()))+"', " +
                         "'" + System.DateTime.Now + "', 0) ");
                    }
                    BD.retornaQuery("end");
                }
                else
                {
                    BD.retornaQuery("begin");
                    SQLiteDataReader dr = BD.retornaQuery("SELECT Telefone FROM Contatos WHERE ID_Grupo = '" + idGrupo + "' ");
                    while (dr.Read())
                    {
                        string telefone = dr[0].ToString();
                        BD.executaQuery("INSERT INTO Caixa_Saida(Telefone, Mensagem, Data, ID_Texto) " +
                                "VALUES('"+telefone+"','"+ RemoveAccents(message) + "','"+System.DateTime.Now+"',0)");
                    }
                    BD.retornaQuery("end");
                }

                

                MessageBox.Show("Mensagens adicionadas a caixa de saída", "", MessageBoxButtons.OK);
            }
            catch (Exception erro)
            {
                MessageBox.Show(erro.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } 
        }

        public string RemoveAccents(string text)
        {
            StringBuilder sbReturn = new StringBuilder();
            var arrayText = text.Normalize(NormalizationForm.FormD).ToCharArray();
            foreach (char letter in arrayText)
            {
                if (CharUnicodeInfo.GetUnicodeCategory(letter) != UnicodeCategory.NonSpacingMark)
                    sbReturn.Append(letter);
            }
            return sbReturn.ToString();
        }

        private void comboGrupo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (comboGrupo.SelectedIndex != null)
            //    idGrupo = Convert.ToInt32(comboGrupo.SelectedValue.ToString());
        }

        private void labelCaracter_Click(object sender, EventArgs e)
        {

        }

        private void buttonLimpar_Click_1(object sender, EventArgs e)
        {
            textMensagem.Text = "";
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }


    }
}
