﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMS
{
    class Codigo_Descricao
    {
        private string descricao;
        private int codigo;

        public Codigo_Descricao(string n, int c)
        {
            this.descricao = n;
            this.codigo = c;
        }

        public string Descricao
        {
            get
            {
                return descricao;
            }
        }

        public int Codigo
        {
            get
            {
                return codigo;
            }
        }

        public override string ToString()
        {
            return this.descricao + " - " + this.codigo;
        }
    }
}
