﻿namespace SMS
{
    partial class Config_Email
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Config_Email));
            this.buttonCancelar = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonSalvar = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.checkSsl = new System.Windows.Forms.CheckBox();
            this.textPorta = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel4 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.textSmtp = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel3 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.textSenha = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel2 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.textEmail = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.buttonTeste = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textEmailTeste = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonCancelar
            // 
            this.buttonCancelar.Location = new System.Drawing.Point(334, 54);
            this.buttonCancelar.Name = "buttonCancelar";
            this.buttonCancelar.Size = new System.Drawing.Size(33, 25);
            this.buttonCancelar.TabIndex = 145;
            this.buttonCancelar.TabStop = false;
            this.buttonCancelar.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonCancelar.Values.Image")));
            this.buttonCancelar.Values.Text = "";
            this.buttonCancelar.Click += new System.EventHandler(this.buttonCancelar_Click);
            // 
            // buttonSalvar
            // 
            this.buttonSalvar.Location = new System.Drawing.Point(334, 23);
            this.buttonSalvar.Name = "buttonSalvar";
            this.buttonSalvar.Size = new System.Drawing.Size(33, 25);
            this.buttonSalvar.TabIndex = 144;
            this.buttonSalvar.TabStop = false;
            this.buttonSalvar.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonSalvar.Values.Image")));
            this.buttonSalvar.Values.Text = "";
            this.buttonSalvar.Click += new System.EventHandler(this.buttonSalvar_Click);
            // 
            // checkSsl
            // 
            this.checkSsl.AutoSize = true;
            this.checkSsl.Location = new System.Drawing.Point(191, 152);
            this.checkSsl.Name = "checkSsl";
            this.checkSsl.Size = new System.Drawing.Size(134, 17);
            this.checkSsl.TabIndex = 143;
            this.checkSsl.Text = "Requer conexão SSL?";
            this.checkSsl.UseVisualStyleBackColor = true;
            // 
            // textPorta
            // 
            this.textPorta.Location = new System.Drawing.Point(10, 149);
            this.textPorta.Name = "textPorta";
            this.textPorta.Size = new System.Drawing.Size(148, 20);
            this.textPorta.TabIndex = 142;
            // 
            // kryptonLabel4
            // 
            this.kryptonLabel4.Location = new System.Drawing.Point(10, 133);
            this.kryptonLabel4.Name = "kryptonLabel4";
            this.kryptonLabel4.Size = new System.Drawing.Size(39, 20);
            this.kryptonLabel4.TabIndex = 141;
            this.kryptonLabel4.Values.Text = "Porta";
            // 
            // textSmtp
            // 
            this.textSmtp.Location = new System.Drawing.Point(9, 107);
            this.textSmtp.Name = "textSmtp";
            this.textSmtp.Size = new System.Drawing.Size(316, 20);
            this.textSmtp.TabIndex = 140;
            // 
            // kryptonLabel3
            // 
            this.kryptonLabel3.Location = new System.Drawing.Point(9, 91);
            this.kryptonLabel3.Name = "kryptonLabel3";
            this.kryptonLabel3.Size = new System.Drawing.Size(42, 20);
            this.kryptonLabel3.TabIndex = 139;
            this.kryptonLabel3.Values.Text = "SMTP";
            // 
            // textSenha
            // 
            this.textSenha.Location = new System.Drawing.Point(10, 65);
            this.textSenha.Name = "textSenha";
            this.textSenha.PasswordChar = '●';
            this.textSenha.Size = new System.Drawing.Size(316, 20);
            this.textSenha.TabIndex = 138;
            // 
            // kryptonLabel2
            // 
            this.kryptonLabel2.Location = new System.Drawing.Point(10, 49);
            this.kryptonLabel2.Name = "kryptonLabel2";
            this.kryptonLabel2.Size = new System.Drawing.Size(44, 20);
            this.kryptonLabel2.TabIndex = 137;
            this.kryptonLabel2.Values.Text = "Senha";
            // 
            // textEmail
            // 
            this.textEmail.Location = new System.Drawing.Point(10, 23);
            this.textEmail.Name = "textEmail";
            this.textEmail.Size = new System.Drawing.Size(316, 20);
            this.textEmail.TabIndex = 136;
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.Location = new System.Drawing.Point(10, 7);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(45, 20);
            this.kryptonLabel1.TabIndex = 135;
            this.kryptonLabel1.Values.Text = "E-Mail";
            // 
            // buttonTeste
            // 
            this.buttonTeste.Location = new System.Drawing.Point(303, 16);
            this.buttonTeste.Name = "buttonTeste";
            this.buttonTeste.Size = new System.Drawing.Size(47, 25);
            this.buttonTeste.TabIndex = 121;
            this.buttonTeste.Values.Text = "Teste";
            this.buttonTeste.Click += new System.EventHandler(this.buttonTeste_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textEmailTeste);
            this.groupBox1.Controls.Add(this.buttonTeste);
            this.groupBox1.Location = new System.Drawing.Point(10, 175);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(357, 51);
            this.groupBox1.TabIndex = 146;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Teste de envio";
            // 
            // textEmailTeste
            // 
            this.textEmailTeste.Location = new System.Drawing.Point(6, 19);
            this.textEmailTeste.Name = "textEmailTeste";
            this.textEmailTeste.Size = new System.Drawing.Size(291, 20);
            this.textEmailTeste.TabIndex = 123;
            // 
            // Config_Email
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(377, 233);
            this.Controls.Add(this.buttonCancelar);
            this.Controls.Add(this.buttonSalvar);
            this.Controls.Add(this.checkSsl);
            this.Controls.Add(this.textPorta);
            this.Controls.Add(this.kryptonLabel4);
            this.Controls.Add(this.textSmtp);
            this.Controls.Add(this.kryptonLabel3);
            this.Controls.Add(this.textSenha);
            this.Controls.Add(this.kryptonLabel2);
            this.Controls.Add(this.textEmail);
            this.Controls.Add(this.kryptonLabel1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Config_Email";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuração de Email";
            this.Load += new System.EventHandler(this.Config_Email_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonCancelar;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonSalvar;
        private System.Windows.Forms.CheckBox checkSsl;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textPorta;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel4;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textSmtp;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel3;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textSenha;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel2;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textEmail;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonTeste;
        private System.Windows.Forms.GroupBox groupBox1;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textEmailTeste;
    }
}