﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SMS
{
    public partial class Config_Email : Form
    {
        public Config_Email()
        {
            InitializeComponent();
        }

        private void Config_Email_Load(object sender, EventArgs e)
        {
            atualizaLista();
        }

        private void atualizaLista()
        {
            SQLiteDataReader dr = BD.retornaQuery("SELECT Email, Senha, Smtp, Porta, SSL FROM Email");
            while (dr.Read())
            {
                textEmail.Text = dr[0].ToString();
                textSenha.Text = dr[1].ToString();
                textSmtp.Text = dr[2].ToString();
                textPorta.Text = dr[3].ToString();
                checkSsl.Checked = Convert.ToInt32(dr[4].ToString()) == 1 ? true : false;
            } dr.Close();
        }

        private void buttonSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                int ssl = 0;
                if (checkSsl.Checked == true)
                    ssl = 1;
                BD.executaQuery("UPDATE Email SET Email = '" + textEmail.Text + "', Senha = '" + textSenha.Text + "', " +
                    "Smtp = '" + textSmtp.Text + "', Porta = '" + textPorta.Text + "', SSL = '" + ssl + "' ");

                MessageBox.Show("Salvo com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception erro)
            {
                MessageBox.Show(erro.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonTeste_Click(object sender, EventArgs e)
        {
            EnviaEmail.envia(textEmailTeste.Text, "Teste de Envio - Sistema de SMS", "Teste");
        }

        
    }
}
