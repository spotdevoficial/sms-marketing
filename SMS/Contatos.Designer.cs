﻿namespace SMS
{
    partial class Contatos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Contatos));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.headerContatos = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.kryptonPanel2 = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.kryptonPanel1 = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.gridContatos = new ComponentFactory.Krypton.Toolkit.KryptonDataGridView();
            this.Selecionar = new ComponentFactory.Krypton.Toolkit.KryptonDataGridViewCheckBoxColumn();
            this.ID_Contato = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Telefone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID_Grupo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Variavel01 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Variavel02 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.excluirTodosOsContatatosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel2 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel3 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel5 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel6 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.textNome = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.textTelefone = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.textVariavel01 = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.textVariavel02 = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonEditar = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonSalvar = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonNovo = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonRemover = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.comboGrupo = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.buttonImportar = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel1)).BeginInit();
            this.kryptonPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridContatos)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboGrupo)).BeginInit();
            this.SuspendLayout();
            // 
            // headerContatos
            // 
            this.headerContatos.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerContatos.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Custom2;
            this.headerContatos.Location = new System.Drawing.Point(0, 0);
            this.headerContatos.Name = "headerContatos";
            this.headerContatos.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Blue;
            this.headerContatos.Size = new System.Drawing.Size(605, 31);
            this.headerContatos.TabIndex = 2;
            this.headerContatos.Values.Description = "";
            this.headerContatos.Values.Heading = "Contatos";
            this.headerContatos.Values.Image = ((System.Drawing.Image)(resources.GetObject("headerContatos.Values.Image")));
            // 
            // kryptonPanel2
            // 
            this.kryptonPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonPanel2.Location = new System.Drawing.Point(0, 204);
            this.kryptonPanel2.Name = "kryptonPanel2";
            this.kryptonPanel2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Blue;
            this.kryptonPanel2.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.HeaderCustom2;
            this.kryptonPanel2.Size = new System.Drawing.Size(605, 28);
            this.kryptonPanel2.TabIndex = 5;
            // 
            // kryptonPanel1
            // 
            this.kryptonPanel1.Controls.Add(this.gridContatos);
            this.kryptonPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonPanel1.Location = new System.Drawing.Point(0, 31);
            this.kryptonPanel1.Name = "kryptonPanel1";
            this.kryptonPanel1.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.GridHeaderColumnList;
            this.kryptonPanel1.Size = new System.Drawing.Size(605, 173);
            this.kryptonPanel1.TabIndex = 4;
            // 
            // gridContatos
            // 
            this.gridContatos.AllowUserToAddRows = false;
            this.gridContatos.AllowUserToDeleteRows = false;
            this.gridContatos.AllowUserToResizeRows = false;
            this.gridContatos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left
            | System.Windows.Forms.AnchorStyles.Right))));
            this.gridContatos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridContatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridContatos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Selecionar,
            this.ID_Contato,
            this.Nome,
            this.Telefone,
            this.ID_Grupo,
            this.Variavel01,
            this.Variavel02});
            this.gridContatos.ContextMenuStrip = this.contextMenuStrip1;
            this.gridContatos.GridStyles.Style = ComponentFactory.Krypton.Toolkit.DataGridViewStyle.Mixed;
            this.gridContatos.GridStyles.StyleBackground = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.GridHeaderColumnCustom1;
            this.gridContatos.Location = new System.Drawing.Point(0, 0);
            this.gridContatos.MultiSelect = false;
            this.gridContatos.Name = "gridContatos";
            this.gridContatos.RowHeadersVisible = false;
            this.gridContatos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridContatos.Size = new System.Drawing.Size(605, 173);
            this.gridContatos.TabIndex = 0;
            this.gridContatos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridContatos_CellClick);
            // 
            // Selecionar
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = false;
            this.Selecionar.DefaultCellStyle = dataGridViewCellStyle2;
            this.Selecionar.FalseValue = null;
            this.Selecionar.HeaderText = "";
            this.Selecionar.IndeterminateValue = null;
            this.Selecionar.Name = "Selecionar";
            this.Selecionar.TrueValue = null;
            // 
            // ID_Contato
            // 
            this.ID_Contato.DataPropertyName = "ID_Contato";
            this.ID_Contato.HeaderText = "ID_Contato";
            this.ID_Contato.Name = "ID_Contato";
            this.ID_Contato.ReadOnly = true;
            this.ID_Contato.Visible = false;
            // 
            // Nome
            // 
            this.Nome.DataPropertyName = "Nome";
            this.Nome.HeaderText = "Nome";
            this.Nome.Name = "Nome";
            this.Nome.ReadOnly = true;
            // 
            // Telefone
            // 
            this.Telefone.DataPropertyName = "Telefone";
            this.Telefone.HeaderText = "Telefone";
            this.Telefone.Name = "Telefone";
            this.Telefone.ReadOnly = true;
            // 
            // ID_Grupo
            // 
            this.ID_Grupo.DataPropertyName = "Grupo";
            this.ID_Grupo.HeaderText = "Grupo";
            this.ID_Grupo.Name = "ID_Grupo";
            this.ID_Grupo.ReadOnly = true;
            // 
            // Variavel01
            // 
            this.Variavel01.DataPropertyName = "Variavel1";
            this.Variavel01.HeaderText = "Variavel 01";
            this.Variavel01.Name = "Variavel01";
            this.Variavel01.ReadOnly = true;
            // 
            // Variavel02
            // 
            this.Variavel02.DataPropertyName = "Variavel2";
            this.Variavel02.HeaderText = "Variavel 02";
            this.Variavel02.Name = "Variavel02";
            this.Variavel02.ReadOnly = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.excluirTodosOsContatatosToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(284, 26);
            // 
            // excluirTodosOsContatatosToolStripMenuItem
            // 
            this.excluirTodosOsContatatosToolStripMenuItem.Name = "excluirTodosOsContatatosToolStripMenuItem";
            this.excluirTodosOsContatatosToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.excluirTodosOsContatatosToolStripMenuItem.Text = "Selecionar e excluir todos os Contatatos";
            this.excluirTodosOsContatatosToolStripMenuItem.Click += new System.EventHandler(this.excluirTodosOsContatatosToolStripMenuItem_Click);
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.kryptonLabel1.Location = new System.Drawing.Point(132, 242);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(47, 20);
            this.kryptonLabel1.TabIndex = 6;
            this.kryptonLabel1.Values.Text = "Nome:";
            // 
            // kryptonLabel2
            // 
            this.kryptonLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.kryptonLabel2.Location = new System.Drawing.Point(120, 268);
            this.kryptonLabel2.Name = "kryptonLabel2";
            this.kryptonLabel2.Size = new System.Drawing.Size(60, 20);
            this.kryptonLabel2.TabIndex = 7;
            this.kryptonLabel2.Values.Text = "Telefone:";
            // 
            // kryptonLabel3
            // 
            this.kryptonLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.kryptonLabel3.Location = new System.Drawing.Point(132, 294);
            this.kryptonLabel3.Name = "kryptonLabel3";
            this.kryptonLabel3.Size = new System.Drawing.Size(47, 20);
            this.kryptonLabel3.TabIndex = 8;
            this.kryptonLabel3.Values.Text = "Grupo:";
            // 
            // kryptonLabel5
            // 
            this.kryptonLabel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.kryptonLabel5.Location = new System.Drawing.Point(106, 320);
            this.kryptonLabel5.Name = "kryptonLabel5";
            this.kryptonLabel5.Size = new System.Drawing.Size(73, 20);
            this.kryptonLabel5.TabIndex = 9;
            this.kryptonLabel5.Values.Text = "Variável 01:";
            // 
            // kryptonLabel6
            // 
            this.kryptonLabel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.kryptonLabel6.Location = new System.Drawing.Point(107, 346);
            this.kryptonLabel6.Name = "kryptonLabel6";
            this.kryptonLabel6.Size = new System.Drawing.Size(73, 20);
            this.kryptonLabel6.TabIndex = 10;
            this.kryptonLabel6.Values.Text = "Variável 02:";
            // 
            // textNome
            // 
            this.textNome.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textNome.Location = new System.Drawing.Point(181, 242);
            this.textNome.Name = "textNome";
            this.textNome.Size = new System.Drawing.Size(288, 20);
            this.textNome.TabIndex = 11;
            // 
            // textTelefone
            // 
            this.textTelefone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textTelefone.Location = new System.Drawing.Point(181, 268);
            this.textTelefone.Name = "textTelefone";
            this.textTelefone.Size = new System.Drawing.Size(288, 20);
            this.textTelefone.TabIndex = 12;
            this.textTelefone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textTelefone_KeyPress);
            // 
            // textVariavel01
            // 
            this.textVariavel01.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textVariavel01.Location = new System.Drawing.Point(181, 320);
            this.textVariavel01.Name = "textVariavel01";
            this.textVariavel01.Size = new System.Drawing.Size(288, 20);
            this.textVariavel01.TabIndex = 14;
            // 
            // textVariavel02
            // 
            this.textVariavel02.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textVariavel02.Location = new System.Drawing.Point(181, 346);
            this.textVariavel02.Name = "textVariavel02";
            this.textVariavel02.Size = new System.Drawing.Size(288, 20);
            this.textVariavel02.TabIndex = 15;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(6, 249);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(97, 110);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // buttonEditar
            // 
            this.buttonEditar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEditar.Location = new System.Drawing.Point(538, 308);
            this.buttonEditar.Name = "buttonEditar";
            this.buttonEditar.Size = new System.Drawing.Size(57, 58);
            this.buttonEditar.TabIndex = 20;
            this.buttonEditar.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonEditar.Values.Image")));
            this.buttonEditar.Values.Text = "";
            this.buttonEditar.Click += new System.EventHandler(this.buttonEditar_Click);
            // 
            // buttonSalvar
            // 
            this.buttonSalvar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSalvar.Location = new System.Drawing.Point(478, 308);
            this.buttonSalvar.Name = "buttonSalvar";
            this.buttonSalvar.Size = new System.Drawing.Size(57, 58);
            this.buttonSalvar.TabIndex = 21;
            this.buttonSalvar.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonSalvar.Values.Image")));
            this.buttonSalvar.Values.Text = "";
            this.buttonSalvar.Click += new System.EventHandler(this.buttonSalvar_Click);
            // 
            // buttonNovo
            // 
            this.buttonNovo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNovo.Location = new System.Drawing.Point(478, 242);
            this.buttonNovo.Name = "buttonNovo";
            this.buttonNovo.Size = new System.Drawing.Size(57, 58);
            this.buttonNovo.TabIndex = 22;
            this.buttonNovo.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonNovo.Values.Image")));
            this.buttonNovo.Values.Text = "";
            this.buttonNovo.Click += new System.EventHandler(this.buttonNovo_Click);
            // 
            // buttonRemover
            // 
            this.buttonRemover.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRemover.Location = new System.Drawing.Point(538, 242);
            this.buttonRemover.Name = "buttonRemover";
            this.buttonRemover.Size = new System.Drawing.Size(57, 58);
            this.buttonRemover.TabIndex = 23;
            this.buttonRemover.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonRemover.Values.Image")));
            this.buttonRemover.Values.Text = "";
            this.buttonRemover.Click += new System.EventHandler(this.buttonRemover_Click);
            // 
            // comboGrupo
            // 
            this.comboGrupo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.comboGrupo.DropDownWidth = 288;
            this.comboGrupo.Location = new System.Drawing.Point(181, 293);
            this.comboGrupo.Name = "comboGrupo";
            this.comboGrupo.Size = new System.Drawing.Size(288, 21);
            this.comboGrupo.TabIndex = 24;
            // 
            // buttonImportar
            // 
            this.buttonImportar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImportar.Location = new System.Drawing.Point(440, 3);
            this.buttonImportar.Name = "buttonImportar";
            this.buttonImportar.Size = new System.Drawing.Size(162, 25);
            this.buttonImportar.TabIndex = 15;
            this.buttonImportar.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonImportar.Values.Image")));
            this.buttonImportar.Values.Text = "Importação em Lote   ";
            this.buttonImportar.Click += new System.EventHandler(this.buttonImportar_Click);
            // 
            // Contatos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(605, 383);
            this.Controls.Add(this.buttonImportar);
            this.Controls.Add(this.comboGrupo);
            this.Controls.Add(this.buttonRemover);
            this.Controls.Add(this.buttonNovo);
            this.Controls.Add(this.buttonSalvar);
            this.Controls.Add(this.buttonEditar);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.textVariavel02);
            this.Controls.Add(this.textVariavel01);
            this.Controls.Add(this.textTelefone);
            this.Controls.Add(this.textNome);
            this.Controls.Add(this.kryptonLabel6);
            this.Controls.Add(this.kryptonLabel5);
            this.Controls.Add(this.kryptonLabel3);
            this.Controls.Add(this.kryptonLabel2);
            this.Controls.Add(this.kryptonLabel1);
            this.Controls.Add(this.kryptonPanel2);
            this.Controls.Add(this.kryptonPanel1);
            this.Controls.Add(this.headerContatos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Contatos";
            this.Text = "Contatos";
            this.Load += new System.EventHandler(this.Contatos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel1)).EndInit();
            this.kryptonPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridContatos)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboGrupo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader headerContatos;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel kryptonPanel2;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel kryptonPanel1;
        private ComponentFactory.Krypton.Toolkit.KryptonDataGridView gridContatos;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel1;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel2;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel3;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel5;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel6;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textNome;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textTelefone;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textVariavel01;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textVariavel02;
        private System.Windows.Forms.PictureBox pictureBox1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonEditar;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonSalvar;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonNovo;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonRemover;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox comboGrupo;
        private ComponentFactory.Krypton.Toolkit.KryptonDataGridViewCheckBoxColumn Selecionar;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_Contato;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn Telefone;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_Grupo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Variavel01;
        private System.Windows.Forms.DataGridViewTextBoxColumn Variavel02;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonImportar;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem excluirTodosOsContatatosToolStripMenuItem;
    }
}