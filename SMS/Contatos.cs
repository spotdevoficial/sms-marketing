﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace SMS
{
    public partial class Contatos : Form
    {
        BindingSource bs = null;
        int idGrupo = 0;
        string codigo = null;

        public Contatos()
        {
            InitializeComponent();
        }

        private void Contatos_Load(object sender, EventArgs e)
        {
            atualiza();

            //PREENCHE O COMBOBOX REFERENTE AO GRUPO
            preencheGrupo();

            //INABILITA OS CAMPOS DE CADASTRO DO USUÁRIO
            textNome.Enabled = false;
            textTelefone.Enabled = false;
            comboGrupo.Enabled = false;
            textVariavel01.Enabled = false;
            textVariavel02.Enabled = false;

            this.Dock = DockStyle.Fill;

        }

        private void atualiza()
        {
            //CRIA UM DATASET COM O RETORNO DA QUERY
            DataSet ds = BD.retornaQueryDataSet("SELECT ID_Contato, Contatos.Nome, Telefone, Grupos.Nome AS Grupo, Variavel1, Variavel2 FROM Contatos " +
                "LEFT JOIN Grupos ON Grupos.ID_Grupo = Contatos.ID_Grupo ORDER BY Contatos.Nome");
            //CRIA UM BINDINGSOURCE
            bs = new BindingSource();
            //ATRIBUI O DATASET AO DATASOURCE DO BINDINGSOURCE
            bs.DataSource = ds;
            //ATRIBUI O BINDINGSOURCE AO BINDINGNAVIGATOR
            bs.DataMember = ds.Tables[0].TableName;
            //ATRIBUI O BINDINGSOURCE AO DATAGRIDVIEW
            gridContatos.DataSource = bs;


            headerContatos.Text = "Contatos (" + gridContatos.RowCount +")";
        }

        private void preencheGrupo()
        {
            try
            {
                ArrayList grupo = new ArrayList();
                SQLiteDataReader dr = BD.retornaQuery("SELECT Nome, ID_Grupo FROM Grupos");
                while (dr.Read())
                {
                    grupo.Add(new Codigo_Descricao(dr[0].ToString(), Convert.ToInt32(dr[1].ToString())));
                } dr.Close();
                comboGrupo.DataSource = grupo;
                // o texto do item será o nome da linguagem
                comboGrupo.DisplayMember = "Descricao";
                // o valor do item será o código da linguagem
                comboGrupo.ValueMember = "Codigo";
                comboGrupo.SelectedIndex = -1;
            }
            catch { }
        }

        private void buttonNovo_Click(object sender, EventArgs e)
        {
            codigo = null;

            //LIMPA OS CAMPOS TEXTO E SELEÇÃO
            textNome.Text = null;
            textTelefone.Text = null;
            comboGrupo.SelectedIndex = -1;
            textVariavel01.Text = null;
            textVariavel02.Text = null;

            //HABILITA OS CAMPOS DE CADASTRO DO USUÁRIO
            textNome.Enabled = true;
            textTelefone.Enabled = true;
            comboGrupo.Enabled = true;
            textVariavel01.Enabled = true;
            textVariavel02.Enabled = true;
        }

        private void buttonEditar_Click(object sender, EventArgs e)
        {
            if (gridContatos.SelectedRows.Count > 0)
            {
                //HABILITA OS CAMPOS DE CADASTRO DO USUÁRIO
                textNome.Enabled = true;
                textTelefone.Enabled = true;
                comboGrupo.Enabled = true;
                textVariavel01.Enabled = true;
                textVariavel02.Enabled = true;
            }
            else
            {
                MessageBox.Show("Necessário selecionar um contato para edição", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                if (codigo == null)
                {
                    if (textTelefone.Text != "0" & textTelefone.Text != "")
                    {
                        idGrupo = (int)comboGrupo.SelectedIndex >= 0 ? (int)comboGrupo.SelectedValue : -1;

                        BD.executaQuery("INSERT INTO Contatos(Nome, Telefone, ID_Grupo, Variavel1, Variavel2) " +
                            "VALUES ('" + textNome.Text + "', '" + textTelefone.Text + "', " +
                            "'" + idGrupo + "', '" + textVariavel01.Text + "', '" + textVariavel02.Text + "')");

                        //INABILITA OS CAMPOS DE CADASTRO DO USUÁRIO E ZERA OS VALORES
                        textNome.Enabled = false; textNome.Text = null;
                        textTelefone.Enabled = false; textTelefone.Text = null;
                        comboGrupo.Enabled = false; comboGrupo.SelectedIndex = -1;
                        textVariavel01.Enabled = false; textVariavel01.Text = null;
                        textVariavel02.Enabled = false; textVariavel02.Text = null;
                    }                    
                }
                else
                {
                    idGrupo = (int)comboGrupo.SelectedIndex >= 0 ? (int)comboGrupo.SelectedValue : -1;
                    codigo = gridContatos.CurrentRow.Cells["ID_Contato"].Value.ToString();
                    BD.executaQuery("UPDATE Contatos SET Nome = '" + textNome.Text + "', " +
                        "Telefone = '" + textTelefone.Text + "', ID_Grupo = '" + idGrupo + "', "+
                        "Variavel1 = '" + textVariavel01.Text + "', Variavel2= '" + textVariavel02.Text + "' WHERE ID_Contato = '"+codigo+"' ");
                }

                atualiza();
            }
            catch (Exception erro)
            {
                MessageBox.Show(erro.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void gridContatos_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if(gridContatos.CurrentRow == null)
            {
                return;
            }

            codigo = gridContatos.CurrentRow.Cells["ID_Contato"].Value.ToString();
            string query = "SELECT ID_Contato, Nome, Telefone, ID_Grupo, Variavel1, Variavel2 "+
                "FROM Contatos WHERE ID_Contato = '"+codigo+"'";
            SQLiteDataReader dr = BD.retornaQuery(query);
            while (dr.Read())
            {
                textNome.Text = dr[1].ToString();
                textTelefone.Text = dr[2].ToString();
                textVariavel01.Text = dr[4].ToString();
                textVariavel02.Text = dr[5].ToString();
                if(comboGrupo.Items.Count > 0)
                    comboGrupo.SelectedValue = Convert.ToInt32(dr[3]);

            } dr.Close();
        }

        private void buttonRemover_Click(object sender, EventArgs e)
        {
            try
            {
                codigo = gridContatos.CurrentRow.Cells["ID_Contato"].Value.ToString();
                BD.executaQuery("DELETE FROM Contatos WHERE ID_Contato = '" + codigo + "'");
                Contatos_Load(sender, e);

                //INABILITA OS CAMPOS DE CADASTRO DO USUÁRIO E ZERA OS VALORES
                textNome.Enabled = false; textNome.Text = null;
                textTelefone.Enabled = false; textTelefone.Text = null;
                comboGrupo.Enabled = false; comboGrupo.SelectedIndex = -1;
                textVariavel01.Enabled = false; textVariavel01.Text = null;
                textVariavel02.Enabled = false; textVariavel02.Text = null;
            }
            catch { }
            
        }

        private void textTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
                e.Handled = true;
        }

        private void buttonImportar_Click(object sender, EventArgs e)
        {
            Importacao importacao = new Importacao();
            importacao.ShowDialog();
            atualiza();
        }

        private void excluirTodosOsContatatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            BD.executaQuery("DELETE FROM Contatos");
            atualiza();
            this.Cursor = Cursors.Default;
        }
    }
}
