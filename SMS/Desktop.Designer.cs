﻿namespace SMS
{
    partial class Desktop
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">verdade se for necessário descartar os recursos gerenciados; caso contrário, falso.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte do Designer - não modifique
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Desktop));
            this.timerAgendamento = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.sistemaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importarContatosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportarContatosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.informaçõesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ferramentasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eMailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opçõesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.visualizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.campanhaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contatosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gruposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.entradaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saídaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enviadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aparelhosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.onlineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.licençaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.labelCancelando = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureImagem = new System.Windows.Forms.PictureBox();
            this.buttonReceber = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonEnviar = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonParar = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.buttonContatos = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonBlackList = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonConf = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonEntrada = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonEnviados = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonAparelhos = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonGrupos = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonSaida = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonCampanha = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.painelPrincipal = new System.Windows.Forms.Panel();
            this.bwsReceber = new System.ComponentModel.BackgroundWorker();
            this.timerVerifica = new System.Windows.Forms.Timer(this.components);
            this.bwVerificador = new System.ComponentModel.BackgroundWorker();
            this.timerContadoresMenu = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureImagem)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // timerAgendamento
            // 
            this.timerAgendamento.Interval = 1000;
            this.timerAgendamento.Tick += new System.EventHandler(this.timerAgendamento_Tick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sistemaToolStripMenuItem,
            this.ferramentasToolStripMenuItem,
            this.visualizarToolStripMenuItem,
            this.ajudaToolStripMenuItem,
            this.licençaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(811, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // sistemaToolStripMenuItem
            // 
            this.sistemaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importarContatosToolStripMenuItem,
            this.exportarContatosToolStripMenuItem,
            this.toolStripSeparator1,
            this.informaçõesToolStripMenuItem,
            this.sairToolStripMenuItem});
            this.sistemaToolStripMenuItem.Name = "sistemaToolStripMenuItem";
            this.sistemaToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.sistemaToolStripMenuItem.Text = "Sistema";
            // 
            // importarContatosToolStripMenuItem
            // 
            this.importarContatosToolStripMenuItem.Name = "importarContatosToolStripMenuItem";
            this.importarContatosToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.importarContatosToolStripMenuItem.Text = "Importar Contatos";
            this.importarContatosToolStripMenuItem.Click += new System.EventHandler(this.importarContatosToolStripMenuItem_Click);
            // 
            // exportarContatosToolStripMenuItem
            // 
            this.exportarContatosToolStripMenuItem.Name = "exportarContatosToolStripMenuItem";
            this.exportarContatosToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.exportarContatosToolStripMenuItem.Text = "Exportar Contatos";
            this.exportarContatosToolStripMenuItem.Click += new System.EventHandler(this.exportarContatosToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(168, 6);
            // 
            // informaçõesToolStripMenuItem
            // 
            this.informaçõesToolStripMenuItem.Name = "informaçõesToolStripMenuItem";
            this.informaçõesToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.informaçõesToolStripMenuItem.Text = "Informações";
            // 
            // sairToolStripMenuItem
            // 
            this.sairToolStripMenuItem.Name = "sairToolStripMenuItem";
            this.sairToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.sairToolStripMenuItem.Text = "Sair";
            // 
            // ferramentasToolStripMenuItem
            // 
            this.ferramentasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eMailToolStripMenuItem,
            this.opçõesToolStripMenuItem});
            this.ferramentasToolStripMenuItem.Name = "ferramentasToolStripMenuItem";
            this.ferramentasToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.ferramentasToolStripMenuItem.Text = "Ferramentas";
            // 
            // eMailToolStripMenuItem
            // 
            this.eMailToolStripMenuItem.Name = "eMailToolStripMenuItem";
            this.eMailToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.eMailToolStripMenuItem.Text = "EMail";
            this.eMailToolStripMenuItem.Click += new System.EventHandler(this.eMailToolStripMenuItem_Click);
            // 
            // opçõesToolStripMenuItem
            // 
            this.opçõesToolStripMenuItem.Name = "opçõesToolStripMenuItem";
            this.opçõesToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.opçõesToolStripMenuItem.Text = "Opções";
            this.opçõesToolStripMenuItem.Click += new System.EventHandler(this.opçõesToolStripMenuItem_Click);
            // 
            // visualizarToolStripMenuItem
            // 
            this.visualizarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.campanhaToolStripMenuItem,
            this.contatosToolStripMenuItem,
            this.gruposToolStripMenuItem,
            this.entradaToolStripMenuItem,
            this.saídaToolStripMenuItem,
            this.enviadosToolStripMenuItem,
            this.aparelhosToolStripMenuItem});
            this.visualizarToolStripMenuItem.Name = "visualizarToolStripMenuItem";
            this.visualizarToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.visualizarToolStripMenuItem.Text = "Visualizar";
            // 
            // campanhaToolStripMenuItem
            // 
            this.campanhaToolStripMenuItem.Name = "campanhaToolStripMenuItem";
            this.campanhaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.campanhaToolStripMenuItem.Text = "Campanha";
            this.campanhaToolStripMenuItem.Click += new System.EventHandler(this.campanhaToolStripMenuItem_Click);
            // 
            // contatosToolStripMenuItem
            // 
            this.contatosToolStripMenuItem.Name = "contatosToolStripMenuItem";
            this.contatosToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.contatosToolStripMenuItem.Text = "Contatos";
            this.contatosToolStripMenuItem.Click += new System.EventHandler(this.contatosToolStripMenuItem_Click);
            // 
            // gruposToolStripMenuItem
            // 
            this.gruposToolStripMenuItem.Name = "gruposToolStripMenuItem";
            this.gruposToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.gruposToolStripMenuItem.Text = "Grupos";
            this.gruposToolStripMenuItem.Click += new System.EventHandler(this.gruposToolStripMenuItem_Click);
            // 
            // entradaToolStripMenuItem
            // 
            this.entradaToolStripMenuItem.Name = "entradaToolStripMenuItem";
            this.entradaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.entradaToolStripMenuItem.Text = "Entrada";
            this.entradaToolStripMenuItem.Click += new System.EventHandler(this.entradaToolStripMenuItem_Click);
            // 
            // saídaToolStripMenuItem
            // 
            this.saídaToolStripMenuItem.Name = "saídaToolStripMenuItem";
            this.saídaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.saídaToolStripMenuItem.Text = "Saída";
            this.saídaToolStripMenuItem.Click += new System.EventHandler(this.saídaToolStripMenuItem_Click);
            // 
            // enviadosToolStripMenuItem
            // 
            this.enviadosToolStripMenuItem.Name = "enviadosToolStripMenuItem";
            this.enviadosToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.enviadosToolStripMenuItem.Text = "Enviados";
            this.enviadosToolStripMenuItem.Click += new System.EventHandler(this.enviadosToolStripMenuItem_Click);
            // 
            // aparelhosToolStripMenuItem
            // 
            this.aparelhosToolStripMenuItem.Name = "aparelhosToolStripMenuItem";
            this.aparelhosToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.aparelhosToolStripMenuItem.Text = "Aparelhos";
            this.aparelhosToolStripMenuItem.Click += new System.EventHandler(this.aparelhosToolStripMenuItem_Click);
            // 
            // ajudaToolStripMenuItem
            // 
            this.ajudaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.onlineToolStripMenuItem});
            this.ajudaToolStripMenuItem.Name = "ajudaToolStripMenuItem";
            this.ajudaToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.ajudaToolStripMenuItem.Text = "Ajuda";
            // 
            // onlineToolStripMenuItem
            // 
            this.onlineToolStripMenuItem.Name = "onlineToolStripMenuItem";
            this.onlineToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.onlineToolStripMenuItem.Text = "Online";
            this.onlineToolStripMenuItem.Click += new System.EventHandler(this.onlineToolStripMenuItem_Click);
            // 
            // licençaToolStripMenuItem
            // 
            this.licençaToolStripMenuItem.Name = "licençaToolStripMenuItem";
            this.licençaToolStripMenuItem.Size = new System.Drawing.Size(108, 20);
            this.licençaToolStripMenuItem.Text = "Conf. de Sistema";
            this.licençaToolStripMenuItem.Click += new System.EventHandler(this.licençaToolStripMenuItem_Click);
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.pictureImagem);
            this.panel1.Controls.Add(this.buttonReceber);
            this.panel1.Controls.Add(this.buttonEnviar);
            this.panel1.Controls.Add(this.buttonParar);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(811, 79);
            this.panel1.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel4);
            this.groupBox1.Location = new System.Drawing.Point(384, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(241, 70);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Status";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.labelCancelando);
            this.panel4.Location = new System.Drawing.Point(20, 19);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(215, 47);
            this.panel4.TabIndex = 5;
            // 
            // labelCancelando
            // 
            this.labelCancelando.Location = new System.Drawing.Point(10, 14);
            this.labelCancelando.Name = "labelCancelando";
            this.labelCancelando.Size = new System.Drawing.Size(160, 20);
            this.labelCancelando.TabIndex = 0;
            this.labelCancelando.Values.Text = "CANCELANDO, AGUARDE...";
            this.labelCancelando.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(642, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(183, 40);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // pictureImagem
            // 
            this.pictureImagem.Location = new System.Drawing.Point(615, 0);
            this.pictureImagem.Name = "pictureImagem";
            this.pictureImagem.Size = new System.Drawing.Size(132, 79);
            this.pictureImagem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureImagem.TabIndex = 3;
            this.pictureImagem.TabStop = false;
            // 
            // buttonReceber
            // 
            this.buttonReceber.Location = new System.Drawing.Point(130, 3);
            this.buttonReceber.Name = "buttonReceber";
            this.buttonReceber.Size = new System.Drawing.Size(121, 73);
            this.buttonReceber.TabIndex = 2;
            this.buttonReceber.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonReceber.Values.Image")));
            this.buttonReceber.Values.Text = "Receber";
            this.buttonReceber.Click += new System.EventHandler(this.buttonReceber_Click);
            // 
            // buttonEnviar
            // 
            this.buttonEnviar.Location = new System.Drawing.Point(3, 3);
            this.buttonEnviar.Name = "buttonEnviar";
            this.buttonEnviar.Size = new System.Drawing.Size(121, 73);
            this.buttonEnviar.TabIndex = 1;
            this.buttonEnviar.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonEnviar.Values.Image")));
            this.buttonEnviar.Values.Text = "Enviar";
            this.buttonEnviar.Click += new System.EventHandler(this.buttonEnviar_Click);
            // 
            // buttonParar
            // 
            this.buttonParar.Location = new System.Drawing.Point(257, 3);
            this.buttonParar.Name = "buttonParar";
            this.buttonParar.Size = new System.Drawing.Size(121, 73);
            this.buttonParar.TabIndex = 0;
            this.buttonParar.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonParar.Values.Image")));
            this.buttonParar.Values.Text = "Parar";
            this.buttonParar.Visible = false;
            this.buttonParar.Click += new System.EventHandler(this.buttonParar_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 103);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 386);
            this.panel2.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel3.BackColor = System.Drawing.SystemColors.HighlightText;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.buttonContatos);
            this.panel3.Controls.Add(this.buttonBlackList);
            this.panel3.Controls.Add(this.buttonConf);
            this.panel3.Controls.Add(this.buttonEntrada);
            this.panel3.Controls.Add(this.buttonEnviados);
            this.panel3.Controls.Add(this.buttonAparelhos);
            this.panel3.Controls.Add(this.buttonGrupos);
            this.panel3.Controls.Add(this.buttonSaida);
            this.panel3.Controls.Add(this.buttonCampanha);
            this.panel3.Location = new System.Drawing.Point(12, 15);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(177, 357);
            this.panel3.TabIndex = 3;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // buttonContatos
            // 
            this.buttonContatos.Location = new System.Drawing.Point(3, 42);
            this.buttonContatos.Name = "buttonContatos";
            this.buttonContatos.Size = new System.Drawing.Size(168, 35);
            this.buttonContatos.TabIndex = 1;
            this.buttonContatos.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonContatos.Values.Image")));
            this.buttonContatos.Values.Text = "      Contatos";
            this.buttonContatos.Click += new System.EventHandler(this.buttonContatos_Click);
            // 
            // buttonBlackList
            // 
            this.buttonBlackList.Location = new System.Drawing.Point(2, 316);
            this.buttonBlackList.Name = "buttonBlackList";
            this.buttonBlackList.Size = new System.Drawing.Size(168, 35);
            this.buttonBlackList.TabIndex = 7;
            this.buttonBlackList.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonBlackList.Values.Image")));
            this.buttonBlackList.Values.Text = "    Black List";
            this.buttonBlackList.Click += new System.EventHandler(this.buttonBlackList_Click);
            // 
            // buttonConf
            // 
            this.buttonConf.Location = new System.Drawing.Point(3, 120);
            this.buttonConf.Name = "buttonConf";
            this.buttonConf.Size = new System.Drawing.Size(168, 35);
            this.buttonConf.TabIndex = 6;
            this.buttonConf.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonConf.Values.Image")));
            this.buttonConf.Values.Text = " Config. de Envio";
            this.buttonConf.Click += new System.EventHandler(this.buttonConf_Click);
            // 
            // buttonEntrada
            // 
            this.buttonEntrada.Location = new System.Drawing.Point(3, 198);
            this.buttonEntrada.Name = "buttonEntrada";
            this.buttonEntrada.Size = new System.Drawing.Size(168, 35);
            this.buttonEntrada.TabIndex = 3;
            this.buttonEntrada.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonEntrada.Values.Image")));
            this.buttonEntrada.Values.Text = "      Entrada";
            this.buttonEntrada.Click += new System.EventHandler(this.buttonEntrada_Click);
            // 
            // buttonEnviados
            // 
            this.buttonEnviados.Location = new System.Drawing.Point(3, 276);
            this.buttonEnviados.Name = "buttonEnviados";
            this.buttonEnviados.Size = new System.Drawing.Size(168, 35);
            this.buttonEnviados.TabIndex = 6;
            this.buttonEnviados.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonEnviados.Values.Image")));
            this.buttonEnviados.Values.Text = "    Enviados";
            this.buttonEnviados.Click += new System.EventHandler(this.buttonEnviados_Click);
            // 
            // buttonAparelhos
            // 
            this.buttonAparelhos.Location = new System.Drawing.Point(3, 159);
            this.buttonAparelhos.Name = "buttonAparelhos";
            this.buttonAparelhos.Size = new System.Drawing.Size(168, 35);
            this.buttonAparelhos.TabIndex = 5;
            this.buttonAparelhos.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonAparelhos.Values.Image")));
            this.buttonAparelhos.Values.Text = "Aparelhos";
            this.buttonAparelhos.Click += new System.EventHandler(this.buttonAparelhos_Click);
            // 
            // buttonGrupos
            // 
            this.buttonGrupos.Location = new System.Drawing.Point(3, 3);
            this.buttonGrupos.Name = "buttonGrupos";
            this.buttonGrupos.Size = new System.Drawing.Size(168, 35);
            this.buttonGrupos.TabIndex = 2;
            this.buttonGrupos.Values.ExtraText = "    ";
            this.buttonGrupos.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonGrupos.Values.Image")));
            this.buttonGrupos.Values.Text = "     Grupos";
            this.buttonGrupos.Click += new System.EventHandler(this.buttonGrupos_Click);
            // 
            // buttonSaida
            // 
            this.buttonSaida.Location = new System.Drawing.Point(2, 237);
            this.buttonSaida.Name = "buttonSaida";
            this.buttonSaida.Size = new System.Drawing.Size(168, 35);
            this.buttonSaida.TabIndex = 4;
            this.buttonSaida.Values.ExtraText = "    ";
            this.buttonSaida.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonSaida.Values.Image")));
            this.buttonSaida.Values.Text = "       Saída";
            this.buttonSaida.Click += new System.EventHandler(this.buttonSaida_Click);
            // 
            // buttonCampanha
            // 
            this.buttonCampanha.Location = new System.Drawing.Point(3, 81);
            this.buttonCampanha.Name = "buttonCampanha";
            this.buttonCampanha.Size = new System.Drawing.Size(168, 35);
            this.buttonCampanha.TabIndex = 0;
            this.buttonCampanha.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonCampanha.Values.Image")));
            this.buttonCampanha.Values.Text = "   Campanha";
            this.buttonCampanha.Click += new System.EventHandler(this.buttonCampanha_Click);
            // 
            // painelPrincipal
            // 
            this.painelPrincipal.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.painelPrincipal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.painelPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.painelPrincipal.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.painelPrincipal.Location = new System.Drawing.Point(200, 103);
            this.painelPrincipal.Name = "painelPrincipal";
            this.painelPrincipal.Size = new System.Drawing.Size(611, 386);
            this.painelPrincipal.TabIndex = 3;
            this.painelPrincipal.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.painelPrincipal_ControlAdded);
            // 
            // bwsReceber
            // 
            this.bwsReceber.WorkerSupportsCancellation = true;
            this.bwsReceber.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwsReceber_DoWork);
            // 
            // timerVerifica
            // 
            this.timerVerifica.Interval = 1000;
            this.timerVerifica.Tick += new System.EventHandler(this.timerVerifica_Tick);
            // 
            // bwVerificador
            // 
            this.bwVerificador.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwVerificador_DoWork);
            this.bwVerificador.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwVerificador_RunWorkerCompleted);
            // 
            // timerContadoresMenu
            // 
            this.timerContadoresMenu.Interval = 1000;
            this.timerContadoresMenu.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Desktop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 489);
            this.Controls.Add(this.painelPrincipal);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(827, 527);
            this.Name = "Desktop";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Torpedos Brasil - Sistema para SMS Marketing";
            this.MaximizedBoundsChanged += new System.EventHandler(this.Desktop_MaximizedBoundsChanged);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Desktop_FormClosing);
            this.Load += new System.EventHandler(this.Desktop_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureImagem)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem sistemaToolStripMenuItem;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.Panel panel1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonReceber;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonEnviar;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonParar;
        private System.Windows.Forms.ToolStripMenuItem ferramentasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opçõesToolStripMenuItem;
        private System.Windows.Forms.Panel panel3;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonEnviados;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonAparelhos;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonSaida;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonEntrada;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonGrupos;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonContatos;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonCampanha;
        private System.Windows.Forms.ToolStripMenuItem visualizarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem campanhaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contatosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gruposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem entradaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saídaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enviadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aparelhosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajudaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem onlineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importarContatosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportarContatosToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker bwsReceber;
        private System.Windows.Forms.ToolStripMenuItem licençaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem informaçõesToolStripMenuItem;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonBlackList;
        private System.Windows.Forms.ToolStripMenuItem eMailToolStripMenuItem;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonConf;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Timer timerVerifica;
        public System.Windows.Forms.Panel painelPrincipal;
        private System.ComponentModel.BackgroundWorker bwVerificador;
        public System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel labelCancelando;
        private System.Windows.Forms.Timer timerContadoresMenu;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureImagem;
        private System.Windows.Forms.Timer timerAgendamento;
    }
}

