﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Threading;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Text.RegularExpressions;
using System.Runtime.Remoting.Channels;
using GsmComm.PduConverter;
using GsmComm.PduConverter.SmartMessaging;
using GsmComm.GsmCommunication;
using GsmComm.Interfaces;
using GsmComm.Server;
using System.Globalization;
using System.Windows.Forms;
using AutoUpdaterDotNET;

namespace SMS
{
    public partial class Desktop : Form
    {

        public static ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;

        public Desktop()
        {
            InitializeComponent();
            kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            kryptonHeader1.Text = "Caixa de saída";

            timerContadoresMenu.Start();

            MAIN = this;

        }

        public static Desktop MAIN = null;
        Campanha campanha = null;
        Contatos contatos = null;
        Grupos grupos = null;
        Caixa_Entrada caixaEntrada = null;
        Caixa_Saida caixaSaida = null;
        Caixa_Enviados caixaEnviados = null;
        Config_Email confEmail = null;
        AparelhosForm aparelhos = null;
        Opcoes opcoes = null;
        Envio_Mensagens envioMensagens = null;
        BlackList blackList = null;
        ListBox numeros = new ListBox();
        ListBox portas = new ListBox();
        ListBox mensagem = new ListBox();
        ListBox idmensagem = new ListBox();
        string porta = String.Empty;
        int verificadorErro = 0;
        List<string> listaPortas = new List<string>();


        private delegate void SetTextCallback(string text);
        private SmsServer smsServer;
        int tempoPausa = 1;
        int contador = 0;
        int contadorTempo = 0;
        int contadorTimer = 0;
        int TempoQtde = 0;
        int TempoParada = 0;
        int TimerQtde = 0;
        int TimerParada = 0;
        int verificaErro = 0;
        public string inicio = String.Empty;
        public string fim = String.Empty;
        string email = String.Empty;
        List<Thread> threads = new List<Thread>();


        void campanha_Disposed(object sender, EventArgs e)
        {
            campanha = null;
        }

        void contato_Disposed(object sender, EventArgs e)
        {
            contatos = null;
        }

        void grupos_Disposed(object sender, EventArgs e)
        {
            grupos = null;
        }

        void caixaEntrada_Disposed(object sender, EventArgs e)
        {
            caixaEntrada = null;
        }

        void caixaSaida_Disposed(object sender, EventArgs e)
        {
            caixaSaida = null;
        }

        void caixaEnviados_Disposed(object sender, EventArgs e)
        {
            caixaEnviados = null;
        }

        void aparelhos_Disposed(object sender, EventArgs e)
        {
            aparelhos = null;
        }

        void opcoes_Disposed(object sender, EventArgs e)
        {
            opcoes = null;
        }
        void blackList_Disposed(object sender, EventArgs e)
        {
            blackList = null;
        }

        private void buttonCampanha_Click(object sender, EventArgs e)
        {
            if (contatos != null)
                contatos.Close();
            if (grupos != null)
                grupos.Close();
            if (caixaEntrada != null)
                caixaEntrada.Close();
            if (caixaSaida != null)
                caixaSaida.Close();
            if (caixaEnviados != null)
                caixaEnviados.Close();
            if (aparelhos != null)
                aparelhos.Close();
            if (opcoes != null)
                opcoes.Close();
            if (blackList != null)
                blackList.Close();
            if (Application.OpenForms.OfType<Campanha>().Count() <= 0)
            {
                campanha = new Campanha(-1);
                campanha.TopLevel = false;
                painelPrincipal.Controls.Add(campanha);
                campanha.FormBorderStyle = FormBorderStyle.None;
                campanha.Show();
            }
        }

        public void telaCampanha(int grupo)
        {
            
        }

        private void campanhaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (contatos != null)
                contatos.Close();
            if (grupos != null)
                grupos.Close();
            if (caixaEntrada != null)
                caixaEntrada.Close();
            if (caixaSaida != null)
                caixaSaida.Close();
            if (caixaEnviados != null)
                caixaEnviados.Close();
            if (aparelhos != null)
                aparelhos.Close();
            if (opcoes != null)
                opcoes.Close();
            if (blackList != null)
                blackList.Close();
            if (Application.OpenForms.OfType<Campanha>().Count() <= 0)
            {
                campanha = new Campanha(-1);
                campanha.TopLevel = false;
                painelPrincipal.Controls.Add(campanha);
                campanha.FormBorderStyle = FormBorderStyle.None;
                campanha.Show();
            }
        }

        private void buttonContatos_Click(object sender, EventArgs e)
        {
            if (campanha != null)
                campanha.Close();
            if (grupos != null)
                grupos.Close();
            if (caixaEntrada != null)
                caixaEntrada.Close();
            if (caixaSaida != null)
                caixaSaida.Close();
            if (caixaEnviados != null)
                caixaEnviados.Close();
            if (aparelhos != null)
                aparelhos.Close();
            if (opcoes != null)
                opcoes.Close();
            if (blackList != null)
                blackList.Close();
            if (Application.OpenForms.OfType<Contatos>().Count() <= 0)
            {
                contatos = new Contatos();
                contatos.TopLevel = false;
                painelPrincipal.Controls.Add(contatos);
                contatos.FormBorderStyle = FormBorderStyle.None;
                contatos.Show();
            }
        }

        private void contatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (campanha != null)
                campanha.Close();
            if (grupos != null)
                grupos.Close();
            if (caixaEntrada != null)
                caixaEntrada.Close();
            if (caixaSaida != null)
                caixaSaida.Close();
            if (caixaEnviados != null)
                caixaEnviados.Close();
            if (aparelhos != null)
                aparelhos.Close();
            if (opcoes != null)
                opcoes.Close();
            if (blackList != null)
                blackList.Close();
            if (Application.OpenForms.OfType<Contatos>().Count() <= 0)
            {
                contatos = new Contatos();
                contatos.TopLevel = false;
                painelPrincipal.Controls.Add(contatos);
                contatos.FormBorderStyle = FormBorderStyle.None;
                contatos.Show();
            }
        }

        private void buttonGrupos_Click(object sender, EventArgs e)
        {
            if (contatos != null)
                contatos.Close();
            if (campanha != null)
                campanha.Close();
            if (caixaEntrada != null)
                caixaEntrada.Close();
            if (caixaSaida != null)
                caixaSaida.Close();
            if (caixaEnviados != null)
                caixaEnviados.Close();
            if (aparelhos != null)
                aparelhos.Close();
            if (opcoes != null)
                opcoes.Close();
            if (blackList != null)
                blackList.Close();
            if (Application.OpenForms.OfType<Grupos>().Count() <= 0)
            {
                grupos = new Grupos();
                grupos.TopLevel = false;
                painelPrincipal.Controls.Add(grupos);
                grupos.FormBorderStyle = FormBorderStyle.None;
                grupos.Show();
            }
        }

        private void gruposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (contatos != null)
                contatos.Close();
            if (campanha != null)
                campanha.Close();
            if (caixaEntrada != null)
                caixaEntrada.Close();
            if (caixaSaida != null)
                caixaSaida.Close();
            if (caixaEnviados != null)
                caixaEnviados.Close();
            if (aparelhos != null)
                aparelhos.Close();
            if (opcoes != null)
                opcoes.Close();
            if (blackList != null)
                blackList.Close();
            if (Application.OpenForms.OfType<Grupos>().Count() <= 0)
            {
                grupos = new Grupos();
                grupos.TopLevel = false;
                painelPrincipal.Controls.Add(grupos);
                grupos.FormBorderStyle = FormBorderStyle.None;
                grupos.Show();
            }
        }

        private void buttonEntrada_Click(object sender, EventArgs e)
        {
            if (contatos != null)
                contatos.Close();
            if (campanha != null)
                campanha.Close();
            if (grupos != null)
                grupos.Close();
            if (caixaSaida != null)
                caixaSaida.Close();
            if (caixaEnviados != null)
                caixaEnviados.Close();
            if (aparelhos != null)
                aparelhos.Close();
            if (opcoes != null)
                opcoes.Close();
            if (blackList != null)
                blackList.Close();
            if (Application.OpenForms.OfType<Caixa_Entrada>().Count() <= 0)
            {
                caixaEntrada = new Caixa_Entrada();
                caixaEntrada.TopLevel = false;
                painelPrincipal.Controls.Add(caixaEntrada);
                caixaEntrada.FormBorderStyle = FormBorderStyle.None;
                caixaEntrada.Show();
            }
        }

        private void entradaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (contatos != null)
                contatos.Close();
            if (campanha != null)
                campanha.Close();
            if (grupos != null)
                grupos.Close();
            if (caixaSaida != null)
                caixaSaida.Close();
            if (caixaEnviados != null)
                caixaEnviados.Close();
            if (aparelhos != null)
                aparelhos.Close();
            if (opcoes != null)
                opcoes.Close();
            if (blackList != null)
                blackList.Close();
            if (Application.OpenForms.OfType<Caixa_Entrada>().Count() <= 0)
            {
                caixaEntrada = new Caixa_Entrada();
                caixaEntrada.TopLevel = false;
                painelPrincipal.Controls.Add(caixaEntrada);
                caixaEntrada.FormBorderStyle = FormBorderStyle.None;
                caixaEntrada.Show();
            }
        }

        private void buttonSaida_Click(object sender, EventArgs e)
        {
            if (contatos != null)
                contatos.Close();
            if (campanha != null)
                campanha.Close();
            if (grupos != null)
                grupos.Close();
            if (caixaEntrada != null)
                caixaEntrada.Close();
            if (caixaEnviados != null)
                caixaEnviados.Close();
            if (aparelhos != null)
                aparelhos.Close();
            if (opcoes != null)
                opcoes.Close();
            if (blackList != null)
                blackList.Close();
            if (Application.OpenForms.OfType<Caixa_Saida>().Count() <= 0)
            {
                caixaSaida = new Caixa_Saida();                     
                caixaSaida.TopLevel = false;                        
                painelPrincipal.Controls.Add(caixaSaida);           
                caixaSaida.FormBorderStyle = FormBorderStyle.None;

                caixaSaida.Controls.Add(Desktop.kryptonHeader1);
                caixaSaida.Show();     
            }
        }

        private void saídaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (contatos != null)
                contatos.Close();
            if (campanha != null)
                campanha.Close();
            if (grupos != null)
                grupos.Close();
            if (caixaEntrada != null)
                caixaEntrada.Close();
            if (caixaEnviados != null)
                caixaEnviados.Close();
            if (aparelhos != null)
                aparelhos.Close();
            if (opcoes != null)
                opcoes.Close();
            if (blackList != null)
                blackList.Close();
            if (Application.OpenForms.OfType<Caixa_Saida>().Count() <= 0)
            {
                caixaSaida = new Caixa_Saida();
                caixaSaida.TopLevel = false;
                painelPrincipal.Controls.Add(caixaSaida);
                caixaSaida.FormBorderStyle = FormBorderStyle.None;
                caixaSaida.Show();
            }
        }

        private void buttonEnviados_Click(object sender, EventArgs e)
        {
            if (contatos != null)
                contatos.Close();
            if (campanha != null)
                campanha.Close();
            if (grupos != null)
                grupos.Close();
            if (caixaEntrada != null)
                caixaEntrada.Close();
            if (caixaSaida != null)
                caixaSaida.Close();
            if (aparelhos != null)
                aparelhos.Close();
            if (opcoes != null)
                opcoes.Close();
            if (blackList != null)
                blackList.Close();
            if (Application.OpenForms.OfType<Caixa_Enviados>().Count() <= 0)
            {
                caixaEnviados = new Caixa_Enviados();
                caixaEnviados.TopLevel = false;
                painelPrincipal.Controls.Add(caixaEnviados);
                caixaEnviados.FormBorderStyle = FormBorderStyle.None;
                caixaEnviados.Show();
            }
        }

        private void enviadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (contatos != null)
                contatos.Close();
            if (campanha != null)
                campanha.Close();
            if (grupos != null)
                grupos.Close();
            if (caixaEntrada != null)
                caixaEntrada.Close();
            if (caixaSaida != null)
                caixaSaida.Close();
            if (aparelhos != null)
                aparelhos.Close();
            if (opcoes != null)
                opcoes.Close();
            if (blackList != null)
                blackList.Close();
            if (Application.OpenForms.OfType<Caixa_Enviados>().Count() <= 0)
            {
                caixaEnviados = new Caixa_Enviados();
                caixaEnviados.TopLevel = false;
                painelPrincipal.Controls.Add(caixaEnviados);
                caixaEnviados.FormBorderStyle = FormBorderStyle.None;
                caixaEnviados.Show();
            }
        }

        private void buttonAparelhos_Click(object sender, EventArgs e)
        {
            if (contatos != null)
                contatos.Close();
            if (campanha != null)
                campanha.Close();
            if (grupos != null)
                grupos.Close();
            if (caixaEntrada != null)
                caixaEntrada.Close();
            if (caixaSaida != null)
                caixaSaida.Close();
            if (caixaEnviados != null)
                caixaEnviados.Close();
            if (opcoes != null)
                opcoes.Close();
            if (blackList != null)
                blackList.Close();
            if (Application.OpenForms.OfType<AparelhosForm>().Count() <= 0)
            {
                aparelhos = new AparelhosForm();
                aparelhos.TopLevel = false;
                painelPrincipal.Controls.Add(aparelhos);
                aparelhos.FormBorderStyle = FormBorderStyle.None;
                aparelhos.Show();
            }
        }

        private void aparelhosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (contatos != null)
                contatos.Close();
            if (campanha != null)
                campanha.Close();
            if (grupos != null)
                grupos.Close();
            if (caixaEntrada != null)
                caixaEntrada.Close();
            if (caixaSaida != null)
                caixaSaida.Close();
            if (caixaEnviados != null)
                caixaEnviados.Close();
            if (opcoes != null)
                opcoes.Close();
            if (blackList != null)
                blackList.Close();
            if (Application.OpenForms.OfType<AparelhosForm>().Count() <= 0)
            {
                aparelhos = new AparelhosForm();
                aparelhos.TopLevel = false;
                painelPrincipal.Controls.Add(aparelhos);
                aparelhos.FormBorderStyle = FormBorderStyle.None;
                aparelhos.Show();
            }
        }

        private void opçõesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (contatos != null)
                contatos.Close();
            if (campanha != null)
                campanha.Close();
            if (grupos != null)
                grupos.Close();
            if (caixaEntrada != null)
                caixaEntrada.Close();
            if (caixaSaida != null)
                caixaSaida.Close();
            if (caixaEnviados != null)
                caixaEnviados.Close();
            if (aparelhos != null)
                aparelhos.Close();
            if (blackList != null)
                blackList.Close();
            if (Application.OpenForms.OfType<Opcoes>().Count() <= 0)
            {
                opcoes = new Opcoes();
                opcoes.TopLevel = false;
                painelPrincipal.Controls.Add(opcoes);
                opcoes.FormBorderStyle = FormBorderStyle.None;
                opcoes.Show();
            }
        }

        private void buttonBlackList_Click(object sender, EventArgs e)
        {
            if (contatos != null)
                contatos.Close();
            if (campanha != null)
                campanha.Close();
            if (grupos != null)
                grupos.Close();
            if (caixaEntrada != null)
                caixaEntrada.Close();
            if (caixaSaida != null)
                caixaSaida.Close();
            if (caixaEnviados != null)
                caixaEnviados.Close();
            if (aparelhos != null)
                aparelhos.Close();
            if (opcoes != null)
                opcoes.Close();
            if (Application.OpenForms.OfType<BlackList>().Count() <= 0)
            {
                blackList = new BlackList();
                blackList.TopLevel = false;
                painelPrincipal.Controls.Add(blackList);
                blackList.FormBorderStyle = FormBorderStyle.None;
                blackList.Show();
            }
        }

        private void Desktop_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
            //BD.executaQuery("DELETE FROM Dispositivos");
        }
                

        private void buttonEnviar_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<Envio_Mensagens>().Count() <= 0)
            {
                envioMensagens = new Envio_Mensagens();
                envioMensagens.TopLevel = false;
                panel4.Controls.Add(envioMensagens);
                envioMensagens.FormBorderStyle = FormBorderStyle.None;
                envioMensagens.Show();
            }
            
            buttonEnviar.Enabled = false;
            buttonReceber.Enabled = false;
            buttonParar.Visible = true;
            buttonParar.Enabled = true;
            timerVerifica.Start();
        }

         

        private void Recebe(GsmCommMain porta)
        {
            string storage = GetMessageStorage();
            try
            {
                DecodedShortMessage[] messages = porta.ReadMessages(PhoneMessageStatus.ReceivedUnread, storage);
                foreach (DecodedShortMessage message in messages)
                {
                    ShowMessage(message.Data);
                }
            }
            catch (Exception erro)
            {
                //MessageBox.Show(erro.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        //RECEBIMENTO DE MENSAGENS DE TEXTO
        private void buttonReceber_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Mensagens recebidas com sucesso.", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        //private void bwsReceber_DoWork(object sender, DoWorkEventArgs e)
        //{
        //    conectar();
        //    int quantidadeNumeros = numeros.Items.Count;
        //    int quantidadePortas = portas.Items.Count;

        //    while (quantidadePortas > 0)
        //    {
        //        if (quantidadePortas >= 1)
        //        {
        //            Thread t1 = new Thread(new ParameterizedThreadStart(Parametriza));
        //            List<string> parametro = new List<string>();
        //            parametro.Add("Ecomm1");
        //            t1.Start(parametro);
        //        }
        //        if (quantidadePortas >= 2)
        //        {
        //            Thread t2 = new Thread(new ParameterizedThreadStart(Parametriza));
        //            List<string> parametro = new List<string>();
        //            parametro.Add("Ecomm2");
        //            t2.Start(parametro);
        //        }
        //        if (quantidadePortas >= 3)
        //        {
        //            Thread t3 = new Thread(new ParameterizedThreadStart(Parametriza));
        //            List<string> parametro = new List<string>();
        //            parametro.Add("Ecomm3");
        //            t3.Start(parametro);
        //        }
        //        if (quantidadePortas >= 4)
        //        {
        //            Thread t4 = new Thread(new ParameterizedThreadStart(Parametriza));
        //            List<string> parametro = new List<string>();
        //            parametro.Add("Ecomm4");
        //            t4.Start(parametro);
        //        }
        //        if (quantidadePortas >= 5)
        //        {
        //            Thread t5 = new Thread(new ParameterizedThreadStart(Parametriza));
        //            List<string> parametro = new List<string>();
        //            parametro.Add("Ecomm5");
        //            t5.Start(parametro);
        //        }
        //        if (quantidadePortas >= 6)
        //        {
        //            Thread t6 = new Thread(new ParameterizedThreadStart(Parametriza));
        //            List<string> parametro = new List<string>();
        //            parametro.Add("Ecomm6");
        //            t6.Start(parametro);
        //        }
        //        if (quantidadePortas >= 7)
        //        {
        //            Thread t7 = new Thread(new ParameterizedThreadStart(Parametriza));
        //            List<string> parametro = new List<string>();
        //            parametro.Add("Ecomm7");
        //            t7.Start(parametro);
        //        }
        //        if (quantidadePortas >= 8)
        //        {
        //            Thread t8 = new Thread(new ParameterizedThreadStart(Parametriza));
        //            List<string> parametro = new List<string>();
        //            parametro.Add("Ecomm8");
        //            t8.Start(parametro);
        //        }
        //        if (quantidadePortas >= 9)
        //        {
        //            Thread t9 = new Thread(new ParameterizedThreadStart(Parametriza));
        //            List<string> parametro = new List<string>();
        //            parametro.Add("Ecomm9");
        //            t9.Start(parametro);
        //        }
        //        if (quantidadePortas >= 10)
        //        {
        //            Thread t10 = new Thread(new ParameterizedThreadStart(Parametriza));
        //            List<string> parametro = new List<string>();
        //            parametro.Add("Ecomm10");
        //            t10.Start(parametro);
        //        }

                

        //        quantidadePortas = 0;
        //    }
        //    comm1.Close();
        //    comm2.Close();
        //    comm3.Close();
        //    comm4.Close();
        //    comm5.Close();
        //    comm6.Close();
        //    comm7.Close();
        //    comm8.Close();
        //    comm9.Close();
        //    comm10.Close();
        //}

        private string GetMessageStorage()
        {
            string storage = string.Empty;
            //if (rbMessageSIM.Checked)
            storage = PhoneStorageType.Sim;
            //if (rbMessagePhone.Checked)
            // storage = PhoneStorageType.Phone;

            if (storage.Length == 0)
                throw new ApplicationException("Unknown message storage.");
            else
                return storage;
        }

        private string StatusToString(PhoneMessageStatus status)
        {
            // Map a message status to a string
            string ret;
            switch (status)
            {
                case PhoneMessageStatus.All:
                    ret = "All";
                    break;
                case PhoneMessageStatus.ReceivedRead:
                    ret = "Read";
                    break;
                case PhoneMessageStatus.ReceivedUnread:
                    ret = "Unread";
                    break;
                case PhoneMessageStatus.StoredSent:
                    ret = "Sent";
                    break;
                case PhoneMessageStatus.StoredUnsent:
                    ret = "Unsent";
                    break;
                default:
                    ret = "Unknown (" + status.ToString() + ")";
                    break;
            }
            return ret;
        }

        private string ShowMessage(SmsPdu pdu)
        {
            string output;
            if (pdu is SmsSubmitPdu)
            {
                // Stored (sent/unsent) message
                SmsSubmitPdu data = (SmsSubmitPdu)pdu;
                output = "SENT/UNSENT MESSAGE" + "\n";
                output = output + "Recipient: " + data.DestinationAddress + "\n";
                output = output + "Message text: " + data.UserDataText + "\n";
                return output;
            }
            if (pdu is SmsDeliverPdu)
            {
                // Received message
                SmsDeliverPdu data = (SmsDeliverPdu)pdu;
                //output = "RECEIVED MESSAGE" + "\n";
                //output = output + "Sender: " + data.OriginatingAddress + "\n";
                //output = output + "Sent: " + data.SCTimestamp.ToString() + "\n";
                //output = output + "Message text: " + data.UserDataText + "\n";
                BD.executaQuery("INSERT INTO Caixa_Recebidos(Telefone,Data,Mensagem) VALUES( " +
                        "'" + data.OriginatingAddress + "', '" + data.SCTimestamp.ToString() + "', " +
                        "'" + data.UserDataText + "' )");


                //return output;
            }
            if (pdu is SmsStatusReportPdu)
            {
                // Status report
                SmsStatusReportPdu data = (SmsStatusReportPdu)pdu;
                output = "STATUS REPORT" + "\n";
                output = output + "Recipient: " + data.RecipientAddress + "\n";
                output = output + "Status: " + data.Status.ToString() + "\n";
                output = output + "Timestamp: " + data.DischargeTime.ToString() + "\n";
                output = output + "Message ref: " + data.MessageReference.ToString() + "\n";
                return output;
            }
            MessageBox.Show("Unknown message type: " + pdu.GetType().ToString());
            return null;
        }

        private void licençaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Licenca licenca = new Licenca();
            licenca.ShowDialog();
            carregaConfig();
            verificaLicenca();
        }

        private void timerAgendamento_Tick(object sender, EventArgs e)
        {
            string date1 = System.DateTime.Now.TimeOfDay.ToString().Substring(0, 8);
            string date2 = inicio;
            string date3 = fim;

            if (date1 == date2)
            {
                if (buttonEnviar.Enabled)
                {
                    buttonEnviar_Click(null, null);
                    MessageBox.Show("Iniciando envios agendado.",
                        "Agendamento de mensagens.",
                       MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Não foi possível iniciar a campanha agendada pois já havia uma campanha em andamento.",
                        "Falha ao iniciar capanha agendada.",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

            if (date1 == date3) {


                if (buttonParar.Enabled)
                {
                    buttonParar_Click(null, null);
                    MessageBox.Show("Encerrando envios devido ao agendamento configurado.",
                        "Agendamento de mensagens.",
                       MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void Desktop_Load(object sender, EventArgs e)
        {
            timerAgendamento.Start();
            bwVerificador.RunWorkerAsync();
            Console.WriteLine("Iniciando auto updater");
            AutoUpdater.OpenDownloadPage = true;
            AutoUpdater.Start("http://www.spotdev.com.br/sms-marketing/sms-updater.xml");
        }

        private void preencheHardware()
        {
            int verificador = 0;
            SQLiteDataReader dr = BD.retornaQuery("SELECT HardwareKey FROM Configuracoes_Sistema");
            while (dr.Read())
                if (dr[0].ToString() == "")
                    verificador = 1;
            dr.Close();

            //if (verificador == 1)
                BD.executaQuery("UPDATE Configuracoes_Sistema SET HardwareKey = '" + HardwareKey.Value() + "'");
        }

        private void carregaAgendamento()
        {
            SQLiteDataReader dr = BD.retornaQuery("SELECT Inicio, Fim FROM Opcoes");
            while (dr.Read())
            {
                inicio = dr[0].ToString();
                fim = dr[1].ToString();
            } dr.Close();
        }

        private void carregaConfig()
        {
            try
            {
                SQLiteDataReader dr = BD.retornaQuery("SELECT Nome_Empresa, Imagem, Email FROM Configuracoes_Sistema");
                while (dr.Read())
                {
                    if (dr[0].ToString() == "")
                        this.Text = "Torpedos Brasil";
                    else
                        this.Text = "SMS - " + dr[0].ToString();

                    pictureImagem.ImageLocation = dr[1].ToString();
                    email = dr[2].ToString();
                    pictureBox1.Visible = false;
                    pictureImagem.Visible = true;

                    if (dr[1].ToString() == "")
                    {
                        pictureBox1.Visible = true;
                        pictureImagem.Visible = false;
                    }
                } dr.Close();
            }
            catch
            {
                verificadorErro = 1;
            }
        }

        private void verificaLicenca()
        {
            string licenca = null;
            string hardware = null;
            SQLiteDataReader dr = BD.retornaQuery("SELECT Licenca, HardwareKey FROM Configuracoes_Sistema");
            while (dr.Read())
            {
                licenca = dr[0].ToString();
                hardware = dr[1].ToString();
            } dr.Close();

            if (Criptografia.Decrypt(licenca, hardware))
            {
                habilitaSistema();
            }
            else
                desabilitaSistema();
        }

        private void desabilitaSistema()
        {
            try
            {
                panel2.Enabled = false;
                painelPrincipal.Enabled = false;
                buttonEnviar.Enabled = false;
                buttonReceber.Enabled = false;
                ferramentasToolStripMenuItem.Enabled = false;
                visualizarToolStripMenuItem.Enabled = false;
                sistemaToolStripMenuItem.Enabled = false;
            }
            catch { }
        }
        private void habilitaSistema()
        {
            try
            {
                panel2.Enabled = true;
                painelPrincipal.Enabled = true;
                buttonEnviar.Enabled = true;
                buttonReceber.Enabled = true;
                ferramentasToolStripMenuItem.Enabled = true;
                visualizarToolStripMenuItem.Enabled = true;
                sistemaToolStripMenuItem.Enabled = true;
            }
            catch { }
        }

        

        private void importarContatosToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                this.Cursor = Cursors.WaitCursor;
                OpenFileDialog file = new OpenFileDialog();
                file.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                if (file.ShowDialog() == DialogResult.OK)
                {
                    StreamReader stream = new StreamReader(@file.FileName);

                    string linha = null;
                    int linha_numero = 0;
                    while ((linha = stream.ReadLine()) != null)
                    {
                        string[] coluna = linha.Split(';');
                        var dados = new Contato
                        {
                            nome = (coluna.Length > 1) ? coluna[0] : "",
                            telefone = (coluna.Length > 1) ? coluna[1] : linha,
                            variavel1 = (coluna.Length > 1) ? coluna[2] : "",
                            variavel2 = (coluna.Length > 1) ? coluna[3] : "",
                            grupo = "-1"
                        };
                        linha_numero++;
                    }
                    stream.Close();
                    MessageBox.Show(linha_numero +" contatos importados com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception erro)
            {
                MessageBox.Show("Erro ao fazer a leitura do arquivo! Verifique se o mesmo esta de acordo com o exemplo", "Erro de leitura", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Cursor = Cursors.Default;
            }

        }

        private void exportarContatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                // Displays a SaveFileDialog so the user can save the Image
                // assigned to Button2.
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Text files (*.txt)|*.txt";
                saveFileDialog1.Title = "Exportar lista de números na BlackList";
                saveFileDialog1.ShowDialog();

                if (saveFileDialog1.FileName != "")
                {
                    StreamWriter stream = new StreamWriter(saveFileDialog1.FileName, true, Encoding.ASCII);
                    SQLiteDataReader dr = BD.retornaQuery("SELECT Nome, Telefone, Variavel1, Variavel2 FROM Contatos");
                    while (dr.Read())
                        stream.WriteLine(dr[0].ToString() + ";" + dr[1].ToString() + ";" + dr[2].ToString() + ";" + dr[3].ToString());
                    stream.Close();
                }
                MessageBox.Show("Lista de contatos exportada com sucesso", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch { }
        }

        private void bwSMS_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            buttonEnviar.Enabled = true;
            buttonReceber.Enabled = true;
        }

        private void eMailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<Config_Email>().Count() <= 0)
            {
                confEmail = new Config_Email();
                confEmail.TopLevel = true;
                confEmail.Show();
            }
            else
                confEmail.BringToFront();
        }

        private void buttonConf_Click(object sender, EventArgs e)
        {
            if (contatos != null)
                contatos.Close();
            if (campanha != null)
                campanha.Close();
            if (grupos != null)
                grupos.Close();
            if (caixaEntrada != null)
                caixaEntrada.Close();
            if (caixaSaida != null)
                caixaSaida.Close();
            if (caixaEnviados != null)
                caixaEnviados.Close();
            if (aparelhos != null)
                aparelhos.Close();
            if (blackList != null)
                blackList.Close();
            if (Application.OpenForms.OfType<Opcoes>().Count() <= 0)
            {
                opcoes = new Opcoes();
                opcoes.TopLevel = false;
                painelPrincipal.Controls.Add(opcoes);
                opcoes.FormBorderStyle = FormBorderStyle.None;
                opcoes.Show();
            }
        }

        public void buttonParar_Click(object sender, EventArgs e)
        {
            if(kryptonHeader1 != null)
                kryptonHeader1.Text = "Caixa de saída";
            buttonParar.BeginInvoke((Action)delegate ()
            {
                this.Cursor = Cursors.WaitCursor;
                labelCancelando.Visible = true;
                envioMensagens.Close();
                envioMensagens.Dispose();
                buttonEnviar.Enabled = true;
                buttonReceber.Enabled = true;
                labelCancelando.Visible = false;
                buttonParar.Visible = false;
                buttonParar.Enabled = false;
                this.Cursor = Cursors.Default;
            });
        }

        private void timerVerifica_Tick(object sender, EventArgs e)
        {
            
            if (Application.OpenForms.OfType<Envio_Mensagens>().Count() <= 0)
            {
                buttonEnviar.Enabled = true;
                buttonReceber.Enabled = true;
                buttonParar.Visible = false;
                buttonParar.Enabled = false;
                timerVerifica.Stop();
            }
        }

        private void bwVerificador_DoWork(object sender, DoWorkEventArgs e)
        {
            carregaAgendamento();
            preencheHardware();
            verificaLicenca();
        }

        private void bwVerificador_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            carregaConfig();
            if (verificadorErro == 1)
            {
                pictureBox1.Visible = true;
                pictureImagem.Visible = false;
                pictureImagem.Image = null;
            }

        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private void Desktop_MaximizedBoundsChanged(object sender, EventArgs e)
        {
            painelPrincipal.Refresh();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            List<String> aparelhos = new List<String>();
            List<Aparelho> salvos = BD.listAparelhosSalvos();

            List<Aparelho> aparelhosSegmentados = new List<Aparelho>();

            foreach (Aparelho aparelho in salvos)
            {
                foreach(String aparelho2 in SerialPort.GetPortNames())
                {
                    if(aparelho.com == aparelho2)
                    {
                        aparelhosSegmentados.Add(aparelho);
                        break;
                    }
                }
            }

            buttonAparelhos.Text = "Aparelhos (" + aparelhosSegmentados.Count + ")";
            buttonSaida.Text = "Saida (" +  BD.getCountSMS()[0] + ")";
            buttonEntrada.Text = "Entrada (" + BD.getCountSMS()[1] + ")";
            buttonEnviados.Text = "Enviados (" + BD.getCountSMS()[2] + ")";
        }

        private void bwsReceber_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void painelPrincipal_ControlAdded(object sender, ControlEventArgs e)
        {
            foreach (Control item in painelPrincipal.Controls)
            {
                item.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Left);
            }
        }

        private void onlineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.torpedosbrasil.com.br/login");
        }
    }
}
