﻿namespace SMS
{
    partial class Editar_Grupo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Editar_Grupo));
            this.listComGrupo = new ComponentFactory.Krypton.Toolkit.KryptonListBox();
            this.buttonSalvar = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonRemoverAll = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonAdicionarAll = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonRemover1 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonAdicionar1 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.listSemGrupo = new ComponentFactory.Krypton.Toolkit.KryptonListBox();
            this.kryptonLabel3 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel2 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.textNomeGrupo = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.labelTotalS = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.labelTotalG = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.bwSalvar = new System.ComponentModel.BackgroundWorker();
            this.labelPorcentagem = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.buttonCancelar = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.SuspendLayout();
            // 
            // listComGrupo
            // 
            this.listComGrupo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listComGrupo.HorizontalScrollbar = true;
            this.listComGrupo.Location = new System.Drawing.Point(404, 75);
            this.listComGrupo.Name = "listComGrupo";
            this.listComGrupo.Size = new System.Drawing.Size(268, 275);
            this.listComGrupo.TabIndex = 70;
            // 
            // buttonSalvar
            // 
            this.buttonSalvar.Location = new System.Drawing.Point(303, 228);
            this.buttonSalvar.Name = "buttonSalvar";
            this.buttonSalvar.Size = new System.Drawing.Size(78, 58);
            this.buttonSalvar.TabIndex = 68;
            this.buttonSalvar.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonSalvar.Values.Image")));
            this.buttonSalvar.Values.Text = "";
            this.buttonSalvar.Click += new System.EventHandler(this.buttonSalvar_Click);
            // 
            // buttonRemoverAll
            // 
            this.buttonRemoverAll.Location = new System.Drawing.Point(284, 168);
            this.buttonRemoverAll.Name = "buttonRemoverAll";
            this.buttonRemoverAll.Size = new System.Drawing.Size(111, 25);
            this.buttonRemoverAll.TabIndex = 65;
            this.buttonRemoverAll.Values.Text = "Remover <<";
            this.buttonRemoverAll.Click += new System.EventHandler(this.buttonRemoverAll_Click);
            // 
            // buttonAdicionarAll
            // 
            this.buttonAdicionarAll.Location = new System.Drawing.Point(284, 137);
            this.buttonAdicionarAll.Name = "buttonAdicionarAll";
            this.buttonAdicionarAll.Size = new System.Drawing.Size(111, 25);
            this.buttonAdicionarAll.TabIndex = 64;
            this.buttonAdicionarAll.Values.Text = "Adicionar >>";
            this.buttonAdicionarAll.Click += new System.EventHandler(this.buttonAdicionarAll_Click);
            // 
            // buttonRemover1
            // 
            this.buttonRemover1.Location = new System.Drawing.Point(284, 106);
            this.buttonRemover1.Name = "buttonRemover1";
            this.buttonRemover1.Size = new System.Drawing.Size(111, 25);
            this.buttonRemover1.TabIndex = 63;
            this.buttonRemover1.Values.Text = "Remover <";
            this.buttonRemover1.Click += new System.EventHandler(this.buttonRemover1_Click);
            // 
            // buttonAdicionar1
            // 
            this.buttonAdicionar1.Location = new System.Drawing.Point(284, 75);
            this.buttonAdicionar1.Name = "buttonAdicionar1";
            this.buttonAdicionar1.Size = new System.Drawing.Size(111, 25);
            this.buttonAdicionar1.TabIndex = 62;
            this.buttonAdicionar1.Values.Text = "Adicionar >";
            this.buttonAdicionar1.Click += new System.EventHandler(this.buttonAdicionar1_Click);
            // 
            // listSemGrupo
            // 
            this.listSemGrupo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listSemGrupo.HorizontalScrollbar = true;
            this.listSemGrupo.Location = new System.Drawing.Point(10, 75);
            this.listSemGrupo.Name = "listSemGrupo";
            this.listSemGrupo.Size = new System.Drawing.Size(268, 275);
            this.listSemGrupo.TabIndex = 61;
            // 
            // kryptonLabel3
            // 
            this.kryptonLabel3.Location = new System.Drawing.Point(401, 57);
            this.kryptonLabel3.Name = "kryptonLabel3";
            this.kryptonLabel3.Size = new System.Drawing.Size(118, 20);
            this.kryptonLabel3.TabIndex = 60;
            this.kryptonLabel3.Values.Text = "Membros do Grupo";
            // 
            // kryptonLabel2
            // 
            this.kryptonLabel2.Location = new System.Drawing.Point(10, 57);
            this.kryptonLabel2.Name = "kryptonLabel2";
            this.kryptonLabel2.Size = new System.Drawing.Size(126, 20);
            this.kryptonLabel2.TabIndex = 59;
            this.kryptonLabel2.Values.Text = "Membros sem Grupo";
            // 
            // textNomeGrupo
            // 
            this.textNomeGrupo.Location = new System.Drawing.Point(106, 11);
            this.textNomeGrupo.Name = "textNomeGrupo";
            this.textNomeGrupo.Size = new System.Drawing.Size(566, 20);
            this.textNomeGrupo.TabIndex = 58;
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.Location = new System.Drawing.Point(10, 11);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(102, 20);
            this.kryptonLabel1.TabIndex = 57;
            this.kryptonLabel1.Values.Text = "Nome do Grupo:";
            // 
            // labelTotalS
            // 
            this.labelTotalS.Location = new System.Drawing.Point(247, 57);
            this.labelTotalS.Name = "labelTotalS";
            this.labelTotalS.Size = new System.Drawing.Size(31, 20);
            this.labelTotalS.TabIndex = 66;
            this.labelTotalS.Values.Text = "( 0 )";
            // 
            // labelTotalG
            // 
            this.labelTotalG.Location = new System.Drawing.Point(641, 57);
            this.labelTotalG.Name = "labelTotalG";
            this.labelTotalG.Size = new System.Drawing.Size(31, 20);
            this.labelTotalG.TabIndex = 67;
            this.labelTotalG.Values.Text = "( 0 )";
            // 
            // bwSalvar
            // 
            this.bwSalvar.WorkerReportsProgress = true;
            this.bwSalvar.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwSalvar_DoWork);
            this.bwSalvar.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwSalvar_RunWorkerCompleted);
            // 
            // labelPorcentagem
            // 
            this.labelPorcentagem.Location = new System.Drawing.Point(331, 199);
            this.labelPorcentagem.Name = "labelPorcentagem";
            this.labelPorcentagem.Size = new System.Drawing.Size(17, 20);
            this.labelPorcentagem.TabIndex = 71;
            this.labelPorcentagem.Values.Text = "0";
            // 
            // buttonCancelar
            // 
            this.buttonCancelar.Location = new System.Drawing.Point(303, 292);
            this.buttonCancelar.Name = "buttonCancelar";
            this.buttonCancelar.Size = new System.Drawing.Size(78, 58);
            this.buttonCancelar.TabIndex = 69;
            this.buttonCancelar.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonCancelar.Values.Image")));
            this.buttonCancelar.Values.Text = "";
            this.buttonCancelar.Click += new System.EventHandler(this.buttonCancelar_Click);
            // 
            // Editar_Grupo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 361);
            this.Controls.Add(this.labelPorcentagem);
            this.Controls.Add(this.listComGrupo);
            this.Controls.Add(this.buttonCancelar);
            this.Controls.Add(this.buttonSalvar);
            this.Controls.Add(this.buttonRemoverAll);
            this.Controls.Add(this.buttonAdicionarAll);
            this.Controls.Add(this.buttonRemover1);
            this.Controls.Add(this.buttonAdicionar1);
            this.Controls.Add(this.listSemGrupo);
            this.Controls.Add(this.kryptonLabel3);
            this.Controls.Add(this.kryptonLabel2);
            this.Controls.Add(this.textNomeGrupo);
            this.Controls.Add(this.kryptonLabel1);
            this.Controls.Add(this.labelTotalS);
            this.Controls.Add(this.labelTotalG);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Editar_Grupo";
            this.Text = "Editar Grupo";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Editar_Grupo_FormClosing);
            this.Load += new System.EventHandler(this.Editar_Grupo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonListBox listComGrupo;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonSalvar;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonRemoverAll;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonAdicionarAll;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonRemover1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonAdicionar1;
        private ComponentFactory.Krypton.Toolkit.KryptonListBox listSemGrupo;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel3;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel2;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textNomeGrupo;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel1;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel labelTotalS;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel labelTotalG;
        private System.ComponentModel.BackgroundWorker bwSalvar;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel labelPorcentagem;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonCancelar;
    }
}