﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SMS
{
    public partial class Editar_Grupo : Form
    {
        int idGrupo = 0;

        public Editar_Grupo(int ID)
        {
            InitializeComponent();
            idGrupo = ID;
        }

        private void Editar_Grupo_Load(object sender, EventArgs e)
        {
            SQLiteDataReader dr = BD.retornaQuery("SELECT Nome, Telefone, ID_Grupo, ID_Contato FROM Contatos");
            Console.WriteLine(dr[2].ToString());
            while (dr.Read())
            {
                try
                {
                    if (dr[2].ToString() == "-1")
                        listSemGrupo.Items.Add((dr[0].ToString() + " <" + dr[1].ToString() + ">"));
                    else if (Convert.ToInt32(dr[2].ToString()) == idGrupo)
                        listComGrupo.Items.Add((dr[0].ToString() + " <" + dr[1].ToString() + ">"));
                }
                catch (Exception ex) {
                    Console.WriteLine(ex.StackTrace);
                }

            } dr.Close();

            SQLiteDataReader dr1 = BD.retornaQuery("SELECT Nome FROM Grupos WHERE ID_Grupo = '"+idGrupo+"'");
            while (dr1.Read())
                textNomeGrupo.Text = dr1[0].ToString();

            atualizaTotal();
            labelPorcentagem.Enabled = false;
        }

        private void atualizaTotal()
        {
            //ATUALIZA OS CONTADORES ATUAIS 
            labelTotalS.Text = "(" + listSemGrupo.Items.Count + ")";
            labelTotalG.Text = "(" + listComGrupo.Items.Count + ")";
        }

        private void buttonAdicionar1_Click(object sender, EventArgs e)
        {
            if (listSemGrupo.SelectedIndex >= 0)
            {
                Object selecionado = listSemGrupo.SelectedItem;
                listComGrupo.Items.Add(selecionado);
                listSemGrupo.Items.Remove(selecionado);

                string telefoneArrastado = selecionado.ToString().Substring(selecionado.ToString().IndexOf(" <"), +
                        (selecionado.ToString().Length - selecionado.ToString().IndexOf(" <"))).Replace("<", "").Replace(">", "").Replace(" ", "");

                int count = listSemGrupo.Items.Count;
                List<object> aRemover = new List<object>();

                for (int i = 0; i < count; i++)
                {
                    Object listBoxItem = listSemGrupo.Items[i];
                    string telefone = listBoxItem.ToString().Substring(listBoxItem.ToString().IndexOf(" <"), +
                            (listBoxItem.ToString().Length - listBoxItem.ToString().IndexOf(" <"))).Replace("<", "").Replace(">", "").Replace(" ", "");
                    if (telefone.Equals(telefoneArrastado))
                    {
                        listComGrupo.Items.Add(listBoxItem);
                        aRemover.Add(listBoxItem);
                    }

                }

                foreach (object o in aRemover)
                {
                    listSemGrupo.Items.Remove(o);
                }

                if (aRemover.Count > 1)
                    MessageBox.Show("Movimentado " + aRemover.Count + " outros contatos por conter o mesmo número de celular.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else if (aRemover.Count == 1)
                    MessageBox.Show("Movimentado " + aRemover.Count + " contato a mais por conter o mesmo número de celular.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }

            atualizaTotal();
        }

        private void buttonAdicionarAll_Click(object sender, EventArgs e)
        {
            //inseri os valores na lista 2
            foreach (Object listBoxItem in listSemGrupo.Items)
                listComGrupo.Items.Add(listBoxItem);


            //remover os valores da lista 1
            foreach (Object listBoxItem2 in listComGrupo.Items)
            {
                foreach (Object listBoxItem in listSemGrupo.Items)
                {
                    if (listBoxItem.ToString() == listBoxItem2.ToString())
                    {
                        listSemGrupo.Items.Remove(listBoxItem2);
                        break;
                    }
                }
            }

            atualizaTotal();
        }

        private void buttonRemover1_Click(object sender, EventArgs e)
        {
            if (listComGrupo.SelectedIndex >= 0)
            {
                Object selecionado = listComGrupo.SelectedItem;
                listSemGrupo.Items.Add(selecionado);
                listComGrupo.Items.Remove(selecionado);

                string telefoneArrastado = selecionado.ToString().Substring(selecionado.ToString().IndexOf(" <"), +
                        (selecionado.ToString().Length - selecionado.ToString().IndexOf(" <"))).Replace("<", "").Replace(">", "").Replace(" ", "");

                int count = listComGrupo.Items.Count;
                List<object> aRemover = new List<object>();

                for (int i = 0; i < count; i++)
                {
                    Object listBoxItem = listComGrupo.Items[i];
                    string telefone = listBoxItem.ToString().Substring(listBoxItem.ToString().IndexOf(" <"), +
                            (listBoxItem.ToString().Length - listBoxItem.ToString().IndexOf(" <"))).Replace("<", "").Replace(">", "").Replace(" ", "");
                    if (telefone.Equals(telefoneArrastado))
                    {
                        listSemGrupo.Items.Add(listBoxItem);
                        aRemover.Add(listBoxItem);
                    }

                }

                foreach (object o in aRemover)
                {
                    listComGrupo.Items.Remove(o);
                }

                if (aRemover.Count > 1)
                    MessageBox.Show("Movimentado " + aRemover.Count + " outros contatos por conter o mesmo número de celular.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else if (aRemover.Count == 1)
                    MessageBox.Show("Movimentado " + aRemover.Count + " contato a mais por conter o mesmo número de celular.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }

            atualizaTotal();
        }

        private void buttonRemoverAll_Click(object sender, EventArgs e)
        {
            //inseri os valores na lista 2
            foreach (Object listBoxItem in listComGrupo.Items)
                listSemGrupo.Items.Add(listBoxItem);


            //remover os valores da lista 1
            listComGrupo.Items.Clear();

            atualizaTotal();
        }

        private void buttonSalvar_Click(object sender, EventArgs e)
        {
            //bwSalvar.DoWork += new DoWorkEventHandler(bwSalvar_DoWork);
            //bwSalvar.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwSalvar_RunWorkerCompleted);
            //bwSalvar.WorkerReportsProgress = true;
            bwSalvar.RunWorkerAsync();
            labelPorcentagem.Enabled = true;
            buttonSalvar.Enabled = false;
            buttonCancelar.Enabled = false;
        }

        private void Editar_Grupo_FormClosing(object sender, FormClosingEventArgs e)
        {
            Grupos obj = (Grupos)Application.OpenForms["Grupos"];
            obj.Grupos_Load(sender, e);  
        }

        private void bwSalvar_DoWork(object sender, DoWorkEventArgs e)
        {
            if (textNomeGrupo.Text == "")
                MessageBox.Show("Necessário preencher o nome do Grupo", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                BD.executaQuery("begin");
                BD.executaQuery("UPDATE GRUPOS SET Nome = '" + textNomeGrupo.Text + "' WHERE ID_Grupo = '" + idGrupo + "' ");
                string nome = null;
                string telefone = null;
                int a = 0;
                int b = 0;
                int total = listComGrupo.Items.Count + listSemGrupo.Items.Count;
                
                foreach (Object listBoxItem in listComGrupo.Items)
                {
                    a = a + 1;
                    nome = listBoxItem.ToString().Substring(0, listBoxItem.ToString().IndexOf(" <"));
                    telefone = listBoxItem.ToString().Substring(listBoxItem.ToString().IndexOf(" <"), +
                        (listBoxItem.ToString().Length - listBoxItem.ToString().IndexOf(" <"))).Replace("<", "").Replace(">", "").Replace(" ", "");
                    BD.executaQuery("UPDATE Contatos SET ID_GRUPO = '" + idGrupo + "' "+ //(SELECT ID_Grupo FROM Grupos WHERE Nome LIKE '" + textNomeGrupo.Text + "') " +
                                    "WHERE Nome LIKE '" + nome + "' AND Telefone LIKE '" + telefone + "' AND ID_Grupo <> '" + idGrupo + "' ");

                    int percents = (a*100)/total;
                    labelPorcentagem.Text = percents.ToString() + '%';

                }

                foreach (Object listBoxItem in listSemGrupo.Items)
                {
                    b = b + 1;
                    nome = listBoxItem.ToString().Substring(0, listBoxItem.ToString().IndexOf(" <"));
                    telefone = listBoxItem.ToString().Substring(listBoxItem.ToString().IndexOf(" <"), +
                        (listBoxItem.ToString().Length - listBoxItem.ToString().IndexOf(" <"))).Replace("<", "").Replace(">", "").Replace(" ", "");
                    BD.executaQuery("UPDATE Contatos SET ID_GRUPO = -1 " +
                                    "WHERE Nome LIKE '" + nome + "' AND Telefone LIKE '" + telefone + "' AND ID_Grupo <> '-1' ");
                    
                    int percents = (b * 100) / total;
                    labelPorcentagem.Text = percents.ToString() + '%';
                }
                BD.executaQuery("end");

            }
        }

        private void bwSalvar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            buttonSalvar.Enabled = true;
            buttonCancelar.Enabled = true;
            labelPorcentagem.Text = "0%";
            labelPorcentagem.Enabled = false;

            this.Close();
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
