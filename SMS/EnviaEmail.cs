﻿using System;
using System.Data.SQLite;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Windows.Forms;

namespace SMS
{
    class EnviaEmail
    {
        public static void envia(string Destinatario, string Assunto, string Mensagem)
        {
            string usuario = null;
            string senha = null;
            string enderecosmtp = null;
            string porta = null;
            bool ssl = false;

            SQLiteDataReader dr = BD.retornaQuery("SELECT Email, Senha, Smtp, Porta, SSL FROM Email");
            while (dr.Read())
            {
                usuario = dr[0].ToString();
                senha = dr[1].ToString();
                enderecosmtp = dr[2].ToString();
                porta = dr[3].ToString();
                ssl = Convert.ToInt32(dr[4].ToString()) == 1 ? true : false;
            } dr.Close();

            try
            {
                //Mensagem
                MailMessage message = new MailMessage();

                //Estes 2 campos corresponde a quem está enviando o e-mail
                message.Sender = new MailAddress(usuario);
                message.From = new MailAddress(usuario);

                //Para quem quer enviar o e-mail
                message.To.Add(new MailAddress(Destinatario));
                //message.Bcc.Add(new MailAddress(DestinatarioOculto));

                //Aqui você coloca o assunto
                message.Subject = Assunto;

                //E aqui você coloca o corpo do e-mail
                message.Body = Mensagem;

                //Se o corpo do e-mail for HTML, coloque o IsBodyHtml = true, caso contrário, = false
                message.IsBodyHtml = true;

                //Máxima prioridade
                message.Priority = MailPriority.High;

                //Smtp
                SmtpClient smtp = new SmtpClient();

                //Aqui você coloca seu usuário e senha
                smtp.Credentials = new NetworkCredential(usuario, senha);

                //Caso o servidor tenha SSL, habilite utilizando true 
                smtp.EnableSsl = ssl;

                //Aqui é o endereço do SMTP
                smtp.Host = enderecosmtp;

                //E a porta utilizada para conexão
                smtp.Port = Convert.ToInt32(porta);

                //Envia o e-mail :)
                smtp.Send(message);

                //Firula só pra saber que enviamos
                //MessageBox.Show("E-Mail enviado com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception erro)
            {
                //MessageBox.Show(erro.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
