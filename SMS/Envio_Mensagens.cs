﻿using GsmComm.GsmCommunication;
using GsmComm.Interfaces;
using GsmComm.PduConverter;
using GsmComm.PduConverter.SmartMessaging;
using GsmComm.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.Concurrent;
using System.IO.Ports;
using System.IO;

namespace SMS
{
    public partial class Envio_Mensagens : Form
    {
        ListBox numeros = new ListBox();
        ListBox portas = new ListBox();
        ListBox mensagem = new ListBox();
        ListBox idmensagem = new ListBox();

        string porta = String.Empty;
        private delegate void SetTextCallback(string text);
        private SmsServer smsServer;
        private Random random = new Random();
        int tempoPausa = 1;
        int contador = 0;
        int contadorTempo = 0;
        int contadorTimer = 0;
        int TempoQtde = 0;
        int TempoParada = 0;
        int TimerQtde = 0;
        int TimerParada = 0;
        int TempoOscilador = 0;
        int verificaErro = 0;
        int para = 0;
        string inicio = String.Empty;
        string fim = String.Empty;
        string email = String.Empty;
        private List<BackgroundWorker> _workers = new List<BackgroundWorker>();
        static ConcurrentQueue<Mensagem> listaMensagem = new ConcurrentQueue<Mensagem>();
        private Dictionary<GsmCommMain, Aparelho> aparelhosMap = new Dictionary<GsmCommMain, Aparelho>();
        private List<Thread> threadsDeEnvio = new List<Thread>();
        BackgroundWorker bwSMS = null;
        int cancela = 0;
        Thread t;

        public static Envio_Mensagens MAIN = null;


        public Envio_Mensagens()
        {
            InitializeComponent();
            MAIN = this;
        }

        private void Envio_Mensagens_Load(object sender, EventArgs e)
        {
            //LIMPA A LISTA COM OS NUMEROS
            numeros.Items.Clear();
            verificaOpcoes();
            Console.WriteLine("Iniciado");

            t = new Thread(teste);
            t.Start();
        }

        private void teste()
        {
            if (cancela == 1)
                return;
            try
            {
                label1.Text = "Carregando";
                int quantiadade = 0;
                int quantidade_longo = 0;
                int oscilador = 0;
                DateTime inicio;

                inicio = System.DateTime.Now;

                string port = string.Empty;
                SQLiteDataReader dr = BD.retornaQuery("SELECT Porta FROM Dispositivos");
                Aparelho aparelho = null;
                
                while (dr.Read())
                {
                    port = dr[0].ToString();
                    aparelho = BD.getAparelho(port);

                    int index = 0;
                    foreach(String porta in SerialPort.GetPortNames()){
                        if (aparelho.com == porta)
                            break;
                        else
                            index++;
                    }

                    if(index == SerialPort.GetPortNames().Length)
                    {
                        continue;
                    }

                    GsmCommMain portaComm = new GsmCommMain(port, 9600, 300);
                    aparelhosMap.Add(portaComm, aparelho);

                }
                dr.Close();
             
                if(aparelhosMap.Count == 0)
                {
                    Desktop.MAIN.buttonParar_Click(null, null);
                    return;
                }

                listaMensagem = new ConcurrentQueue<Mensagem>();
                SQLiteDataReader dr1 = BD.retornaQuery("SELECT * FROM Caixa_Saida");
                while (dr1.Read())
                {
                    var mensagem = new Mensagem
                    {
                        id_mensagem = dr1[0].ToString(),
                        destinatario = dr1[1].ToString(),
                        mensagem = dr1[2].ToString(),
                    };
                    listaMensagem.Enqueue(mensagem);
                }
                dr1.Close();
                
                int initialSize = listaMensagem.Count;
                // Loop em todos os aparelhos para fazer o envio assincrono
                foreach (GsmCommMain porta in aparelhosMap.Keys)
                {
                    Aparelho aparelhoDoMomento = null;
                    bool sucessoAoTentarPegarOAparelho = aparelhosMap.TryGetValue(porta, out aparelhoDoMomento);


                    if (aparelhoDoMomento == null) continue;

                    Thread t = new Thread(() => 
                        enviandoMessageEvent(inicio,aparelho,porta,quantidade_longo,quantiadade,initialSize)
                    );

                    threadsDeEnvio.Add(t);
                    t.Start();
                }
            }
            catch (Exception erro)
            {
                if (!erro.Message.Contains("anulado"))
                    MessageBox.Show("Erro no envio, necessario verificar o modem. \nMensagem: " + erro.Message);

            }
        }

        private void enviandoMessageEvent(DateTime inicio, Aparelho aparelho, GsmCommMain portaComm, int quantidade_longo, int quantiadade, int initialSize)
        {
            int index = 0;
            int tentativas = 0;
            const int maxima_tentativa = 5;
            while (listaMensagem.Count > 0)
            {

                if(tentativas > maxima_tentativa)
                {
                    MessageBox.Show("Porta " + portaComm.PortName + " sendo abortada devido a falha durante o envio." +
                                         "\n Certifique-se de que a mesma esteja funcionando corretamente.",
                                         "Falha durante o envio",
                                         MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
                }

                Desktop.kryptonHeader1.Text = "Caixa de saída (" + (initialSize - listaMensagem.Count) + " de " + initialSize + ")";
                if (index == Opcoes.LIMITE_PORT_CHIP && Opcoes.LIMITAR_CHIP)
                {
                    MessageBox.Show("Porta " + portaComm.PortName + " teve seu limite atingido e seus envios interrompidos.",
                           "Limite de envios atingido.",
                           MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Console.WriteLine("Chip limitado na thread: " + Thread.CurrentThread.ManagedThreadId);
                    break;
                }

                Mensagem mensagem = null;
                try
                {
                    if (quantidade_longo < TimerQtde)
                    {
                        if (quantiadade < TempoQtde)
                        {
                            Console.WriteLine(quantiadade+" é < "+TempoQtde);
                            if (!portaComm.IsOpen())
                            {
                                Console.WriteLine("Verificando se porta esta aberta para enviar mensagem. Thread: "+Thread.CurrentThread.ManagedThreadId);
                                try
                                {
                                    portaComm.Open();
                                }
                                catch (Exception e)
                                {
                                    tentativas++;
                                    Console.WriteLine("Não foi possivel abrir a porta. Thread: " + Thread.CurrentThread.ManagedThreadId);
                                    if (e.InnerException != null && e.InnerException is IOException)
                                    {
                                        
                                        Console.WriteLine("Não pode solicitar tentativa " + tentativas + " por erro " + e.ToString());
                                        MessageBox.Show("Porta " + portaComm.PortName + " sendo abortada devido a falha durante o envio." +
                                         "\n Certifique-se de que a mesma esteja funcionando corretamente.",
                                         "Falha durante o envio",
                                         MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        break;
                                    }
                                    else
                                    {
                                        if (portaComm.IsOpen())
                                            portaComm.Close();

                                        Thread.Sleep(4000);
                                    }
                                    continue;
                                }
                            }

                            label1.Text = "Enviando na "+portaComm.PortName;
                            
                            if (!listaMensagem.TryDequeue(out mensagem))
                            {
                                Console.WriteLine("Guardando a próxima");
                                continue;
                            }
                            Console.WriteLine("Enviando mensagem. Thread: " + Thread.CurrentThread.ManagedThreadId+" Instancia da mensagem: "+mensagem.mensagem+" ID: "+mensagem.id_mensagem
                                );
                            SMS.Envia(portaComm, mensagem.mensagem+ " "+ RandomString(3), mensagem.destinatario, mensagem.id_mensagem);
                            tentativas = 0;
                            index++;
                            Console.WriteLine("Mensagem enviada (Restam: "+ (initialSize - listaMensagem.Count) + "/" + initialSize + "). Thread: " + Thread.CurrentThread.ManagedThreadId);
                            
                            Console.WriteLine("Adicionando em mensagens enviadas. Thread: " + Thread.CurrentThread.ManagedThreadId);
                            BD.executaQuery("update dispositivos set enviados = enviados + 1 where porta = '" + portaComm.PortName + "';");
                            Console.WriteLine("Adicionado em mensagens enviadas. Thread: " + Thread.CurrentThread.ManagedThreadId);
                            if (listaMensagem.Count <= 0)
                            {
                                Console.WriteLine("Finalizando thread por envio finalizado");
                                break;
                            }
                            Console.WriteLine("Pausa entre mensagem de "+tempoPausa+" segundos. Thread: " + Thread.CurrentThread.ManagedThreadId);
                            if(tempoPausa > 0)
                                Thread.Sleep(tempoPausa * 1000);
                            quantiadade++;
                        }
                        else
                        {
                            label1.Text = "Parada de " + TempoParada + " segundos na "+ portaComm.PortName;
                            quantiadade = 0;
                            Thread.Sleep(TempoParada * 1000);
                        }
                        quantidade_longo++;
                    }
                    else
                    {
                        label1.Text = "Parada Longa de " + TimerParada + " segundos na "+ portaComm.PortName;
                        quantidade_longo = 0;
                        Thread.Sleep(TimerParada * 1000);
                    }

                    Console.WriteLine("Passaram-se " + (DateTime.Now - inicio).TotalSeconds + " segundos ou " + (DateTime.Now - inicio).TotalMinutes + " minutos desde a última oscilação");
                    if ((DateTime.Now - inicio).TotalMinutes >= TempoOscilador && TempoOscilador > 0)
                    {
                        Console.WriteLine("Passaram-se "+ (DateTime.Now - inicio).TotalMinutes + "minutos desde a última oscilação");
                        if (contador == 0)
                            tempoPausa = tempoPausa + 1;
                        if (contador == 1)
                            tempoPausa = tempoPausa + 2;
                        if (contador == 2)
                            tempoPausa = tempoPausa - 4;
                        if (contador == 3)
                            tempoPausa = tempoPausa - 1;
                        if (contador == 4) { 
                            tempoPausa = tempoPausa + 2;
                            contador = 0;
                        }
                        Console.WriteLine("Agora o tempo de pausa entre mensagem é "+tempoPausa+" segundos");
                        contador = contador + 1;
                        inicio = System.DateTime.Now;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exceção na thread:" + Thread.CurrentThread.ManagedThreadId + ": " + e.ToString());
                    if (e.Message.Contains("The phone reports") || e.Message.Contains("No data received from") || e is MessageServiceErrorException)
                    {

                        tentativas++;
                        Console.WriteLine("Solicitando tentativa " + tentativas);
                        continue;
                    }
                    if (mensagem != null && (e is CommException|| e is InvalidOperationException))
                    {
                        listaMensagem.Enqueue(mensagem);
                    }
                }
            }
            threadsDeEnvio.Remove(Thread.CurrentThread);

            if(threadsDeEnvio.Count == 0)
            {
                Console.WriteLine("Acabaram as threads");
                Desktop.MAIN.buttonParar_Click(null, null);
            }
        }

        private void verificaOpcoes()
        {
            SQLiteDataReader dr = BD.retornaQuery("SELECT Tempo, Oscilador, TempoQtde, TempoParada, TimerQtde, TimerParada, Inicio, Fim, LimitarChip, Limitar FROM Opcoes");
            while (dr.Read())
            {
                tempoPausa = Convert.ToInt32(dr[0].ToString());
                TempoOscilador = Convert.ToInt32(dr[1].ToString());
                //timerOscilador.Interval = 60000 * Convert.ToInt32(dr[1].ToString());
                TempoQtde = Convert.ToInt32(dr[2].ToString());
                TempoParada = Convert.ToInt32(dr[3].ToString());
                TimerQtde = Convert.ToInt32(dr[4].ToString());
                TimerParada = Convert.ToInt32(dr[5].ToString());
                Opcoes.LIMITE_PORT_CHIP = Convert.ToInt32(dr[8].ToString());
                Opcoes.LIMITAR_CHIP = Convert.ToBoolean(Convert.ToInt32(dr[9].ToString()));
            }
            dr.Close();
        }

        private void timerOscilador_Tick(object sender, EventArgs e)
        {
            if (contador == 0)
                tempoPausa = tempoPausa + 1;
            if (contador == 1)
                tempoPausa = tempoPausa + 2;
            if (contador == 2)
                tempoPausa = tempoPausa - 4;
            if (contador == 3)
                tempoPausa = tempoPausa - 1;
            if (contador == 4)
                tempoPausa = tempoPausa + 2;
            if (contador == 5)
                contador = -1;

            contador = contador + 1;
        }

        private void Envio_Mensagens_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                Properties.Settings.Default.Verifica = 1;
                Properties.Settings.Default.Save();
                cancela = 1;

                t.Abort();

                // Encerra todas as threas de envio
                foreach(Thread thread in threadsDeEnvio)
                {
                    thread.Abort();
                }
                threadsDeEnvio.Clear();

                // Fecha as portas que estavam sendo usadas
                foreach (GsmCommMain portaComm in aparelhosMap.Keys)
                {
                    if (portaComm.IsOpen())
                        portaComm.Close();
                }

                // Limpa a lista de aparelhos
                aparelhosMap.Clear();
            }
            catch { }
        }

        public string RandomString(int length)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var stringChars = new char[length];

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            return new String(stringChars);
        }

        private void ThreadFecharPortaSerial()
        {
            try
            {
                t.Abort();
            }
            catch { }
        }

        class Mensagem
        {
            public string id_mensagem { get; set; }
            public string destinatario { get; set; }
            public string mensagem { get; set; }
        }

    }
}
