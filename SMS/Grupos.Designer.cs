﻿namespace SMS
{
    partial class Grupos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Grupos));
            this.label1 = new System.Windows.Forms.Label();
            this.kryptonPanel2 = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.kryptonPanel1 = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.gridContatos = new ComponentFactory.Krypton.Toolkit.KryptonDataGridView();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID_Grupo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Contagem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonCampanha = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonRemover = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonNovo = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonEditar = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.listContatos = new ComponentFactory.Krypton.Toolkit.KryptonListBox();
            this.painelPrincipal = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel2)).BeginInit();
            this.kryptonPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel1)).BeginInit();
            this.kryptonPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridContatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(478, 356);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 12);
            this.label1.TabIndex = 29;
            this.label1.Text = "Campanha";
            // 
            // kryptonPanel2
            // 
            this.kryptonPanel2.Controls.Add(this.kryptonLabel1);
            this.kryptonPanel2.Location = new System.Drawing.Point(0, 204);
            this.kryptonPanel2.Name = "kryptonPanel2";
            this.kryptonPanel2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Blue;
            this.kryptonPanel2.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.HeaderCustom2;
            this.kryptonPanel2.Size = new System.Drawing.Size(605, 28);
            this.kryptonPanel2.TabIndex = 7;
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.Location = new System.Drawing.Point(3, 5);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(189, 20);
            this.kryptonLabel1.TabIndex = 0;
            this.kryptonLabel1.Values.Text = "Membros relacionados ao grupo";
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kryptonHeader1.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Custom2;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(98, 31);
            this.kryptonHeader1.TabIndex = 3;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Grupos";
            this.kryptonHeader1.Values.Image = ((System.Drawing.Image)(resources.GetObject("kryptonHeader1.Values.Image")));
            // 
            // kryptonPanel1
            // 
            this.kryptonPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kryptonPanel1.Controls.Add(this.gridContatos);
            this.kryptonPanel1.Location = new System.Drawing.Point(0, 31);
            this.kryptonPanel1.Name = "kryptonPanel1";
            this.kryptonPanel1.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.GridHeaderColumnList;
            this.kryptonPanel1.Size = new System.Drawing.Size(605, 173);
            this.kryptonPanel1.TabIndex = 6;
            // 
            // gridContatos
            // 
            this.gridContatos.AllowUserToAddRows = false;
            this.gridContatos.AllowUserToDeleteRows = false;
            this.gridContatos.AllowUserToResizeRows = false;
            this.gridContatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridContatos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nome,
            this.ID_Grupo,
            this.Contagem});
            this.gridContatos.GridStyles.Style = ComponentFactory.Krypton.Toolkit.DataGridViewStyle.Mixed;
            this.gridContatos.GridStyles.StyleBackground = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.GridHeaderColumnCustom1;
            this.gridContatos.Location = new System.Drawing.Point(0, 0);
            this.gridContatos.MultiSelect = false;
            this.gridContatos.Name = "gridContatos";
            this.gridContatos.RowHeadersVisible = false;
            this.gridContatos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridContatos.Size = new System.Drawing.Size(605, 173);
            this.gridContatos.TabIndex = 0;
            this.gridContatos.SelectionChanged += new System.EventHandler(this.gridContatos_SelectionChanged);
            // 
            // Nome
            // 
            this.Nome.DataPropertyName = "Nome";
            this.Nome.HeaderText = "Nome do Grupo";
            this.Nome.Name = "Nome";
            this.Nome.ReadOnly = true;
            this.Nome.Width = 410;
            // 
            // ID_Grupo
            // 
            this.ID_Grupo.DataPropertyName = "ID_Grupo";
            this.ID_Grupo.HeaderText = "ID_Grupo";
            this.ID_Grupo.Name = "ID_Grupo";
            this.ID_Grupo.ReadOnly = true;
            this.ID_Grupo.Visible = false;
            // 
            // Contagem
            // 
            this.Contagem.DataPropertyName = "Contagem";
            this.Contagem.HeaderText = "Nº de Contatos";
            this.Contagem.Name = "Contagem";
            this.Contagem.Width = 150;
            // 
            // buttonCampanha
            // 
            this.buttonCampanha.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonCampanha.Location = new System.Drawing.Point(474, 313);
            this.buttonCampanha.Name = "buttonCampanha";
            this.buttonCampanha.Size = new System.Drawing.Size(57, 58);
            this.buttonCampanha.TabIndex = 28;
            this.buttonCampanha.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonCampanha.Values.Image")));
            this.buttonCampanha.Values.Text = "";
            this.buttonCampanha.Click += new System.EventHandler(this.buttonCampanha_Click);
            // 
            // buttonRemover
            // 
            this.buttonRemover.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonRemover.Location = new System.Drawing.Point(537, 238);
            this.buttonRemover.Name = "buttonRemover";
            this.buttonRemover.Size = new System.Drawing.Size(57, 58);
            this.buttonRemover.TabIndex = 27;
            this.buttonRemover.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonRemover.Values.Image")));
            this.buttonRemover.Values.Text = "";
            this.buttonRemover.Click += new System.EventHandler(this.buttonRemover_Click);
            // 
            // buttonNovo
            // 
            this.buttonNovo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonNovo.Location = new System.Drawing.Point(474, 238);
            this.buttonNovo.Name = "buttonNovo";
            this.buttonNovo.Size = new System.Drawing.Size(57, 58);
            this.buttonNovo.TabIndex = 26;
            this.buttonNovo.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonNovo.Values.Image")));
            this.buttonNovo.Values.Text = "";
            this.buttonNovo.Click += new System.EventHandler(this.buttonNovo_Click);
            // 
            // buttonEditar
            // 
            this.buttonEditar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonEditar.Location = new System.Drawing.Point(537, 313);
            this.buttonEditar.Name = "buttonEditar";
            this.buttonEditar.Size = new System.Drawing.Size(57, 58);
            this.buttonEditar.TabIndex = 24;
            this.buttonEditar.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonEditar.Values.Image")));
            this.buttonEditar.Values.Text = "";
            this.buttonEditar.Click += new System.EventHandler(this.buttonEditar_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(8, 240);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 128);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // listContatos
            // 
            this.listContatos.Location = new System.Drawing.Point(140, 238);
            this.listContatos.Name = "listContatos";
            this.listContatos.Size = new System.Drawing.Size(328, 133);
            this.listContatos.TabIndex = 8;
            // 
            // painelPrincipal
            // 
            this.painelPrincipal.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.painelPrincipal.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.painelPrincipal.Location = new System.Drawing.Point(0, 0);
            this.painelPrincipal.Name = "painelPrincipal";
            this.painelPrincipal.Size = new System.Drawing.Size(605, 383);
            this.painelPrincipal.TabIndex = 30;
            this.painelPrincipal.Visible = false;
            // 
            // Grupos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(605, 383);
            this.Controls.Add(this.painelPrincipal);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonCampanha);
            this.Controls.Add(this.buttonRemover);
            this.Controls.Add(this.buttonNovo);
            this.Controls.Add(this.buttonEditar);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.listContatos);
            this.Controls.Add(this.kryptonPanel2);
            this.Controls.Add(this.kryptonPanel1);
            this.Controls.Add(this.kryptonHeader1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Grupos";
            this.Text = "1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Grupos_Load);
            this.Click += new System.EventHandler(this.Grupos_Click);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel2)).EndInit();
            this.kryptonPanel2.ResumeLayout(false);
            this.kryptonPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel1)).EndInit();
            this.kryptonPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridContatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel kryptonPanel2;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel1;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel kryptonPanel1;
        private ComponentFactory.Krypton.Toolkit.KryptonDataGridView gridContatos;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_Grupo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Contagem;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonCampanha;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonRemover;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonNovo;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonEditar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private ComponentFactory.Krypton.Toolkit.KryptonListBox listContatos;
        public System.Windows.Forms.Panel painelPrincipal;



    }
}