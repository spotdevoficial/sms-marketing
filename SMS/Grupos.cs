﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace SMS
{
    public partial class Grupos : Form
    {
        BindingSource bs = null;

        public Grupos()
        {
            InitializeComponent();
        }

        public void Grupos_Load(object sender, EventArgs e)
        {
            atualiza();
        }

        private void buttonNovo_Click(object sender, EventArgs e)
        {
            Adicionar_Grupo addGrupo = new Adicionar_Grupo();
            addGrupo.Show();
        }

        private void buttonEditar_Click(object sender, EventArgs e)
        {

            if (gridContatos.CurrentRow == null)
            {
                return;
            }

            int idGrupo = Convert.ToInt32(gridContatos.CurrentRow.Cells["ID_Grupo"].Value.ToString());
            Editar_Grupo editGrupo = new Editar_Grupo(idGrupo);
            editGrupo.Show();
        }

        private void Grupos_Click(object sender, EventArgs e)
        {
            Grupos_Load(sender, e);
        }

        public void atualiza()
        {
            //CRIA UM DATASET COM O RETORNO DA QUERY
            DataSet ds = BD.retornaQueryDataSet("SELECT DISTINCT ID_GRUPO, NOME, CONTAGEM FROM vw_Grupos");
            //CRIA UM BINDINGSOURCE
            bs = new BindingSource();
            //ATRIBUI O DATASET AO DATASOURCE DO BINDINGSOURCE
            bs.DataSource = ds;
            //ATRIBUI O BINDINGSOURCE AO BINDINGNAVIGATOR
            bs.DataMember = ds.Tables[0].TableName;
            //ATRIBUI O BINDINGSOURCE AO DATAGRIDVIEW
            gridContatos.DataSource = bs;

            if (gridContatos.RowCount > 0)
                buttonCampanha.Enabled = true;
            else
                buttonCampanha.Enabled = false;
        }

        private void buttonRemover_Click(object sender, EventArgs e)
        {

            if (gridContatos.CurrentRow == null)
            {
                return;
            }

            int idGrupo = Convert.ToInt32(gridContatos.CurrentRow.Cells["ID_Grupo"].Value.ToString());
            BD.retornaQuery("UPDATE Contatos SET ID_Grupo = -1 WHERE ID_Grupo = '" + idGrupo + "' ");
            BD.retornaQuery("DELETE FROM Grupos WHERE ID_Grupo = '" + idGrupo + "' ");
            Grupos_Load(sender, e);
        }

        private void gridContatos_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                listContatos.Items.Clear();
                int idGrupo = Convert.ToInt32(gridContatos.CurrentRow.Cells["ID_Grupo"].Value.ToString());
                SQLiteDataReader dr = BD.retornaQuery("SELECT Nome, Telefone FROM Contatos WHERE ID_Grupo = '" + idGrupo + "' LIMIT 50");
                while (dr.Read())
                    listContatos.Items.Add((dr[0].ToString() + " <" + dr[1].ToString() + ">"));
                dr.Close();
            }
            catch { }
        }

        private void buttonCampanha_Click(object sender, EventArgs e)
        {
            
            int idGrupo = Convert.ToInt32(gridContatos.CurrentRow.Cells["ID_Grupo"].Value.ToString());
            
            painelPrincipal.Visible = true;
            Campanha campanha = new Campanha(idGrupo);
            campanha.TopLevel = false;
            painelPrincipal.Controls.Add(campanha);
            campanha.FormBorderStyle = FormBorderStyle.None;
            campanha.Show();
        }

        private void Grupos_MaximizedBoundsChanged(object sender, EventArgs e)
        {
            this.Refresh();
        }

        
    }
}
