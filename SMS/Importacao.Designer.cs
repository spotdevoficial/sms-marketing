﻿namespace SMS
{
    partial class Importacao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Importacao));
            this.listGrupo = new ComponentFactory.Krypton.Toolkit.KryptonListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.kryptonLabel2 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.textArquivo = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonButton1 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonImportar = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkAdicionarGrupo = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.kryptonButton2 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.checkNExcluir = new ComponentFactory.Krypton.Toolkit.KryptonRadioButton();
            this.checkExcluir = new ComponentFactory.Krypton.Toolkit.KryptonRadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listGrupo
            // 
            this.listGrupo.Location = new System.Drawing.Point(15, 220);
            this.listGrupo.Name = "listGrupo";
            this.listGrupo.Size = new System.Drawing.Size(177, 167);
            this.listGrupo.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe WP", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(42, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(349, 28);
            this.label1.TabIndex = 1;
            this.label1.Text = "IMPORTAÇÃO DE CONTATOS EM  LOTE";
            // 
            // kryptonLabel2
            // 
            this.kryptonLabel2.Location = new System.Drawing.Point(12, 71);
            this.kryptonLabel2.Name = "kryptonLabel2";
            this.kryptonLabel2.Size = new System.Drawing.Size(102, 20);
            this.kryptonLabel2.TabIndex = 3;
            this.kryptonLabel2.Values.Text = "Carregar arquivo";
            // 
            // textArquivo
            // 
            this.textArquivo.Location = new System.Drawing.Point(12, 90);
            this.textArquivo.Name = "textArquivo";
            this.textArquivo.Size = new System.Drawing.Size(326, 20);
            this.textArquivo.TabIndex = 4;
            // 
            // kryptonButton1
            // 
            this.kryptonButton1.Location = new System.Drawing.Point(326, 87);
            this.kryptonButton1.Name = "kryptonButton1";
            this.kryptonButton1.Size = new System.Drawing.Size(107, 25);
            this.kryptonButton1.TabIndex = 5;
            this.kryptonButton1.Values.Text = "Carregar Arquivo";
            this.kryptonButton1.Click += new System.EventHandler(this.kryptonButton1_Click);
            // 
            // buttonImportar
            // 
            this.buttonImportar.Location = new System.Drawing.Point(263, 220);
            this.buttonImportar.Name = "buttonImportar";
            this.buttonImportar.Size = new System.Drawing.Size(128, 25);
            this.buttonImportar.TabIndex = 6;
            this.buttonImportar.Values.Text = "Importar Contatos";
            this.buttonImportar.Click += new System.EventHandler(this.buttonImportar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkExcluir);
            this.groupBox1.Controls.Add(this.checkNExcluir);
            this.groupBox1.Location = new System.Drawing.Point(12, 116);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(421, 72);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Opções de Importação";
            // 
            // checkAdicionarGrupo
            // 
            this.checkAdicionarGrupo.Location = new System.Drawing.Point(19, 200);
            this.checkAdicionarGrupo.Name = "checkAdicionarGrupo";
            this.checkAdicionarGrupo.Size = new System.Drawing.Size(179, 20);
            this.checkAdicionarGrupo.TabIndex = 9;
            this.checkAdicionarGrupo.Values.Text = "Adicionar contatos ao grupo";
            this.checkAdicionarGrupo.CheckedChanged += new System.EventHandler(this.checkAdicionarGrupo_CheckedChanged);
            // 
            // kryptonButton2
            // 
            this.kryptonButton2.Location = new System.Drawing.Point(263, 251);
            this.kryptonButton2.Name = "kryptonButton2";
            this.kryptonButton2.Size = new System.Drawing.Size(128, 25);
            this.kryptonButton2.TabIndex = 10;
            this.kryptonButton2.Values.Text = "Exemplo de  Arquivo";
            this.kryptonButton2.Click += new System.EventHandler(this.kryptonButton2_Click_1);
            // 
            // checkNExcluir
            // 
            this.checkNExcluir.Checked = true;
            this.checkNExcluir.Location = new System.Drawing.Point(25, 19);
            this.checkNExcluir.Name = "checkNExcluir";
            this.checkNExcluir.Size = new System.Drawing.Size(300, 20);
            this.checkNExcluir.TabIndex = 11;
            this.checkNExcluir.Values.Text = "Importar arquivo e não excluir contatos duplicados";
            // 
            // checkExcluir
            // 
            this.checkExcluir.Location = new System.Drawing.Point(25, 45);
            this.checkExcluir.Name = "checkExcluir";
            this.checkExcluir.Size = new System.Drawing.Size(276, 20);
            this.checkExcluir.TabIndex = 12;
            this.checkExcluir.Values.Text = "Importar arquivo e excluir contatos duplicados";
            // 
            // Importacao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(445, 399);
            this.Controls.Add(this.kryptonButton2);
            this.Controls.Add(this.checkAdicionarGrupo);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonImportar);
            this.Controls.Add(this.kryptonButton1);
            this.Controls.Add(this.textArquivo);
            this.Controls.Add(this.kryptonLabel2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listGrupo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Importacao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Importação de contatos";
            this.Load += new System.EventHandler(this.Importacao_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonListBox listGrupo;
        private System.Windows.Forms.Label label1;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel2;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textArquivo;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonImportar;
        private System.Windows.Forms.GroupBox groupBox1;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox checkAdicionarGrupo;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton2;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private ComponentFactory.Krypton.Toolkit.KryptonRadioButton checkExcluir;
        private ComponentFactory.Krypton.Toolkit.KryptonRadioButton checkNExcluir;
    }
}