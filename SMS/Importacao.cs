﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Microsoft.VisualBasic.FileIO;

namespace SMS
{
    public partial class Importacao : Form
    {
        BindingSource bs = new BindingSource();
        List<Contato> listaDados = new List<Contato>();

        public Importacao()
        {
            InitializeComponent();

       

        }

        private void Importacao_Load(object sender, EventArgs e)
        {
            desabilitaCampos();
            carregaGrupo();

            if (listGrupo.Items.Count <= 0)
                checkAdicionarGrupo.Enabled = false;
        }

        private void desabilitaCampos()
        {
            listGrupo.Enabled = false;
        }

        private void carregaGrupo()
        {
            try
            {
                ArrayList grupo = new ArrayList();
                SQLiteDataReader dr = BD.retornaQuery("SELECT ID_Grupo, Nome FROM Grupos");
                while (dr.Read())
                {
                    grupo.Add(new Codigo_Descricao(dr[1].ToString(), Convert.ToInt32(dr[0].ToString())));
                }
                dr.Close();
                // atribuimos o ArrayList como DataSource do ComboBox
                //linguagensCombo.DataSource = linguagens;
                listGrupo.DataSource = grupo;
                // o texto do item será o nome da linguagem
                listGrupo.DisplayMember = "Descricao";
                // o valor do item será o código da linguagem
                listGrupo.ValueMember = "Codigo";
            }
            catch {
                checkAdicionarGrupo.Enabled = false;
            }
            
        }

        private void kryptonButton1_Click(object sender, EventArgs e)
        {
            try
            {
                listaDados.Clear();
                this.Cursor = Cursors.WaitCursor;
                OpenFileDialog file = new OpenFileDialog();
                file.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                if (file.ShowDialog() == DialogResult.OK)
                {
                    StreamReader stream = new StreamReader(@file.FileName);
                    textArquivo.Text = file.FileName;

                    string linha = null;
                    while ((linha = stream.ReadLine()) != null)
                    {
                        string[] coluna = linha.Split(';');
                        var dados = new Contato
                        {
                            nome = (coluna.Length>1)? coluna[0]:"",
                            telefone = (coluna.Length > 1) ? coluna[1] : linha,
                            variavel1 = (coluna.Length > 1) ? coluna[2] : "",
                            variavel2 = (coluna.Length > 1) ? coluna[3] : "",
                            grupo = "-1"
                        };
                        listaDados.Add(dados);
                    }
                    stream.Close();
                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception erro)
            {
                textArquivo.Text = string.Empty;
                MessageBox.Show("Erro ao fazer a leitura do arquivo! Verifique se o mesmo esta de acordo com o exemplo", "Erro de leitura", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Cursor = Cursors.Default;
            }
        }

        private void checkAdicionarGrupo_CheckedChanged(object sender, EventArgs e)
        {
            if (checkAdicionarGrupo.Checked == true)
                listGrupo.Enabled = true;
            else
                listGrupo.Enabled = false;
        }

        private void buttonImportar_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            int id_grupo = -1;
            if(checkAdicionarGrupo.Checked == true)
            {
                if(listGrupo.SelectedValue != null)
                    id_grupo = ((Codigo_Descricao)listGrupo.SelectedItem).Codigo;

            }

            if (checkNExcluir.Checked == true)
            {
                if (id_grupo > -1)
                {

                    BD.SqlCommandPrepareEx(listaDados, id_grupo);
                    //foreach (var item in listaDados)
                   // {
                     //   BD.executaQuery("INSERT INTO Contatos(Nome, Telefone, ID_Grupo, Variavel1, Variavel2) " +
                      //          "VALUES ('" + item.nome + "', '" + item.telefone + "', '" + listGrupo.SelectedValue + "', '" + item.variavel1 + "', '" + item.variavel2 + "')");
                    //}
                }
                else
                {
                    BD.SqlCommandPrepareEx(listaDados);

                    //foreach (var item in listaDados)
                   // {
                    //    BD.executaQuery("INSERT INTO Contatos(Nome, Telefone, ID_Grupo, Variavel1, Variavel2) " +
                    //            "VALUES ('" + item.nome + "', '" + item.telefone + "', '" + item.grupo + "', '" + item.variavel1 + "', '" + item.variavel2 + "')");
                    //}
                }
                MessageBox.Show("Quantidade de contatos importados: " + listaDados.Count().ToString());
                this.Close();
            }
            else
            {
                List<Contato> resultado = listaDados.GroupBy(d => new { Telefone = d.telefone })
                                  .Select(d => d.First())
                                  .ToList();

                if (id_grupo > -1)
                {
                    foreach (var item in resultado)
                    {
                        BD.executaQuery("INSERT INTO Contatos(Nome, Telefone, ID_Grupo, Variavel1, Variavel2) " +
                                "VALUES ('" + item.nome + "', '" + item.telefone + "', '" + id_grupo + "', '" + item.variavel1 + "', '" + item.variavel2 + "')");
                    }
                }
                else
                {
                    foreach (var item in resultado)
                    {
                        BD.executaQuery("INSERT INTO Contatos(Nome, Telefone, ID_Grupo, Variavel1, Variavel2) " +
                                "VALUES ('" + item.nome + "', '" + item.telefone + "', '" + item.grupo + "', '" + item.variavel1 + "', '" + item.variavel2 + "')");
                    }
                }

                MessageBox.Show("Quantidade de contatos importados: " + (listaDados.Count() -  (listaDados.Count() - resultado.Count())) + "\nQuantidade de contatos removidos (duplicados): " + (listaDados.Count() - resultado.Count()) );
                this.Close();
            }
            this.Cursor = Cursors.Default;  
        }


        private void kryptonButton2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(listaDados[1].telefone);
        }

        private void kryptonButton2_Click_1(object sender, EventArgs e)
        {
                var desktop = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop);
                string fName = @"./Arquivo_Exemplo.txt";

                try
                {
                    // Will not overwrite if the destination file already exists.
                    File.Copy(fName, Path.Combine(desktop, "Arquivo_Exemplo.txt"), true);
                    MessageBox.Show("Arquivo baixado na área de trabalho");
                }

                // Catch exception if the file was already copied.
                catch (IOException copyError)
                {
                    MessageBox.Show(copyError.Message);
                }



            /*string sourcePath = @"./Arquivo_Exemplo.txt";
            string[] coluna = null;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Texto|*.txt";
            saveFileDialog1.Title = "Salvar arquivo de exemplo";
            saveFileDialog1.ShowDialog();

            if (saveFileDialog1.FileName != "")
            {
                //System.IO.FileStream fs = (System.IO.FileStream)saveFileDialog1.OpenFile();

                StreamReader stream = new StreamReader(sourcePath);
                string linha = null;
                string texto = stream.ReadToEnd();

                System.IO.File.WriteAllText(saveFileDialog1.FileName, texto);


                //fs.Close();
            }*/
        }
    }
}
