﻿using System;
using System.Data;
using System.Data.Odbc;
using System.Data.SQLite;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace SMS
{
    class Importar_CSV
    {
        public static void leArquivo(string caminho)
        {
            try
            {
                //Declaro o StreamReader para o caminho onde se encontra o arquivo 
                StreamReader rd = new StreamReader(@caminho);
                //Declaro uma string que será utilizada para receber a linha completa do arquivo 
                string linha = null;
                //Declaro um array do tipo string que será utilizado para adicionar o conteudo da linha separado 
                string[] linhaseparada = null;
                //realizo o while para ler o conteudo da linha 
                while ((linha = rd.ReadLine()) != null)
                {
                    //com o split adiciono a string 'quebrada' dentro do array 
                    linhaseparada = linha.Split(';');
                    MessageBox.Show(linhaseparada.ToString());
                    //aqui incluo o método necessário para continuar o trabalho 

                }
                rd.Close();
            }
            catch
            {
                Console.WriteLine("Erro ao executar Leitura do Arquivo");
            } 
        }
    }
}
