﻿namespace SMS
{
    partial class Licenca
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Licenca));
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.textCodigo = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.textHardwareID = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel2 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureValido = new System.Windows.Forms.PictureBox();
            this.pictureInvalido = new System.Windows.Forms.PictureBox();
            this.labelValidacao = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.gridEnviados = new ComponentFactory.Krypton.Toolkit.KryptonDataGridView();
            this.Telefone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mensagem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Data_Enviada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonSalvar = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.buttonCapturar = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.pictureImagem = new System.Windows.Forms.PictureBox();
            this.textTelefone = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel6 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.textEndereco = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel5 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.textCNPJ = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel4 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.textEmpresa = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel3 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonButton2 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonButton1 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureValido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureInvalido)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEnviados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureImagem)).BeginInit();
            this.SuspendLayout();
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.Location = new System.Drawing.Point(6, 57);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(50, 20);
            this.kryptonLabel1.TabIndex = 0;
            this.kryptonLabel1.Values.Text = "Licença";
            // 
            // textCodigo
            // 
            this.textCodigo.Location = new System.Drawing.Point(6, 74);
            this.textCodigo.Name = "textCodigo";
            this.textCodigo.Size = new System.Drawing.Size(354, 20);
            this.textCodigo.TabIndex = 1;
            // 
            // textHardwareID
            // 
            this.textHardwareID.Location = new System.Drawing.Point(6, 36);
            this.textHardwareID.Name = "textHardwareID";
            this.textHardwareID.Size = new System.Drawing.Size(354, 20);
            this.textHardwareID.TabIndex = 4;
            // 
            // kryptonLabel2
            // 
            this.kryptonLabel2.Location = new System.Drawing.Point(6, 19);
            this.kryptonLabel2.Name = "kryptonLabel2";
            this.kryptonLabel2.Size = new System.Drawing.Size(86, 20);
            this.kryptonLabel2.TabIndex = 3;
            this.kryptonLabel2.Values.Text = "Hardware Key";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureValido);
            this.groupBox1.Controls.Add(this.pictureInvalido);
            this.groupBox1.Controls.Add(this.labelValidacao);
            this.groupBox1.Controls.Add(this.textCodigo);
            this.groupBox1.Controls.Add(this.kryptonButton1);
            this.groupBox1.Controls.Add(this.textHardwareID);
            this.groupBox1.Controls.Add(this.kryptonLabel2);
            this.groupBox1.Controls.Add(this.kryptonLabel1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(567, 109);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Licenciamento do Sistema";
            // 
            // pictureValido
            // 
            this.pictureValido.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureValido.Image = ((System.Drawing.Image)(resources.GetObject("pictureValido.Image")));
            this.pictureValido.Location = new System.Drawing.Point(523, 62);
            this.pictureValido.Name = "pictureValido";
            this.pictureValido.Size = new System.Drawing.Size(37, 37);
            this.pictureValido.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureValido.TabIndex = 7;
            this.pictureValido.TabStop = false;
            this.pictureValido.Click += new System.EventHandler(this.pictureValido_Click);
            // 
            // pictureInvalido
            // 
            this.pictureInvalido.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureInvalido.Image = ((System.Drawing.Image)(resources.GetObject("pictureInvalido.Image")));
            this.pictureInvalido.Location = new System.Drawing.Point(524, 62);
            this.pictureInvalido.Name = "pictureInvalido";
            this.pictureInvalido.Size = new System.Drawing.Size(37, 37);
            this.pictureInvalido.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureInvalido.TabIndex = 6;
            this.pictureInvalido.TabStop = false;
            // 
            // labelValidacao
            // 
            this.labelValidacao.AutoSize = true;
            this.labelValidacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelValidacao.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelValidacao.Location = new System.Drawing.Point(377, 69);
            this.labelValidacao.Name = "labelValidacao";
            this.labelValidacao.Size = new System.Drawing.Size(58, 22);
            this.labelValidacao.TabIndex = 5;
            this.labelValidacao.Text = "label1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.gridEnviados);
            this.groupBox2.Controls.Add(this.kryptonButton2);
            this.groupBox2.Controls.Add(this.buttonSalvar);
            this.groupBox2.Controls.Add(this.buttonCapturar);
            this.groupBox2.Controls.Add(this.pictureImagem);
            this.groupBox2.Controls.Add(this.textTelefone);
            this.groupBox2.Controls.Add(this.kryptonLabel6);
            this.groupBox2.Controls.Add(this.textEndereco);
            this.groupBox2.Controls.Add(this.kryptonLabel5);
            this.groupBox2.Controls.Add(this.textCNPJ);
            this.groupBox2.Controls.Add(this.kryptonLabel4);
            this.groupBox2.Controls.Add(this.textEmpresa);
            this.groupBox2.Controls.Add(this.kryptonLabel3);
            this.groupBox2.Location = new System.Drawing.Point(12, 127);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(567, 251);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dados do Sistema";
            // 
            // gridEnviados
            // 
            this.gridEnviados.AllowUserToAddRows = false;
            this.gridEnviados.AllowUserToDeleteRows = false;
            this.gridEnviados.AllowUserToResizeRows = false;
            this.gridEnviados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridEnviados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Telefone,
            this.Mensagem,
            this.Data_Enviada,
            this.Status});
            this.gridEnviados.GridStyles.Style = ComponentFactory.Krypton.Toolkit.DataGridViewStyle.Mixed;
            this.gridEnviados.GridStyles.StyleBackground = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.GridHeaderColumnCustom1;
            this.gridEnviados.Location = new System.Drawing.Point(240, 219);
            this.gridEnviados.MultiSelect = false;
            this.gridEnviados.Name = "gridEnviados";
            this.gridEnviados.RowHeadersVisible = false;
            this.gridEnviados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridEnviados.Size = new System.Drawing.Size(136, 26);
            this.gridEnviados.TabIndex = 152;
            this.gridEnviados.Visible = false;
            // 
            // Telefone
            // 
            this.Telefone.DataPropertyName = "Destinatario";
            this.Telefone.HeaderText = "Telefone";
            this.Telefone.Name = "Telefone";
            this.Telefone.ReadOnly = true;
            this.Telefone.Width = 135;
            // 
            // Mensagem
            // 
            this.Mensagem.DataPropertyName = "Mensagem";
            this.Mensagem.HeaderText = "Mensagem";
            this.Mensagem.Name = "Mensagem";
            this.Mensagem.ReadOnly = true;
            this.Mensagem.Width = 150;
            // 
            // Data_Enviada
            // 
            this.Data_Enviada.DataPropertyName = "Data";
            this.Data_Enviada.HeaderText = "Data de Envio";
            this.Data_Enviada.Name = "Data_Enviada";
            this.Data_Enviada.ReadOnly = true;
            this.Data_Enviada.Width = 150;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.Width = 150;
            // 
            // buttonSalvar
            // 
            this.buttonSalvar.Location = new System.Drawing.Point(516, 217);
            this.buttonSalvar.Name = "buttonSalvar";
            this.buttonSalvar.Size = new System.Drawing.Size(45, 34);
            this.buttonSalvar.TabIndex = 150;
            this.buttonSalvar.Values.Image = ((System.Drawing.Image)(resources.GetObject("buttonSalvar.Values.Image")));
            this.buttonSalvar.Values.Text = "";
            this.buttonSalvar.Click += new System.EventHandler(this.buttonSalvar_Click);
            // 
            // buttonCapturar
            // 
            this.buttonCapturar.Location = new System.Drawing.Point(340, 185);
            this.buttonCapturar.Name = "buttonCapturar";
            this.buttonCapturar.Size = new System.Drawing.Size(221, 25);
            this.buttonCapturar.TabIndex = 149;
            this.buttonCapturar.TabStop = false;
            this.buttonCapturar.Values.Text = "Selecionar logo";
            this.buttonCapturar.Click += new System.EventHandler(this.buttonCapturar_Click);
            // 
            // pictureImagem
            // 
            this.pictureImagem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureImagem.Image = ((System.Drawing.Image)(resources.GetObject("pictureImagem.Image")));
            this.pictureImagem.Location = new System.Drawing.Point(340, 18);
            this.pictureImagem.Name = "pictureImagem";
            this.pictureImagem.Size = new System.Drawing.Size(221, 161);
            this.pictureImagem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureImagem.TabIndex = 148;
            this.pictureImagem.TabStop = false;
            // 
            // textTelefone
            // 
            this.textTelefone.Location = new System.Drawing.Point(6, 159);
            this.textTelefone.Name = "textTelefone";
            this.textTelefone.Size = new System.Drawing.Size(328, 20);
            this.textTelefone.TabIndex = 13;
            // 
            // kryptonLabel6
            // 
            this.kryptonLabel6.Location = new System.Drawing.Point(6, 141);
            this.kryptonLabel6.Name = "kryptonLabel6";
            this.kryptonLabel6.Size = new System.Drawing.Size(57, 20);
            this.kryptonLabel6.TabIndex = 14;
            this.kryptonLabel6.Values.Text = "Telefone";
            // 
            // textEndereco
            // 
            this.textEndereco.Location = new System.Drawing.Point(6, 120);
            this.textEndereco.Name = "textEndereco";
            this.textEndereco.Size = new System.Drawing.Size(328, 20);
            this.textEndereco.TabIndex = 11;
            // 
            // kryptonLabel5
            // 
            this.kryptonLabel5.Location = new System.Drawing.Point(6, 102);
            this.kryptonLabel5.Name = "kryptonLabel5";
            this.kryptonLabel5.Size = new System.Drawing.Size(61, 20);
            this.kryptonLabel5.TabIndex = 12;
            this.kryptonLabel5.Values.Text = "Endereço";
            // 
            // textCNPJ
            // 
            this.textCNPJ.Location = new System.Drawing.Point(6, 77);
            this.textCNPJ.Name = "textCNPJ";
            this.textCNPJ.Size = new System.Drawing.Size(328, 20);
            this.textCNPJ.TabIndex = 9;
            // 
            // kryptonLabel4
            // 
            this.kryptonLabel4.Location = new System.Drawing.Point(6, 59);
            this.kryptonLabel4.Name = "kryptonLabel4";
            this.kryptonLabel4.Size = new System.Drawing.Size(39, 20);
            this.kryptonLabel4.TabIndex = 10;
            this.kryptonLabel4.Values.Text = "CNPJ";
            // 
            // textEmpresa
            // 
            this.textEmpresa.Location = new System.Drawing.Point(6, 37);
            this.textEmpresa.Name = "textEmpresa";
            this.textEmpresa.Size = new System.Drawing.Size(328, 20);
            this.textEmpresa.TabIndex = 8;
            // 
            // kryptonLabel3
            // 
            this.kryptonLabel3.Location = new System.Drawing.Point(6, 19);
            this.kryptonLabel3.Name = "kryptonLabel3";
            this.kryptonLabel3.Size = new System.Drawing.Size(111, 20);
            this.kryptonLabel3.TabIndex = 8;
            this.kryptonLabel3.Values.Text = "Nome da empresa";
            // 
            // kryptonButton2
            // 
            this.kryptonButton2.Location = new System.Drawing.Point(6, 219);
            this.kryptonButton2.Name = "kryptonButton2";
            this.kryptonButton2.Size = new System.Drawing.Size(144, 25);
            this.kryptonButton2.TabIndex = 151;
            this.kryptonButton2.Values.Text = "Exportar Relatório";
            this.kryptonButton2.Click += new System.EventHandler(this.kryptonButton2_Click);
            // 
            // kryptonButton1
            // 
            this.kryptonButton1.Location = new System.Drawing.Point(374, 31);
            this.kryptonButton1.Name = "kryptonButton1";
            this.kryptonButton1.Size = new System.Drawing.Size(187, 25);
            this.kryptonButton1.TabIndex = 2;
            this.kryptonButton1.Values.Text = "Validar Licença";
            this.kryptonButton1.Click += new System.EventHandler(this.kryptonButton1_Click);
            // 
            // Licenca
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(591, 390);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Licenca";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Licenca";
            this.Load += new System.EventHandler(this.Licenca_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureValido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureInvalido)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEnviados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureImagem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel1;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textCodigo;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textHardwareID;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelValidacao;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureValido;
        private System.Windows.Forms.PictureBox pictureInvalido;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textTelefone;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel6;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textEndereco;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel5;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textCNPJ;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel4;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textEmpresa;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel3;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonCapturar;
        private System.Windows.Forms.PictureBox pictureImagem;
        private ComponentFactory.Krypton.Toolkit.KryptonButton buttonSalvar;
        private ComponentFactory.Krypton.Toolkit.KryptonDataGridView gridEnviados;
        private System.Windows.Forms.DataGridViewTextBoxColumn Telefone;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mensagem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data_Enviada;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton2;
    }
}