﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;
using System.Globalization;
using System.Management;
using System.Threading;
using System.Net.Sockets;
using System.Reflection;
using iTextSharp.text.pdf;
using iTextSharp.text;



namespace SMS
{
    public partial class Licenca : Form
    {
        private Thread thread;
        Bitmap imagemInicial = null;
        string caminhoImagem = String.Empty;
        BindingSource bs = new BindingSource();

        public Licenca()
        {
            InitializeComponent();
        }

        private void Licenca_Load(object sender, EventArgs e)
        {
            desabilitaImagens();
            preencheDados();
            preencheHardware();
            verifica();
            carregaImagem();
            atulizaLista();
        }

        private void desabilitaImagens()
        {
            pictureInvalido.Visible = false;
            pictureValido.Visible = false;
        }

        private void preencheDados()
        {
            SQLiteDataReader dr = BD.retornaQuery("SELECT Licenca, HardwareKey, Nome_Empresa, CNPJ, "+
                "Endereco, Telefone, Imagem FROM Configuracoes_Sistema");
            while (dr.Read())
            {
                textCodigo.Text = dr[0].ToString();
                textHardwareID.Text = dr[1].ToString();
                textEmpresa.Text = dr[2].ToString();
                textCNPJ.Text = dr[3].ToString();
                textEndereco.Text = dr[4].ToString();
                textTelefone.Text = dr[5].ToString();
                caminhoImagem = dr[6].ToString();
            } dr.Close();
        }

        private void preencheHardware()
        {
            int verificador = 0;
            SQLiteDataReader dr = BD.retornaQuery("SELECT HardwareKey FROM Configuracoes_Sistema");
            while (dr.Read())
                if (dr[0].ToString() == null)
                    verificador = 1;
            dr.Close();
                    
            if(verificador==1)
                BD.executaQuery("UPDATE Configuracoes_Sistema SET HardwareKey = '" + HardwareKey.Value() + "'");
        }

        private void verifica()
        {
            if (Criptografia.Decrypt(textCodigo.Text, textHardwareID.Text))
            {
                labelValidacao.Text = "Licença Válida";
                desabilitaImagens();
                pictureValido.Visible = true;
            }
            else
            {
                labelValidacao.Text = "Licença Inválida";
                desabilitaImagens();
                pictureInvalido.Visible = true;
            }
                

            //kryptonTextBox1.Text = Criptografia.Encrypt(textHardwareID.Text);
            //kryptonTextBox2.Text = Criptografia.Decrypt(kryptonTextBox1.Text);
        }

        private void kryptonButton1_Click(object sender, EventArgs e)
        {
            if (Criptografia.Decrypt(textCodigo.Text, textHardwareID.Text))
            {
                BD.executaQuery("UPDATE Configuracoes_Sistema SET Licenca = '" + textCodigo.Text + "'");
                labelValidacao.Text = "Licença Válida";
                desabilitaImagens();
                pictureValido.Visible = true;
                MessageBox.Show("Parabéns! Sua licença é válida!", "Licença", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                labelValidacao.Text = "Licença Inválida";
                desabilitaImagens();
                pictureInvalido.Visible = true;
                MessageBox.Show("Lamento, sua licença é inválida", "Licença", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void buttonSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                BD.executaQuery("UPDATE Configuracoes_Sistema SET Nome_Empresa = '" + textEmpresa.Text + "', " +
                "CNPJ = '" + textCNPJ.Text + "', Endereco = '" + textEndereco.Text + "', Telefone = '" + textTelefone.Text + "'");

                MessageBox.Show("Salvo com sucesso", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception erro)
            {
                MessageBox.Show(erro.Message,"",MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void buttonCapturar_Click(object sender, EventArgs e)
        {
            try
            {                
                OpenFileDialog file = new OpenFileDialog();
                file.Filter = "png|*.png";
                if (file.ShowDialog() == DialogResult.OK)
                {
                    pictureImagem.ImageLocation = file.FileName;
                    BD.executaQuery("UPDATE Configuracoes_Sistema SET Imagem = '" + file.FileName + "'");
                }
            }
            catch (Exception erro)
            {
                MessageBox.Show(erro.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void carregaImagem()
        {
            try
            {
                pictureImagem.ImageLocation = caminhoImagem;
            }
            catch 
            {
                pictureImagem.Image = null;
            }
        }

        private void atulizaLista()
        {
            //define um objeto DataSet
            DataSet ds = BD.retornaQueryDataSet("SELECT Mensagem, Destinatario, Data, Status FROM Mensagens ORDER BY Data DESC");
            //da.Fill(ds);
            //atribui o dataset ao DataSource do BindingSource
            bs.DataSource = ds;
            //atribui o BindingSource ao BindingNavigator
            bs.DataMember = ds.Tables[0].TableName;
            //Atribui o BindingSource ao DataGridView
            //gridPropriedades.DataSource = bs;
            gridEnviados.DataSource = bs;
        }

        private void kryptonButton2_Click(object sender, EventArgs e)
        {
            try
            {
                //Creating iTextSharp Table from the DataTable data
                PdfPTable pdfTable = new PdfPTable(gridEnviados.ColumnCount);
                pdfTable.DefaultCell.Padding = 3;
                pdfTable.WidthPercentage = 100;
                pdfTable.HorizontalAlignment = Element.ALIGN_LEFT;
                pdfTable.DefaultCell.BorderWidth = 1;
                      

                //adiciona cabeçalho
                Paragraph Empresa = new Paragraph();

                //alinha do titulo e subtitulo
                Empresa.Alignment = 1;

                //adiciona o texto ao titulo e subtitulo
                Chunk tituloEmpresa = new Chunk(textEmpresa.Text, FontFactory.GetFont("dax-black"));
                Phrase phrase1 = new Phrase(tituloEmpresa);
                Chunk subCNPJ = new Chunk("\n"+"CNPJ: "+textCNPJ.Text);
                Phrase phrase2 = new Phrase(subCNPJ);
                Chunk subEndereco = new Chunk("\n" + "Endereço: " + textEndereco.Text);
                Phrase phrase3 = new Phrase(subEndereco);
                Chunk subTelefone = new Chunk("\n" + "Telefone: " + textTelefone.Text);
                Phrase phrase4 = new Phrase(subTelefone);
                Chunk subEspaco = new Chunk("\n"+" ");
                Phrase phrase5 = new Phrase(subEspaco);
            
                Empresa.Add(phrase1);
                Empresa.Add(phrase2);
                Empresa.Add(phrase3);
                Empresa.Add(phrase4);
                Empresa.Add(phrase5);

                //Adding Header row
                foreach (DataGridViewColumn column in gridEnviados.Columns)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText));
                    cell.BackgroundColor = new iTextSharp.text.Color(240, 240, 240);
                    pdfTable.AddCell(cell);
                }

                //Adding DataRow
                foreach (DataGridViewRow row in gridEnviados.Rows)
                {
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        pdfTable.AddCell(cell.Value.ToString());
                    }
                }

                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "PDF|*.pdf";
                saveFileDialog1.Title = "Exportar lista de mensagens enviadas";
                saveFileDialog1.ShowDialog();
                if (saveFileDialog1.FileName != "")
                {
                    System.IO.FileStream fs = (System.IO.FileStream)saveFileDialog1.OpenFile();
                    using (fs)
                    {
                        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                        PdfWriter.GetInstance(pdfDoc, fs);
                        pdfDoc.Open();
                        pdfDoc.Add(Empresa);
                        pdfDoc.Add(pdfTable);
                        pdfDoc.Close();
                        fs.Close();
                    }
                } 
            }
            catch (Exception erro)
            {
                MessageBox.Show(erro.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
                       
        }

        private void pictureValido_Click(object sender, EventArgs e)
        {

        }

    }
}
