﻿namespace SMS
{
    partial class Opcoes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.comboTempo = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.kryptonLabel2 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.kryptonPanel1 = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.kryptonLabel3 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.kryptonLabel9 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.textTempo_Parada = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel8 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel7 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.textTempo_Qtde = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel6 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textOscilador = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel4 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel5 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.kryptonLabel13 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel12 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.textTimer_Parada = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel11 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.textTimer_Qtde = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel10 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textFim = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel17 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel15 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.textInicio = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel14 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel16 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.textBoxLimitePorChip = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.checkBoxDesativarLimitePorChip = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.comboTempo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel1)).BeginInit();
            this.kryptonPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.Location = new System.Drawing.Point(6, 19);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(51, 20);
            this.kryptonLabel1.TabIndex = 0;
            this.kryptonLabel1.Values.Text = "Tempo:";
            // 
            // comboTempo
            // 
            this.comboTempo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboTempo.DropDownWidth = 64;
            this.comboTempo.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "32",
            "34",
            "36",
            "38",
            "40",
            "42",
            "44",
            "46",
            "48",
            "50",
            "52",
            "54",
            "56",
            "58",
            "60",
            "62",
            "64",
            "66",
            "68",
            "70",
            "72",
            "74",
            "76",
            "78",
            "80",
            "82",
            "84",
            "86",
            "88",
            "90",
            "92",
            "94",
            "96",
            "98",
            "100",
            "102",
            "104",
            "106",
            "108",
            "110"});
            this.comboTempo.Location = new System.Drawing.Point(57, 19);
            this.comboTempo.Name = "comboTempo";
            this.comboTempo.Size = new System.Drawing.Size(64, 21);
            this.comboTempo.TabIndex = 1;
            // 
            // kryptonLabel2
            // 
            this.kryptonLabel2.Location = new System.Drawing.Point(127, 20);
            this.kryptonLabel2.Name = "kryptonLabel2";
            this.kryptonLabel2.Size = new System.Drawing.Size(224, 20);
            this.kryptonLabel2.TabIndex = 2;
            this.kryptonLabel2.Values.Text = "Tempo entre as mensagens (segundos)";
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.Size = new System.Drawing.Size(611, 31);
            this.kryptonHeader1.TabIndex = 3;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Opções";
            this.kryptonHeader1.Values.Image = null;
            // 
            // kryptonPanel1
            // 
            this.kryptonPanel1.Controls.Add(this.kryptonHeader1);
            this.kryptonPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonPanel1.Location = new System.Drawing.Point(0, 0);
            this.kryptonPanel1.Name = "kryptonPanel1";
            this.kryptonPanel1.Size = new System.Drawing.Size(611, 31);
            this.kryptonPanel1.TabIndex = 4;
            // 
            // kryptonLabel3
            // 
            this.kryptonLabel3.Location = new System.Drawing.Point(9, 45);
            this.kryptonLabel3.Name = "kryptonLabel3";
            this.kryptonLabel3.Size = new System.Drawing.Size(197, 20);
            this.kryptonLabel3.TabIndex = 13;
            this.kryptonLabel3.Values.Text = "Tempo de Envio: +1, +2, -4, -1, +2";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.kryptonLabel9);
            this.groupBox1.Controls.Add(this.textTempo_Parada);
            this.groupBox1.Controls.Add(this.kryptonLabel8);
            this.groupBox1.Controls.Add(this.kryptonLabel7);
            this.groupBox1.Controls.Add(this.textTempo_Qtde);
            this.groupBox1.Controls.Add(this.kryptonLabel6);
            this.groupBox1.Controls.Add(this.kryptonLabel1);
            this.groupBox1.Controls.Add(this.comboTempo);
            this.groupBox1.Controls.Add(this.kryptonLabel2);
            this.groupBox1.Location = new System.Drawing.Point(12, 37);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(536, 115);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Configurar tempo de envio";
            // 
            // kryptonLabel9
            // 
            this.kryptonLabel9.Location = new System.Drawing.Point(127, 72);
            this.kryptonLabel9.Name = "kryptonLabel9";
            this.kryptonLabel9.Size = new System.Drawing.Size(111, 20);
            this.kryptonLabel9.TabIndex = 8;
            this.kryptonLabel9.Values.Text = "Parada (segundos)";
            // 
            // textTempo_Parada
            // 
            this.textTempo_Parada.Location = new System.Drawing.Point(57, 72);
            this.textTempo_Parada.Name = "textTempo_Parada";
            this.textTempo_Parada.Size = new System.Drawing.Size(64, 20);
            this.textTempo_Parada.TabIndex = 7;
            this.textTempo_Parada.Text = "60";
            this.textTempo_Parada.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textTempo_Parada_KeyPress);
            // 
            // kryptonLabel8
            // 
            this.kryptonLabel8.Location = new System.Drawing.Point(6, 71);
            this.kryptonLabel8.Name = "kryptonLabel8";
            this.kryptonLabel8.Size = new System.Drawing.Size(51, 20);
            this.kryptonLabel8.TabIndex = 6;
            this.kryptonLabel8.Values.Text = "Tempo:";
            // 
            // kryptonLabel7
            // 
            this.kryptonLabel7.Location = new System.Drawing.Point(127, 46);
            this.kryptonLabel7.Name = "kryptonLabel7";
            this.kryptonLabel7.Size = new System.Drawing.Size(131, 20);
            this.kryptonLabel7.TabIndex = 5;
            this.kryptonLabel7.Values.Text = "Envio antes da parada";
            // 
            // textTempo_Qtde
            // 
            this.textTempo_Qtde.Location = new System.Drawing.Point(57, 46);
            this.textTempo_Qtde.Name = "textTempo_Qtde";
            this.textTempo_Qtde.Size = new System.Drawing.Size(64, 20);
            this.textTempo_Qtde.TabIndex = 4;
            this.textTempo_Qtde.Text = "40";
            this.textTempo_Qtde.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textTempo_Qtde_KeyPress);
            // 
            // kryptonLabel6
            // 
            this.kryptonLabel6.Location = new System.Drawing.Point(555, 108);
            this.kryptonLabel6.Name = "kryptonLabel6";
            this.kryptonLabel6.Size = new System.Drawing.Size(40, 20);
            this.kryptonLabel6.TabIndex = 3;
            this.kryptonLabel6.Values.Text = "Qtde:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textOscilador);
            this.groupBox2.Controls.Add(this.kryptonLabel4);
            this.groupBox2.Controls.Add(this.kryptonLabel3);
            this.groupBox2.Controls.Add(this.kryptonLabel5);
            this.groupBox2.Location = new System.Drawing.Point(12, 158);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(246, 77);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Oscilador de Tempo";
            // 
            // textOscilador
            // 
            this.textOscilador.Location = new System.Drawing.Point(91, 19);
            this.textOscilador.Name = "textOscilador";
            this.textOscilador.Size = new System.Drawing.Size(38, 20);
            this.textOscilador.TabIndex = 3;
            this.textOscilador.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textOscilador_KeyPress);
            // 
            // kryptonLabel4
            // 
            this.kryptonLabel4.Location = new System.Drawing.Point(6, 19);
            this.kryptonLabel4.Name = "kryptonLabel4";
            this.kryptonLabel4.Size = new System.Drawing.Size(88, 20);
            this.kryptonLabel4.TabIndex = 0;
            this.kryptonLabel4.Values.Text = "Mudar a cada:";
            // 
            // kryptonLabel5
            // 
            this.kryptonLabel5.Location = new System.Drawing.Point(135, 19);
            this.kryptonLabel5.Name = "kryptonLabel5";
            this.kryptonLabel5.Size = new System.Drawing.Size(55, 20);
            this.kryptonLabel5.TabIndex = 2;
            this.kryptonLabel5.Values.Text = "Minutos";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.kryptonLabel13);
            this.groupBox3.Controls.Add(this.kryptonLabel12);
            this.groupBox3.Controls.Add(this.textTimer_Parada);
            this.groupBox3.Controls.Add(this.kryptonLabel11);
            this.groupBox3.Controls.Add(this.textTimer_Qtde);
            this.groupBox3.Controls.Add(this.kryptonLabel10);
            this.groupBox3.Location = new System.Drawing.Point(302, 158);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(246, 77);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Timer Longo (600)";
            // 
            // kryptonLabel13
            // 
            this.kryptonLabel13.Location = new System.Drawing.Point(113, 19);
            this.kryptonLabel13.Name = "kryptonLabel13";
            this.kryptonLabel13.Size = new System.Drawing.Size(131, 20);
            this.kryptonLabel13.TabIndex = 9;
            this.kryptonLabel13.Values.Text = "Envio antes da parada";
            // 
            // kryptonLabel12
            // 
            this.kryptonLabel12.Location = new System.Drawing.Point(113, 45);
            this.kryptonLabel12.Name = "kryptonLabel12";
            this.kryptonLabel12.Size = new System.Drawing.Size(111, 20);
            this.kryptonLabel12.TabIndex = 9;
            this.kryptonLabel12.Values.Text = "Parada (segundos)";
            // 
            // textTimer_Parada
            // 
            this.textTimer_Parada.Location = new System.Drawing.Point(57, 45);
            this.textTimer_Parada.Name = "textTimer_Parada";
            this.textTimer_Parada.Size = new System.Drawing.Size(50, 20);
            this.textTimer_Parada.TabIndex = 5;
            this.textTimer_Parada.Text = "360";
            this.textTimer_Parada.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textTimer_Parada_KeyPress);
            // 
            // kryptonLabel11
            // 
            this.kryptonLabel11.Location = new System.Drawing.Point(9, 45);
            this.kryptonLabel11.Name = "kryptonLabel11";
            this.kryptonLabel11.Size = new System.Drawing.Size(51, 20);
            this.kryptonLabel11.TabIndex = 4;
            this.kryptonLabel11.Values.Text = "Tempo:";
            // 
            // textTimer_Qtde
            // 
            this.textTimer_Qtde.Location = new System.Drawing.Point(57, 19);
            this.textTimer_Qtde.Name = "textTimer_Qtde";
            this.textTimer_Qtde.Size = new System.Drawing.Size(50, 20);
            this.textTimer_Qtde.TabIndex = 3;
            this.textTimer_Qtde.Text = "600";
            this.textTimer_Qtde.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textTimer_Qtde_KeyPress);
            // 
            // kryptonLabel10
            // 
            this.kryptonLabel10.Location = new System.Drawing.Point(20, 19);
            this.kryptonLabel10.Name = "kryptonLabel10";
            this.kryptonLabel10.Size = new System.Drawing.Size(40, 20);
            this.kryptonLabel10.TabIndex = 0;
            this.kryptonLabel10.Values.Text = "Qtde:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textFim);
            this.groupBox4.Controls.Add(this.kryptonLabel17);
            this.groupBox4.Controls.Add(this.kryptonLabel15);
            this.groupBox4.Controls.Add(this.textInicio);
            this.groupBox4.Controls.Add(this.kryptonLabel14);
            this.groupBox4.Controls.Add(this.kryptonLabel16);
            this.groupBox4.Location = new System.Drawing.Point(12, 241);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(246, 77);
            this.groupBox4.TabIndex = 16;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Agendamento";
            // 
            // textFim
            // 
            this.textFim.Location = new System.Drawing.Point(91, 45);
            this.textFim.Name = "textFim";
            this.textFim.Size = new System.Drawing.Size(99, 20);
            this.textFim.TabIndex = 5;
            this.textFim.Text = "18:00:00";
            this.textFim.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textFim_KeyPress);
            // 
            // kryptonLabel17
            // 
            this.kryptonLabel17.Location = new System.Drawing.Point(188, 45);
            this.kryptonLabel17.Name = "kryptonLabel17";
            this.kryptonLabel17.Size = new System.Drawing.Size(62, 20);
            this.kryptonLabel17.TabIndex = 7;
            this.kryptonLabel17.Values.Text = "hh:mm:ss";
            // 
            // kryptonLabel15
            // 
            this.kryptonLabel15.Location = new System.Drawing.Point(26, 45);
            this.kryptonLabel15.Name = "kryptonLabel15";
            this.kryptonLabel15.Size = new System.Drawing.Size(68, 20);
            this.kryptonLabel15.TabIndex = 4;
            this.kryptonLabel15.Values.Text = "Hora Final:";
            // 
            // textInicio
            // 
            this.textInicio.Location = new System.Drawing.Point(91, 19);
            this.textInicio.Name = "textInicio";
            this.textInicio.Size = new System.Drawing.Size(99, 20);
            this.textInicio.TabIndex = 3;
            this.textInicio.Text = "08:00:00";
            this.textInicio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textInicio_KeyPress);
            // 
            // kryptonLabel14
            // 
            this.kryptonLabel14.Location = new System.Drawing.Point(6, 19);
            this.kryptonLabel14.Name = "kryptonLabel14";
            this.kryptonLabel14.Size = new System.Drawing.Size(90, 20);
            this.kryptonLabel14.TabIndex = 0;
            this.kryptonLabel14.Values.Text = "Hora de Início:";
            // 
            // kryptonLabel16
            // 
            this.kryptonLabel16.Location = new System.Drawing.Point(188, 19);
            this.kryptonLabel16.Name = "kryptonLabel16";
            this.kryptonLabel16.Size = new System.Drawing.Size(62, 20);
            this.kryptonLabel16.TabIndex = 6;
            this.kryptonLabel16.Values.Text = "hh:mm:ss";
            // 
            // textBoxLimitePorChip
            // 
            this.textBoxLimitePorChip.Enabled = false;
            this.textBoxLimitePorChip.Location = new System.Drawing.Point(51, 19);
            this.textBoxLimitePorChip.Name = "textBoxLimitePorChip";
            this.textBoxLimitePorChip.Size = new System.Drawing.Size(81, 20);
            this.textBoxLimitePorChip.TabIndex = 17;
            this.textBoxLimitePorChip.Text = "100";
            this.textBoxLimitePorChip.TextChanged += new System.EventHandler(this.salvar);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Enviar";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(138, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "mensagens por chip";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.checkBoxDesativarLimitePorChip);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.textBoxLimitePorChip);
            this.groupBox5.Location = new System.Drawing.Point(302, 241);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(246, 77);
            this.groupBox5.TabIndex = 20;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Envios por chip";
            // 
            // checkBoxDesativarLimitePorChip
            // 
            this.checkBoxDesativarLimitePorChip.AutoSize = true;
            this.checkBoxDesativarLimitePorChip.Location = new System.Drawing.Point(11, 45);
            this.checkBoxDesativarLimitePorChip.Name = "checkBoxDesativarLimitePorChip";
            this.checkBoxDesativarLimitePorChip.Size = new System.Drawing.Size(53, 17);
            this.checkBoxDesativarLimitePorChip.TabIndex = 20;
            this.checkBoxDesativarLimitePorChip.Text = "Ativar";
            this.checkBoxDesativarLimitePorChip.UseVisualStyleBackColor = true;
            this.checkBoxDesativarLimitePorChip.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // Opcoes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(611, 390);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.kryptonPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Opcoes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Opcoes";
            this.Load += new System.EventHandler(this.Opcoes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.comboTempo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel1)).EndInit();
            this.kryptonPanel1.ResumeLayout(false);
            this.kryptonPanel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel1;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox comboTempo;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel2;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel kryptonPanel1;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textOscilador;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel4;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel5;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel9;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textTempo_Parada;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel8;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel7;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textTempo_Qtde;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel6;
        private System.Windows.Forms.GroupBox groupBox3;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel11;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel10;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel13;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel12;
        private System.Windows.Forms.GroupBox groupBox4;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel17;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textFim;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel15;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel14;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel16;
        private System.Windows.Forms.TextBox textBoxLimitePorChip;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox checkBoxDesativarLimitePorChip;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textTimer_Parada;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textTimer_Qtde;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox textInicio;
    }
}