﻿using System;
using System.Data.SQLite;
using System.Threading;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace SMS
{
    public partial class Opcoes : Form
    {

        public static int LIMITE_PORT_CHIP = 100;
        public static bool LIMITAR_CHIP = false;

        public Opcoes()
        {
            InitializeComponent();


        }

        private void Opcoes_Load(object sender, EventArgs e)
        {
            atualizaDados();
        }


        private void atualizaDados()
        {
        
            try
            {
                SQLiteDataReader dr = BD.retornaQuery("SELECT Tempo, Oscilador, TempoQtde, TempoParada, " +
                "TimerQtde, TimerParada, Inicio, Fim, LimitarChip, Limitar FROM Opcoes");

                while (dr.Read())
                {
                    comboTempo.SelectedItem = dr[0].ToString();
                    textOscilador.Text      = dr[1].ToString();
                    textTempo_Qtde.Text     = dr[2].ToString();
                    textTempo_Parada.Text   = dr[3].ToString();
                    textTimer_Qtde.Text     = dr[4].ToString();
                    textTimer_Parada.Text   = dr[5].ToString();
                    textInicio.Text         = dr[6].ToString();
                    textFim.Text            = dr[7].ToString();
                    textBoxLimitePorChip.Text = dr[8].ToString();


                    try {
                        int limitar = Convert.ToInt32(dr[9].ToString());
                        bool limitarB = Convert.ToBoolean(limitar);
                        checkBoxDesativarLimitePorChip.Checked = limitarB;
                    }catch{
                        checkBoxDesativarLimitePorChip.Checked = false;
                    }
                }

                dr.Close();
                this.textOscilador.TextChanged       += new System.EventHandler(salvar);
                this.textTempo_Qtde.TextChanged      += new System.EventHandler(salvar);
                this.textTempo_Parada.TextChanged    += new System.EventHandler(salvar);
                this.textTimer_Qtde.TextChanged      += new System.EventHandler(salvar);
                this.textTimer_Parada.TextChanged    += new System.EventHandler(salvar);
                this.textInicio.TextChanged          += new System.EventHandler(salvar);
                this.textFim.TextChanged             += new System.EventHandler(salvar);
                this.comboTempo.SelectedIndexChanged += new System.EventHandler(salvar);
                this.textBoxLimitePorChip.TextChanged += new System.EventHandler(salvar);

                this.FormClosing += new FormClosingEventHandler(salvarDatabase);
            }
            catch (Exception erro)
            {
                MessageBox.Show(erro.Message);
            }
            
        }

        private bool checkValidNumber(string value, out int result)
        {
            result = -1;
            if (int.TryParse(value, out result) && result > 1) { 
                return true;
            }

            return false;
        }

        private void salvarDatabase(object sender, EventArgs e)
        {
            BD.executaQuery("UPDATE Opcoes SET "
                + "   Tempo       = '" + comboTempo.SelectedItem
                + "', Oscilador   = '" + textOscilador.Text
                + "', TempoQtde   = '" + textTempo_Qtde.Text
                + "', TempoParada = '" + textTempo_Parada.Text
                + "', TimerQtde   = '" + textTimer_Qtde.Text
                + "', TimerParada = '" + textTimer_Parada.Text
                + "', Inicio      = '" + textInicio.Text
                + "', LimitarChip = '" + textBoxLimitePorChip.Text
                + "', Fim         = '" + textFim.Text
                + "' ");
            Desktop.MAIN.inicio = textInicio.Text;
            Desktop.MAIN.fim = textFim.Text;
        }
        private void salvar(object sender, EventArgs e)
        {
            int result;
            if (!checkValidNumber(textOscilador.Text, out result) 
                || !checkValidNumber(textTempo_Qtde.Text, out result)
                 || !checkValidNumber(textTempo_Parada.Text, out result)
                  || !checkValidNumber(textTimer_Qtde.Text, out result)
                   || !checkValidNumber(textTimer_Parada.Text, out result)
                   || !checkValidNumber(textBoxLimitePorChip.Text, out result))
            {
                Regex digitsOnly = new Regex(@"[^\d]");
                textOscilador.Text = digitsOnly.Replace(textOscilador.Text, "");
                textTempo_Qtde.Text = digitsOnly.Replace(textTempo_Qtde.Text, "");
                textTempo_Parada.Text = digitsOnly.Replace(textTempo_Parada.Text, "");
                textTimer_Qtde.Text = digitsOnly.Replace(textTimer_Qtde.Text, "");
                textTimer_Parada.Text = digitsOnly.Replace(textTimer_Parada.Text, "");
                textBoxLimitePorChip.Text = digitsOnly.Replace(textBoxLimitePorChip.Text, "");
                if (textOscilador.Text == "")
                    textOscilador.Text = "0";
                if (textTempo_Qtde.Text == "")
                    textTempo_Qtde.Text = "0";
                if (textTempo_Parada.Text == "")
                    textTempo_Parada.Text = "0";
                if (textTimer_Qtde.Text == "")
                    textTimer_Qtde.Text = "0";
                if (textTimer_Parada.Text == "")
                    textTimer_Parada.Text = "0";
                if (textBoxLimitePorChip.Text == "")
                    textBoxLimitePorChip.Text = "0";
            }

            

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            textBoxLimitePorChip.Enabled = checkBoxDesativarLimitePorChip.Checked;
            LIMITAR_CHIP = checkBoxDesativarLimitePorChip.Checked;

            string query = "UPDATE Opcoes SET "
               + "Limitar = '" + Convert.ToInt32(checkBoxDesativarLimitePorChip.Checked) + "'";
            BD.executaQuery(query);
            salvarDatabase(null,null);
            atualizaDados();
        }

        private void textTimer_Qtde_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void textInicio_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != ':';
        }

        private void textFim_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != ':';
        }

        private void textTimer_Parada_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void textTempo_Qtde_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void textTempo_Parada_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void textOscilador_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
