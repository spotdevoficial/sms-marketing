﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GsmComm.PduConverter;
using GsmComm.PduConverter.SmartMessaging;
using GsmComm.GsmCommunication;
using GsmComm.Interfaces;
using GsmComm.Server;
using System.Data.SQLite;
using System.Threading;
using System.Windows.Forms;

namespace SMS
{
    class SMS
    {
        static int envioTempo = 0;
        static int envioQuantidade = 0;
        static int envioParada = 0;
        static int oscilador = 0;
        static int longoQuantidade = 0;
        static int longoTempo = 0;
        static int quantidadeParada = 0;
        static List<string> listaPortas = new List<string>();
        static List<Thread> threads = new List<Thread>();
        static List<Mensagem> listaMensagem = new List<Mensagem>();
        static int quantidadePortas = 0;
        static int totalMensagem = 0;

        public static void habilitaEnvio()
        {
            try
            {
                listaMensagem.Clear();
                SQLiteDataReader dr = BD.retornaQuery("SELECT COUNT(*), Porta FROM Dispositivos");
                while (dr.Read())
                {
                    quantidadePortas = Convert.ToInt32(dr[0].ToString());
                    listaPortas.Add(dr[1].ToString());
                    GsmCommMain portaComm = new GsmCommMain(dr[1].ToString(), 9600, 300);
                } dr.Close();

                SQLiteDataReader dr2 = BD.retornaQuery("SELECT Tempo, Oscilador, TempoQtde, TempoParada, TimerQtde, TimerParada, Inicio, Fim, LimitarChip, Limitar FROM Opcoes");
                while (dr.Read())
                {
                    envioTempo = Convert.ToInt32(dr2[0].ToString());
                    oscilador = Convert.ToInt32(dr2[1].ToString());
                    envioQuantidade = Convert.ToInt32(dr2[2].ToString());
                    envioParada = Convert.ToInt32(dr2[3].ToString());
                    longoQuantidade = Convert.ToInt32(dr2[4].ToString());
                    longoTempo = Convert.ToInt32(dr2[5].ToString());
                    Opcoes.LIMITE_PORT_CHIP = Convert.ToInt32(dr2[8].ToString());
                    Opcoes.LIMITAR_CHIP = Convert.ToBoolean(Convert.ToInt32(dr2[9].ToString()));
                } dr.Close();


                SQLiteDataReader dr1 = BD.retornaQuery("SELECT * FROM Caixa_Saida");
                while (dr1.Read())
                {
                    var mensagem = new Mensagem
                    {
                        id_mensagem = dr1[0].ToString(),
                        destinatario = dr1[1].ToString(),
                        mensagem = dr1[2].ToString(),
                    };
                    listaMensagem.Add(mensagem);
                }

                foreach (var item in listaPortas)
                {
                    SmsSubmitPdu pdu;
                    GsmCommMain portaComm = new GsmCommMain(item.ToString(), 9600, 300);
                    if (!portaComm.IsOpen())
                        portaComm.Open();

                    while (listaMensagem.Count > 0)
                    {
                        //Envia(portaComm, listaMensagem[0].mensagem + " " + RandomString(3), listaMensagem[0].destinatario);

                        listaMensagem.RemoveAt(0);
                        Thread.Sleep(envioParada * 1000);
                    }

                }
            }
            catch(Exception erro)
            {
                throw erro;
            }
            

            //foreach (var item in listaPortas)
            //    iniciaEnvio(item);
            //iniciaEnvio();

            /*for (int i = 1; i <= quantidadePortas; i++)
            {
                ParameterizedThreadStart pt = new ParameterizedThreadStart(iniciaEnvio);
                threads[i] = new Thread(pt);
                threads[i].Name = i.ToString();
                threads[i].Start(listaPortas[i]);
            }*/
        }

        public static void Envia(GsmCommMain porta, string mensagem, string destinatario, string id)
        {
            try
            {
                SmsSubmitPdu pdu;
                byte dcs = (byte)DataCodingScheme.GeneralCoding.Alpha7BitDefault;
                pdu = new SmsSubmitPdu(mensagem, destinatario, dcs);

                int times = 1;
                for (int i = 0; i < times; i++)
                {
                    porta.SendMessage(pdu);
                    BD.executaQuery("INSERT INTO Mensagens(Data,Destinatario,Mensagem,Status) VALUES( " +
                        "'" + System.DateTime.Now + "', '" + destinatario + "', '" + mensagem + "', 'Enviado' )");
                    BD.executaQuery("DELETE FROM Caixa_Saida WHERE ID_Mensagem = '" + id + "'");

                }
            }
            catch(Exception erro)
            {
                Console.WriteLine("Não foi enviada... "+erro.ToString()+" Trace: "+erro.StackTrace);
                throw erro;
            }
            finally { porta.Close(); }
            
        }


        public static void iniciaEnvio()
        {
            try
            {
                while (listaMensagem.Count > 0)
                {
                    foreach (var item in listaPortas)
                    {
                        SmsSubmitPdu pdu;
                        GsmCommMain portaComm = new GsmCommMain(item.ToString(), 9600, 300);
                        if (!portaComm.IsOpen())
                            portaComm.Open();

                        if (quantidadeParada <= envioQuantidade)
                        {
                            byte dcs = (byte)DataCodingScheme.GeneralCoding.Alpha7BitDefault;
                            pdu = new SmsSubmitPdu(listaMensagem[0].mensagem + " " + RandomString(3), listaMensagem[0].destinatario, dcs);

                            int times = 1;
                            for (int i = 0; i < times; i++)
                            {
                                portaComm.SendMessage(pdu);
                                BD.executaQuery("INSERT INTO Mensagens(Data,Destinatario,Mensagem,Status) VALUES( " +
                                    "'" + System.DateTime.Now + "', '" + listaMensagem[0].destinatario + "', '" + listaMensagem[0].mensagem + "', 'Enviado' )");
                                BD.executaQuery("DELETE FROM Caixa_Saida WHERE ID_Mensagem = '" + listaMensagem[0].id_mensagem + "'");
                            }
                            Thread.Sleep(envioTempo * 1000);
                            listaMensagem.RemoveAt(0);
                        }
                        else
                        {
                            quantidadeParada = 0;
                            Thread.Sleep(envioParada * 1000);
                        }
                        portaComm.Close();
                    }
                }
            }
            catch (Exception erro)
            {
                MessageBox.Show(erro.Message,"Necessário verificar as portas de comunicação");
            }
            
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        class Mensagem
        {
            public string id_mensagem { get; set; }
            public string destinatario { get; set; }
            public string mensagem { get; set; }
        }

    }
}
